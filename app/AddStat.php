<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddStat extends Model
{
    protected $fillable = ['type','deviceType','deviceModel','appVersion','appBuild','user_id','wasClicked','general_ad_id','competition_native_ad_id'];
    protected $table = 'add_stats';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function generalAd()
    {
        return $this->belongsTo('App\generalAd');
    }
    public function competitionAd()
    {
        return $this->belongsTo('App\CompetitionNativeAdd');
    }
}

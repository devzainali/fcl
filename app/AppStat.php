<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppStat extends Model
{
    protected $fillable = ['name','deviceType','deviceModel','appVersion','appBuild','user_id'];
    protected $table = 'app_stats';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

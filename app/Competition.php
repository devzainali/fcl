<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = [
        'name','subAmount','status','description','start_date','expired_at'];

    public function subscribe(){
        return $this->hasOne('App\userCompetition');
    }

    public function subscriptions(){
        return $this->hasMany('App\userCompetition');
    }

    public function quizes(){
        return $this->hasMany('App\Quiz');
    }

    public function resultAllQuiz(){
        return $this->hasMany('App\QuizResult');
    }

    public function winners()
    {
        return $this->hasMany('App\CompetitionWiner');
    }
    
}

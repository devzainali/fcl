<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitionNativeAdd extends Model
{
    
    protected $fillable = ['competition_id','url','startDate','endDate','image','image2','image3','image4','image5'];
    protected $table = 'competition_native_ads';

    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }
}

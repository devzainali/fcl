<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitionNativeImage extends Model
{
    protected $fillable = ['competition_native_ad_id','image'];
}

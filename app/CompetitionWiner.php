<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitionWiner extends Model
{
    protected $fillable = ['competition_id','winner1','winner2','winner3','date'];
    protected $table = 'competition_winers';

    public function getWinner1(){
        return $this->hasOne('App\User', 'id','winner1');
    }
    public function getWinner2(){
        return $this->hasOne('App\User', 'id','winner2');
    }
    public function getWinner3(){
        return $this->hasOne('App\User', 'id','winner3');
    }
}

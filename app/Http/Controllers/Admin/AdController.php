<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AddStat;

class AdController extends Controller
{
    public function index($id,$type){
        if($type == 'Competition'){
            $getAdd =  AddStat::where([
                'type'=>$type,
                'competition_native_ad_id'=>$id
            ])->with(['user','competitionAd'])->paginate(20);
            return view('admin.adStat.list')->with(['stats'=>$getAdd,'type'=>'Competition']);
        }else if($type == 'General'){
            $getAdd =  AddStat::where([
                'type'=>$type,
                'general_ad_id'=>$id
            ])->with(['user','generalAd'])->paginate(20);
            return view('admin.adStat.list')->with(['stats'=>$getAdd,'type'=>'General']);
        }

        return redirect()->back()->with('error','Unknown add Type ');
    }   
    public function destroy(Request $request){
        $id = $request->statID;
        $AddStat = AddStat::where('id', $id)->first();
        if (!empty($AddStat)) {
            $destroy = AddStat::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return redirect()->back()->with('error','Record not found.');
        }
    }
}

<?php



namespace App\Http\Controllers\Admin;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Transaction;
use App\News;
use App\AppStat;
use App\generalAd;
use App\Competition;
use App\userCompetition;
use Illuminate\Support\Facades\Hash;

use App\Admin;



class AdminController extends Controller

{

	public function __construct()

    {

        // $this->middleware(['auth:admin','admin.verified']);
        $this->middleware(['auth:admin']);

    }

    public function index(){
        
        return view('admin.profile');
    }

    public function showProfile(){

        // dd(Auth::guard('admin')->user()->email);

    	return view('admin.profile');

    }

    public function changePassword(Request $request){

        $this->validate($request,[

             'oldpassword' => 'required|string',

             'password' => 'required|confirmed',

        ]);

        $admin = Admin::where('id', Auth::guard('admin')->user()->id)->first();
        if (Hash::check($request->oldpassword, $admin->password)) {

            $admin->password = hash::make($request->password);

            $admin->save();

            return redirect()->route('admin.profile')->with('success','Password changed successfully');

        }else{

            return redirect()->route('admin.profile')->with('error','Old password does not match');

        }
    }
    public function userList(){
        $user =  User::orderBy('id','desc')->paginate(20);
        $count =  User::count();
        return view('admin.user.list',['users'=>$user,'count'=>$count]);

    }

    public function dashboard(){
        $totalUser = User::count();
        $totalNews = News::count();
        $AppStat = AppStat::count();
        $generalAd = generalAd::count();
        $totalSale = Transaction::sum('amount');
        $activeCompetition = Competition::where('status',true)->count();
        $totalSubscribed = userCompetition::groupBy('user_id')->count();
        $orderThisMonth = Transaction::whereMonth('created_at',date('m'))->count();
        $monthSale = Transaction::whereMonth('created_at',date('m'))->sum('amount');
        $monthSaleGraph = Transaction::whereMonth('created_at',date('m'))->pluck('amount');
        $lastMonthSale = Transaction::whereMonth('created_at',date('m', strtotime("-1 month")))->sum('amount');
        $lastMonthSaleGraph = Transaction::whereMonth('created_at',date('m', strtotime("-1 month")))->pluck('amount');
        $lastTransaction = Transaction::with('user')->orderBy('id', 'DESC')->limit(5)->get();
        return view('admin.blank',compact('totalUser','orderThisMonth','monthSale','lastMonthSale','lastTransaction','monthSaleGraph','lastMonthSaleGraph','totalSale','totalNews','AppStat','generalAd','activeCompetition','totalSubscribed'));

    }

}


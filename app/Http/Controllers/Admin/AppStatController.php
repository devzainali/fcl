<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\AppStat;
use App\Http\Controllers\Controller;

class AppStatController extends Controller
{
    public function gerStatistics(){
        $mainPage =  AppStat::where('name','Application Main page')->with('user')->count();
        $newsPage =  AppStat::where('name','FCL News Page')->with('user')->count();
        $newsDetailPage =  AppStat::where('name','FCL News Details Page')->with('user')->count();
        $competitionPage =  AppStat::where('name','FCL competition List Page')->with('user')->count();
        $leaguePage =  AppStat::where('name','FCL league page')->with('user')->count();
        $questionPage =  AppStat::where('name','FCL question page')->with('user')->count();
        $giftPage =  AppStat::where('name','FCL gift page')->with('user')->count();

        $data =  AppStat::with('user')->orderBy('id', 'desc')->paginate(20);
        return view('admin.appstat.list',[
            'mainPage'=>$mainPage,
            'newsPage'=>$newsPage,
            'newsDetailPage'=>$newsDetailPage,
            'competitionPage'=>$competitionPage,
            'leaguePage'=>$leaguePage,
            'questionPage'=>$questionPage,
            'giftPage'=>$giftPage,
            ]);
    }
    public function gerStatisticsByType($type){
        $pages = array("Application Main page", "FCL News Page", "FCL News Details Page", "FCL competition List Page","FCL league page","FCL question page","FCL gift page");
        
        if (!in_array($type, $pages))
        {
       return redirect()->back()->with('error','Type is not correct');
        }
        $mainPage =  AppStat::where('name','Application Main page')->with('user')->count();
        $newsPage =  AppStat::where('name','FCL News Page')->with('user')->count();
        $newsDetailPage =  AppStat::where('name','FCL News Details Page')->with('user')->count();
        $competitionPage =  AppStat::where('name','FCL competition List Page')->with('user')->count();
        $leaguePage =  AppStat::where('name','FCL league page')->with('user')->count();
        $questionPage =  AppStat::where('name','FCL question page')->with('user')->count();
        $giftPage =  AppStat::where('name','FCL gift page')->with('user')->count();

        $data =  AppStat::where('name',$type)->with('user')->orderBy('id', 'desc')->paginate(20);
        return view('admin.appstat.list',[
            'stats'=>$data,
            'mainPage'=>$mainPage,
            'newsPage'=>$newsPage,
            'newsDetailPage'=>$newsDetailPage,
            'competitionPage'=>$competitionPage,
            'leaguePage'=>$leaguePage,
            'questionPage'=>$questionPage,
            'giftPage'=>$giftPage,
            'selectedValue'=>$type,
            ]);
    }

    public function destroy(Request $request){
        $id = $request->statID;
        $AppStat = AppStat::where('id', $id)->first();
        if (!empty($AppStat)) {
            $destroy = AppStat::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return redirect()->back()->with('error','Record not found.');
        }
    }
}

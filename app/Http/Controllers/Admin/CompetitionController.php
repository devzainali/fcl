<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Competition;
use App\CompetitionWiner;
use App\PushNotification;
use App\userCompetition;
use App\Quiz;
use Carbon;


class CompetitionController extends Controller
{
    public function __construct()

    {

        $this->middleware(['auth:admin']);

    }

    public function markWinner(Request $request){
        $this->validate($request,[
            'winner' => 'required|numeric|in:1,2,3',
            'user_id' => 'required|numeric',
            'competition_id' => 'required|numeric',
           ]);
        $getCompetition = Competition::where('id',$request->competition_id)->with(['subscriptions','subscriptions.user'])->first();
            if(!empty($getCompetition)){
                $winner= CompetitionWiner:: where(
                    'competition_id', $getCompetition->id,
                    )
                ->whereYear('date', '=', Carbon\Carbon::parse(date('y-m-d'))->year)
                ->whereMonth('date', '=', Carbon\Carbon::parse(date('y-m-d'))->month)
                ->first();  
                $columnName = 'winner'.$request->winner;
                if(is_null($winner)){
                    $win = CompetitionWiner::create([
                        $columnName => $request->user_id,
                        'competition_id' => $request->competition_id,
                        'date' => date('y-m-d'),
                    ]);
                    $notification = new PushNotification();
                    $notification->message = 'Admin mark you as a winner No '.$request->winner.' for ( competiton:'.$getCompetition->name.' )';
                    $notification->user_id = $request->user_id;
                    $notification->type = 'user';
                    $notification->save();
                    return redirect()->back()->with('success','Winner added successfully.');
                }else{
                        if($winner->$columnName == $request->user_id){
                            return redirect()->back()->with('error','This person is already - '.$columnName);
                        }elseif($winner->winner1 == $request->user_id ||$winner->winner3 == $request->user_id || $winner->winner3 == $request->user_id){
                                return redirect()->back()->with('error','One person not declared many time in winner list');
                        }
            
                        $winner->$columnName = $request->user_id;
                        $winner->save();
                        $notification = new PushNotification();
                        $notification->message = 'Admin mark you as a winner No '.$request->winner.' for ( competiton:'.$getCompetition->name.' )';
                        $notification->user_id = $request->user_id;
                        $notification->type = 'user';
                        $notification->save();
                        return redirect()->back()->with('success','Winner added successfully.');
                    }
            }
        return redirect()->back()->with('error','Competition Not Found');
    }

    public function getCompetitionScore(Request $request,$id)
    {
        $getCompetition = Competition::where('id',$id)->with('winners')->first();
        if(!empty($getCompetition)){
            $competitionUser = userCompetition::where('competition_id',$id)->with('user')->paginate(20);
            return view('admin.competition.competitionUserScoreList',['competitons' => $competitionUser,'winners'=>$getCompetition->winners,'quizCompetition'=>$id]);
        }
        return redirect()->back()->with('error','Competition Not Found');
    }

    public function competitionUserList(Request $request,$id){
        $getCompetition = Competition::where('id',$id)->with(['subscriptions','subscriptions.user','subscriptions.transaction'])->first();
        if(!empty($getCompetition)){
            $competitionUser = userCompetition::where('competition_id',$id)->with(['user','transaction'])->paginate(20);
            return view('admin.competition.subscriptionList',['competitons' => $competitionUser,'competiton' => $getCompetition,'quizCompetition'=>$id]);
        }
        return redirect()->back()->with('error','Competition Not Found');

    }

    public function index()
    {
        $competitions = Competition::orderBy('id', 'desc')->paginate(20);
        return view('admin.competition.list',['competitons' => $competitions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.competition.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'price' => 'required|numeric',
            'status' => 'sometimes|string',
            'startdate' => 'required|date',
            'expireDate' => 'required|date',
           ]);
           $competition = new Competition();
           $competition->name=$request->title;
           $competition->subAmount=$request->price;
           $competition->expired_at=$request->expireDate;
           $competition->start_date=$request->startdate;
           if($request->status){
            $competition->status = true;
            }else{
                $competition->status =false;
            }
          $competition->save();
          return redirect()->route('admin.showCompetition')->with('success','Competition added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $competiton = Competition::where('id', $id)->first();
        if(!empty($competiton)){
            return view('admin.competition.edit',compact('competiton'));
        }
        return redirect()->route('admin.showCompetition')->with('error','Competition not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'price' => 'required|numeric',
            'status' => 'sometimes|string',
            'startdate' => 'required|date',
            'expireDate' => 'required|date',
           ]);
           $competiton = Competition::where('id', $id)->first();
           if(!empty($competiton)){
           $competiton->name = $request->title;
           $competiton->subAmount=$request->price;
           $competiton->expired_at=$request->expireDate;
           $competiton->start_date=$request->startdate;
           if($request->status){
            $competiton->status = true;
            }else{
                $competiton->status =false;
            }
          $competiton->save();
          return redirect()->route('admin.showCompetition')->with('success','Competition updated successfully');
           }
           return redirect()->route('admin.showCompetition')->with('error','Competition not found');
           

    }

    public function changeStatus($id){
        $competiton = Competition::where('id', $id)->first();
        if (!empty($competiton)) {
            $competiton->status = !$competiton->status;
            $competiton->save();
            return response()->json(
            [
                'status'=>true,
                'message'=>'competiton status changed successfully',
            ]);
        }else{ 
            return response()->json(
                [
                    'status'=>false,
                    'message'=>'competiton not found',
                ]);
            
        }
    }

    public function destroy(Request $request,$id){
        $check = Competition::where('id', $id)->first();
        if (!empty($check)) {
            $checkSubscription = userCompetition::where('competition_id',$id)->count();
            if($checkSubscription > 0){
                return redirect()->back()->with('error','You are not allow to delete competition because their subscriptions found');
            }
            $checkQuiz = Quiz::where('competition_id',$id)->count();
            if($checkQuiz > 0){
                return redirect()->back()->with('error','You are not allow to delete competition because their Quiz found');
            }
            $destroy = Competition::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return redirect()->back()->with('error','Record not found.');
        }
    }
}

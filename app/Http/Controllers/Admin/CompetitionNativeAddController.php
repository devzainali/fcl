<?php

namespace App\Http\Controllers\admin;

use App\Competition;
use App\CompetitionNativeAdd;
use App\CompetitionNativeImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class CompetitionNativeAddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        return view('admin.nativeAdd.add',['competitions' => $competitions]);
    }

    public function list()
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        return view('admin.nativeAdd.filterByCompetition',['competitions' => $competitions]);
    }


    public function loadAdsCompetition($id)
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        $competition = Competition::where('id',$id)->first();
        if($competition){
            $Ads = CompetitionNativeAdd::where('competition_id',$id)->with('competition')->orderBy('id', 'desc')->paginate(20);
            return view('admin.nativeAdd.filterByCompetition',['Ads' => $Ads,'competitions' => $competitions,'competition' => $competition,'quizCompetition'=>$id]);
        }
        return redirect()->back()->with('error','Competition not found');
    }

    public function getCompetitionAds($id){
        $competitions = Competition::where('id',$id)->first();
        if($competitions){
            $Ads = CompetitionNativeAdd::where('competition_id',$id)->with('competition')->orderBy('id', 'desc')->paginate(20);
            return view('admin.nativeAdd.lists',['Ads' => $Ads,'competition' => $competitions]);
        }
       
        return redirect()->back()->with('error','Competition not found');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'competitionID' => 'required|numeric|max:255',
        //     'image' => 'required|array',
        //     'image.*' => 'image|mimes:jpg,jpeg'
        //     ]);

        $this->validate($request,[
            'title' => $request->title != null ?'sometimes|required|String|min:8': '',
            'url' => 'required|string|max:255',
            'competitionID' => 'required|numeric|max:255',
            'startDate' => 'required|before:endDate| after:'. date('Y-m-d'),
            'endDate' => 'required',
            'image' => 'image|mimes:jpg,jpeg|required|max:2000|dimensions:width=1000,height=428',
            'image2' => 'image|mimes:jpg,jpeg|required|max:2000|dimensions:width=1000,height=428',
            'image3' => 'image|mimes:jpg,jpeg|required|max:2000|dimensions:width=1000,height=428',
            'image4' => 'image|mimes:jpg,jpeg|max:2000|dimensions:width=1000,height=428',
            'image5' => 'image|mimes:jpg,jpeg|max:2000|dimensions:width=1000,height=428',
        ],
        [
            'image.max'    => 'Allowed image 1 size is 2 MB Maximum',
            'image.dimensions'    => 'Image 1 dimension must be 1000x428.',
            'image1.max'    => 'Allowed image 2 size is 2 MB Maximum',
            'image1.dimensions'    => 'Image 2 dimension must be 1000x428.',
            'image2.max'    => 'Allowed image 3 size is 2 MB Maximum',
            'image2.dimensions'    => 'Image 3 dimension must be 1000x428.',
            'image3.max'    => 'Allowed image 4 size is 2 MB Maximum',
            'image3.dimensions'    => 'Image 4 dimension must be 1000x428.',
            'image4.max'    => 'Allowed image 5 size is 2 MB Maximum',
            'image4.dimensions'    => 'Image 5 dimension must be 1000x428.',
        ]
    );
            $competiton = Competition::where('id', $request->competitionID)->first();
           if(!empty($competiton)){
               $startDateCheck = CompetitionNativeAdd::where('competition_id', $request->competitionID)->whereDate('startDate', '=', date_format(date_create($request->startDate),"Y-m-d"))->count();
               $endDateCheck = CompetitionNativeAdd::where('competition_id', $request->competitionID)->whereDate('endDate', '=', date_format(date_create($request->endDate),"Y-m-d"))->count();
               if($startDateCheck >= 5){
                return redirect()->route('admin.addNativeAdd')->with('error','Please select another date. Already maximum ads are shown at this start Date.');
               }
               if($endDateCheck >= 5){
                return redirect()->route('admin.addNativeAdd')->with('error','Please select another date. Already maximum ads are shown at this end Date.');
               }
                $newAdd = new CompetitionNativeAdd();
                $newAdd->title = $request->title;
                $newAdd->url = $request->url;
                $newAdd->competition_id  = $request->competitionID;
                $newAdd->startDate = $request->startDate;
                $newAdd->endDate = $request->endDate;
                if($request->has('image')){
                    $newAdd->image =$request->image->store('competition');
                }
                if($request->has('image2')){
                    $newAdd->image2 =$request->image2->store('competition');
                }
                if($request->has('image3')){
                    $newAdd->image3 =$request->image3->store('competition');
                }
                if($request->has('image4')){
                    $newAdd->image4 =$request->image4->store('competition');
                }
                if($request->has('image5')){
                    $newAdd->image5 =$request->image5->store('competition');
                }
                $newAdd->save();
            return redirect()->route('admin.addNativeAdd')->with('success','Competition native add addedd successfully');
           }
           return redirect()->route('admin.addNativeAdd')->with('error','Competition not found');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CompetitionNativeAdd = CompetitionNativeAdd::where('id', $id)->first();
        if (!empty($CompetitionNativeAdd)) {
            @Storage::delete($CompetitionNativeAdd->image);
            $destroy = CompetitionNativeAdd::destroy($id);
            if($destroy){
                return redirect()->route('admin.geteNativeAdd',$CompetitionNativeAdd->competition_id)->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'Competition Native Add is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('error','Record not found.');
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'Competition Native Add not found.',
            //     ]);
        }
    }
}

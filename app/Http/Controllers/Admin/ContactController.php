<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact;
use App\PushNotification;

class ContactController extends Controller
{
    public function __construct()

    {

        $this->middleware(['auth:admin']);

    }

    public function index()
    {
        $contact = Contact::orderBy('id', 'desc')->paginate(20);
        $count = Contact::count();
        return view('admin.contactUs.contact',['contacts' => $contact,'count' => $count]);
    }

    public function contactReplay(Request $request){
        $this->validate($request,[
            'description' => 'required|string|max:255',
            'user_id' => 'required|string|max:255',
           ]);
           $notification = new PushNotification();
           $notification->type='user';
           $notification->message=$request->description;
           $notification->user_id=$request->user_id;
           $notification->save();
          return redirect()->back()->with('success','reply is send successfully');
    }

    public function destroy($id)
    {
        $contact = Contact::where('id', $id)->first();
        if (!empty($contact)) {
            $destroy = Contact::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'Contact Request is deleted successfully.',
                //     ]);
            }
        }else{
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'Contact Request not found.',
            //     ]);
            return redirect()->back()->with('error','Record not found.');
        }

    }
}

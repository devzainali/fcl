<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\generalAd;
use Illuminate\Support\Facades\Storage;


class GeneralAdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.generalAdd.add');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => $request->title != null ?'sometimes|required|String|min:4': '',
            'url' => 'required|string|max:255',
            'startDate' => 'required|date|before:endDate|after:'. date('Y-m-d'),
            'endDate' => 'required|string|max:255',
            'image' => 'required|image|mimes:jpg,jpeg|max:2000|dimensions:width=1013,height=434'
        ],
        [
            'image.max'    => 'Allowed image size is 2 MB Maximum',
            'image.dimensions'    => 'Image dimension must be 1013x434.',
        ]
    );
            $startDateCheck = generalAd::whereDate('startDate', '=', date_format(date_create($request->startDate),"Y-m-d"))->count();
            $endDateCheck = generalAd::whereDate('endDate', '=', date_format(date_create($request->endDate),"Y-m-d"))->count();
            if($startDateCheck >= 5){
                return redirect()->route('admin.addNativeAdd')->with('error','Please select another date. Already maximum ads are shown at this start Date.');
               }
            if($endDateCheck >= 5){
                return redirect()->route('admin.addNativeAdd')->with('error','Please select another date. Already maximum ads are shown at this end Date.');
            }
            $newAdd = new generalAd();
                $newAdd->title = $request->title;
                $newAdd->url = $request->url;
                $newAdd->startDate = $request->startDate;
                $newAdd->endDate = $request->endDate;
                if($request->has('image')){
                    $newAdd->image =$request->image->store('generalAdd');
                }
                $newAdd->save();
            return redirect()->route('admin.geteGeneralNativeSave')->with('success','General native add addedd successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $Ads = generalAd::orderBy('id', 'desc')->paginate(1);
        return view('admin.generalAdd.lists',['Ads' => $Ads]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $CompetitionNativeAdd = generalAd::where('id', $id)->first();
        if (!empty($CompetitionNativeAdd)) {
            @Storage::delete($CompetitionNativeAdd->image);
            $destroy = generalAd::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'General Add Add is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('error','Record not found.');
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'General Native Add not found.',
            //     ]);
        }
    }
}

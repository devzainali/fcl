<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Gift;
use App\Competition;

class GiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $competiton = Competition::where('id', $id)->first();
        if (!empty($competiton)) {
            $gifts = Gift::where('competition_id', $id)->get();
            if($gifts->count() > 0){
                return redirect()->route('admin.showGift',$id);
                // return view('admin.gift.list', compact(array('gifts','competiton'))); 
            }
            $competitionName = $competiton->name;
            $competition = $competiton->id;
            $quizCompetition = $id;
            return view('admin.gift.add', compact(array('competition','competitionName','quizCompetition')));
        }
        return redirect()->route('admin.showCompetition')->with('error','Competition not found');

        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'competitionID' => 'required|string|max:255',
            'titleGiftOne' => 'required|string|max:255',
            // 'pointdGiftOne' => 'required|string|max:255',
            'descriptionOne' => 'required|string',
            'imageForGiftOne' => 'required|mimes:jpeg,png|max:2000|dimensions:width=994,height=426',
            'titleGiftTwo' => 'required|string|max:255',
            // 'pointdGiftTwo' => 'required|string|max:255',
            'descriptionTwo' => 'required|string',
            'imageForGiftTwo' => 'required|mimes:jpeg,png|max:2000|dimensions:width=994,height=426',
            'titleGiftThree' => 'required|string|max:255',
            // 'pointdGiftThree' => 'required|string|max:255',
            'descriptionThree' => 'required|string',
            'imageForGiftThree' => 'required|mimes:jpeg,png|max:2000|dimensions:width=994,height=426'
            ],
            [
                'imageForGiftOne.max'    => 'Allowed Gift 1 image  size is 2 MB Maximum',
                'imageForGiftOne.dimensions'    => 'Gift 1 Image  dimension must be 994x426.',
                'imageForGiftTwo.max'    => 'Allowed Gift 2 image  size is 2 MB Maximum',
                'imageForGiftTwo.dimensions'    => 'Gift 2 Image  dimension must be 994x426.',
                'imageForGiftThree.max'    => 'Allowed Gift 3 image  size is 2 MB Maximum',
                'imageForGiftThree.dimensions'    => 'Gift 3 Image  dimension must be 994x426.',
            ]);
            //1st
            $Gift1 = new Gift();
            $Gift1->title=$request->titleGiftOne;
            // $Gift1->winPoint=$request->pointdGiftOne;
            $Gift1->description=$request->descriptionOne;
            $Gift1->competition_id=$request->competitionID;
            $Gift1->number=1;
           if($request->has('imageForGiftOne')){
               $Gift1->image =$request->imageForGiftOne->store('gift');
           }
           $Gift1->save();
            //2nd
            $Gift2 = new Gift();
            $Gift2->title=$request->titleGiftTwo;
            // $Gift2->winPoint=$request->pointdGiftTwo;
            $Gift2->description=$request->descriptionTwo;
            $Gift2->competition_id=$request->competitionID;
            $Gift2->number=2;
            if($request->has('imageForGiftTwo')){
                $Gift2->image =$request->imageForGiftTwo->store('gift');
            }
            $Gift2->save();
            //3rd
            $Gift3 = new Gift();
            $Gift3->title=$request->titleGiftThree;
            // $Gift3->winPoint=$request->pointdGiftThree;
            $Gift3->description=$request->descriptionThree;
            $Gift3->competition_id=$request->competitionID;
            $Gift3->number=3;
            if($request->has('imageForGiftThree')){
                $Gift3->image =$request->imageForGiftThree->store('gift');
            }
            $Gift3->save();
            return redirect()->route('admin.showGift',$request->competitionID)->with('success','Gifts added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $competiton = Competition::where('id', $id)->first();
        if (!empty($competiton)) {
            $gifts = Gift::where('competition_id', $id)->get();
            $quizCompetition = $id;
            if($gifts->count() > 0){
                return view('admin.gift.list', compact(array('gifts','competiton','quizCompetition'))); 
            }else{
               
                $competitionName = $competiton->name;
                $competition = $competiton->id;
                return redirect()->route('admin.addgift',$id);
            }
           
        }
        return redirect()->route('admin.showCompetition')->with('error','Competition not found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gift = Gift::where('id', $id)->first();
        if(!empty($gift)){
            
            return view('admin.gift.edit',compact('gift'));
        }
        return redirect()->route('admin.showCompetition')->with('error','Gift not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'titleGiftOne' => 'required|string|max:255',
            // 'pointdGiftOne' => 'required|string|max:255',
            'descriptionOne' => 'required|string',
            'imageForGiftOne' => 'sometimes|mimes:jpeg,png',
           ]);
           $Gift = Gift::where('id', $id)->first();
           if(!empty($Gift)){
            $Gift->title=$request->titleGiftOne;
            // $Gift->winPoint=$request->pointdGiftOne;
            $Gift->description=$request->descriptionOne;
           if($request->has('imageForGiftOne')){
            if(Storage::delete($Gift->image)) {
                $Gift->image =$request->imageForGiftOne->store('gift');
             }
               
           }
           $Gift->save();
          return redirect()->route('admin.showGift',$Gift->competition_id)->with('success','Gift updated successfully');
           }
           return redirect()->route('admin.showGift',$Gift->competition_id)->with('error','Gift not found');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LiveScore;

class LiveScoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.livescore.livescore');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'url' => 'required|string|max:255|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
           ]);

           $check = LiveScore::first();
            if (!empty($check)) {
               
                $check->url=$request->url;
                $check->save();
               return redirect()->back()->with('success','Live score url is updated successfully');
            }
            $url = new LiveScore();
            $url->url=$request->url;
            $url->save();
            return redirect()->back()->with('success','Live score url is added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {   
        $check = LiveScore::first();
        if(!empty($check)){
            return view('admin.livescore.list')->with('Score',$check);
        }
        return view('admin.livescore.livescore');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = LiveScore::where('id',$id)->first();
        if(empty($check)){
            return redirect()->back()->with('error','Record not found');
        }
        return view('admin.livescore.edit')->with('score',$check);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'url' => 'required|string|max:255|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
           ]);

           $check = LiveScore::where('id',$id)->first();
            if (!empty($check)) {
                $check->url=$request->url;
                $check->save();
               return redirect()->route('admin.livescoreList')->with('success','Live score url is updated successfully');
            }
            return redirect()->route('admin.livescoreList')->with('error','Record not found');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = LiveScore::where('id',$id)->first();
        if(!empty($check)){
            $destroy = LiveScore::destroy($id);
            return redirect()->route('admin.livescore');
        }
        return redirect()->route('admin.livescoreList');
        
    }
}

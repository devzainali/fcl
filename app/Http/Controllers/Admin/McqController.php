<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Quiz;
use App\QuestionMc;
use App\QuestionTf;
use App\QuestionVs;
use App\McqSubmitAnswer;
use App\Http\Controllers\Controller;

class McqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $quiz = Quiz::where('id',$id)->with('QuestionMc')->first();
        if(!empty($quiz)){
            return view('admin.question.mcq',compact('quiz'));
        }
        return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'question' => 'required|string|max:255',
            'option1' => 'required|string|max:255',
            'option2' => 'required|string|max:255',
            'option3' => 'sometimes|nullable|string|max:255',
            'option4' => 'sometimes|nullable|string|max:255',
            'answer' => 'sometimes|nullable|string',
            'quiz_id' => 'required|string',
            'isBonus' => 'sometimes|in:on',
           ]);
           $check = Quiz::where('id',$request->quizID)->first();
            if (!$check) {
                if($this->checkPostedQuestionCount($request->quiz_id) >= 12){
                    return redirect()->back()->with('error','Question limit reached. ');

                }
                
                $QuestionMc = new QuestionMc();
                $QuestionMc->Question=$request->question;
                $QuestionMc->option1=$request->option1;
                $QuestionMc->option2=$request->option2;
                if($request->isBonus){
                    if($this->checkBonusExist($request->quiz_id) >= 2){
                        return redirect()->back()->with('error','Only 2 bonus questions are allowed');
    
                    }
                    $QuestionMc->bonus=($request->isBonus == "on")?true:false;
                }
                if($request->has('option3')){
                    $QuestionMc->option3=$request->option3;
                }
                if($request->has('option4')){
                    $QuestionMc->option4=$request->option4;
                }
                if($request->has('answer')){
                    $QuestionMc->answer=$request->answer;
                }
                $QuestionMc->quiz_id=$request->quiz_id;
                $QuestionMc->save();
               return redirect()->route('admin.showMcq',$request->quiz_id)->with('success','Mcq question is added successfully');
            }
            return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = QuestionMc::where('id',$id)->first();
        if(!empty($check)){
            return view('admin.question.editMcq',['value' => $check]);

        }
        return redirect()->back()->with('error','Mcq Question not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $QuestionMc = QuestionMc::where('id',$id)->first();
        if(!empty($QuestionMc)){
            $this->validate($request,[
                'question' => 'required|string|max:255',
                'option1' => 'required|string|max:255',
                'option2' => 'required|string|max:255',
                'option3' => 'sometimes|nullable|string|max:255',
                'option4' => 'sometimes|nullable|string|max:255',
                'answer' => 'sometimes|nullable|string',
                'isBonus' => 'sometimes|in:on',
               ]);
               $QuestionMc->Question=$request->question;
               $QuestionMc->option1=$request->option1;
               $QuestionMc->option2=$request->option2;
               if($request->isBonus){
                   if($QuestionMc->bonus != true){
                        if($this->checkBonusExist($QuestionMc->quiz_id) >= 2){
                            return redirect()->back()->with('error','Only 2 bonus questions are allowed');
                        }
                   }
                $QuestionMc->bonus=($request->isBonus == "on")?true:false;
               }else{
                $QuestionMc->bonus=false;
               }
               if($request->has('option3')){
                   $QuestionMc->option3=$request->option3;
               }
               if($request->has('option4')){
                   $QuestionMc->option4=$request->option4;
               }
               if($request->has('answer')){
                   $QuestionMc->answer=$request->answer;
               }
               $QuestionMc->save();
              return redirect()->route('admin.showMcq',$QuestionMc->quiz_id)->with('success','Mcq question is updated successfully');
        }
        return redirect()->route('admin.showMcq',$QuestionMc->quiz_id)->with('error','Mcq question not found.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        $check = QuestionMc::where('id', $id)->first();
        if (!empty($check)) {
            $MCQResultSubmit = McqSubmitAnswer::where('question_mc_id',$id)->count();
            // $TfSubmitAnswer = TfSubmitAnswer::where('quiz_id',$id)->count();
            // $VsSubmitAnswer = VsSubmitAnswer::where('quiz_id',$id)->count();
            if($MCQResultSubmit > 0 ){
                
                return redirect()->back()->with('error','You are not allow to delete question because user submit answers against this question');
            }
            $destroy = QuestionMc::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return  redirect()->back()->with('error','Record not found.');
        }
    }

    function checkPostedQuestionCount($id){
        $vsCount = QuestionVs::where('quiz_id',$id)->count();
        $mcqCount = QuestionMc::where('quiz_id',$id)->count();
        $tfCount = QuestionTf::where('quiz_id',$id)->count();
        return $mcqCount + $tfCount+$vsCount;
    }
    function checkBonusExist($id){
        $mcqCount = QuestionMc::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $tfCount = QuestionTf::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $vsCount = QuestionVs::where(['quiz_id'=>$id,'bonus'=>true])->count();
        return $mcqCount + $tfCount+$vsCount;
    }
    function checkBonusExistWhileUpdate($id,$questionID){
        $mcqCount = QuestionMc::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $tfCount = QuestionTf::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $vsCount = QuestionVs::where(['quiz_id'=>$id,'bonus'=>true])->whereNotIn('id', [$questionID])->count();
        return $mcqCount + $tfCount+$vsCount;
    }
}


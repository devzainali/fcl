<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\News;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('id', 'desc')->paginate(1);
        return view('admin.news.list',['news' => $news]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request,[
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'image|mimes:jpeg,png|required|max:2000|dimensions:width=829,height=197',
           ],
           [
               'image.max'    => 'Allowed image size is 2 MB Maximum',
               'image.dimensions'    => 'Image dimension must be 829x197.',
               
           ]);

                 $news = new News();
                 $news->title=$request->title;
                 $news->description=$request->description;
                 $news->slug = str_slug($request->title, '-');
                if($request->has('image')){
                    $news->image =$request->image->store('blog');
                }
                $news->save();
                return redirect()->route('admin.addNews')->with('success','News added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::where('id',$id)->first();
        if(!empty($news)){
            return view('admin.news.edit',['new' => $news]);

        }
        return redirect()->back()->with('error','News not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'image' => 'mimes:jpeg,png'
           ]);
           $news = News::where('id', $id)->first();
           if(!empty($news)){
            $news->title=$request->title;
            $news->description=$request->description;
            $news->slug = str_slug($request->title, '-');
           if($request->has('image')){
                @Storage::delete($news->image);
               $news->image =$request->image->store('blog');
           }
           $news->save();
          return redirect()->route('admin.newsStorelist')->with('success','News updated successfully');
           }
           return redirect()->route('admin.newsStorelist')->with('error','News not found');
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = News::where('id', $id)->first();
        if (!empty($news)) {
            @Storage::delete($news->image);
            $destroy = News::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'News is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('error','Record not found.');
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'News not found.',
            //     ]);
        }
    }
}

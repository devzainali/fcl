<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PushNotification;

class PushNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notification = PushNotification::where('type','admin')->orderBy('id', 'desc')->paginate(1);
        return view('admin.pushNotification.list',['notification'=>$notification]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'description' => 'required|string|max:255',
           ]);
           $notification = new PushNotification();
           $notification->message=$request->description;
          $notification->save();
          return redirect()->route('admin.NotificationList')->with('success','Push Notification added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = PushNotification::where('id', $id)->first();
        if (!empty($partner)) {
            $destroy = PushNotification::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'Push Notification is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('error','Record not found.');
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'Push Notification not found.',
            //     ]);
        }
    }
}

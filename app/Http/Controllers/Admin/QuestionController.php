<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Competition;
use App\Quiz;

class QuestionController extends Controller
{
    function loadCompetitions(){
        $competitions = Competition::orderBy('id', 'desc')->get();
        return view('admin.question.competitionPage',['competitions' => $competitions,'quizes'=>[],'quiz'=>null,]);
    }

    function loadCompetitionsQuiz($id){
        $quiz = Quiz::where('competition_id',$id)->orderBy('id', 'desc')->get();
        return response()->json([
            "status"=>true,
            'data' => $quiz,
            "errorMessage" => null,
            ], 200);
    }

    function loadQuestions(Request $request){
    //    dd($request);
        $competitions = Competition::orderBy('id', 'desc')->get();
        $quizes = Quiz::orderBy('id', 'desc')->get();
        $quiz = Quiz::where('id',$request->quiz_id)->with(['QuestionMc','QuestionTf','QuestionVs'])->first();
        // dd($quiz);
        return view('admin.question.competitionPage',['competitions' => $competitions,'quizes'=>$quizes,'quiz'=>$quiz,]);
    }

}

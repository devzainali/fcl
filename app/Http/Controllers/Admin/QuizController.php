<?php

namespace App\Http\Controllers\Admin;

use App\Quiz;
use App\Competition;
use App\QuestionMc;
use App\QuestionVs;
use App\QuestionTf;
use App\QuizResult;
use App\McqSubmitAnswer;
use App\TfSubmitAnswer;
use App\VsSubmitAnswer;
use App\PushNotification;
use App\userCompetition;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        $quiz = Quiz::orderBy('id', 'desc')->with('competition','result')->paginate(20);
        $calculated = QuizResult::where('id',$quiz)->first();
        return view('admin.quiz.list',['quizes' => $quiz,'competitions' => $competitions]);
    }

    public function loadQuizByCompetition($competition_id)
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        $check = Competition::where('id',$competition_id)->first();
        if($check){
            $quiz = Quiz::where('competition_id',$competition_id)->orderBy('id', 'desc')->with('competition','result')->paginate(20);
            return view('admin.quiz.list',['quizes' => $quiz,'competitions' => $competitions,'quizCompetition'=>$competition_id]);   
        }
         return redirect()->back()->with('error','Competition Not found');
        
    }

    public function loadQuestionByQuiz($id)
    {
        $quiz = Quiz::where('id',$id)->with(['QuestionMc','QuestionTf','QuestionVs'])->first();
        return view('admin.question.submitAnswer',['quiz'=>$quiz]);
     }
    
     public function submitQuizAnswer(Request $request){

            //calculate result
            $quiz = Quiz::where('id',$request->quiz_id)
        ->with(['QuestionMc','QuestionTf','QuestionVs','QuestionMc.McqAnswers','QuestionTf.TfAnswers','QuestionVs.VsAnswers'])
        ->first();
        if(empty($quiz)){
                return redirect()->back()->with('error','No Such quiz found.');
        }
        $getSubscriptions = userCompetition::where('competition_id',$quiz->competition_id)->get();


        if($quiz->QuestionMc->isEmpty() && $quiz->QuestionMc->isEmpty() && $quiz->QuestionMc->isEmpty()){
            return redirect()->back()->with('error','No Such question found to calculate Result.');
        }
              //Update Answers
              for($i = 0;$i<count($request->mcqID); $i++){
                QuestionMc::where('id', $request->mcqID[$i])->update(['answer' => $request->mcq[$i]]);
            }
            for($t = 0;$t<count($request->mcqID); $t++){
                QuestionTf::where('id', $request->tfID[$t])->update(['is_correct' => $request->tf[$t]]);
            }
            for($v = 0;$v<count($request->vsID); $v++){
                QuestionVs::where('id', $request->vsID[$v])->update(['answer' => $request->vs[$v]]);
            }
            //Update Answers


        $mcqStatus = true;
        $tfStatus = true;
        $vsStatus = true;
        //check Mcq
        foreach($quiz->QuestionMc as $key=>$mcq):
            if(is_null($mcq->answer)){
                $mcqStatus = false;
            }
        endforeach;
        if(!$mcqStatus){
            return redirect()->back()->with('error','Please add all Answers of Mcq Question of this Quiz ( '.$quiz->title.' )');
        }

          //check True/False
          foreach($quiz->QuestionTf as $key=>$tf):
            if(is_null($tf->is_correct)){
                $tfStatus = false;
            }
        endforeach;
        if(!$tfStatus){
            return redirect()->back()->with('error','Please add all Answers of True/False Question of this Quiz ( '.$quiz->title.' )');
        }

           //check Versus Question
           foreach($quiz->QuestionVs as $key=>$vs):
            if(is_null($vs->answer)){
                $vsStatus = false;
            }
        endforeach;
        if(!$vsStatus){
            return redirect()->back()->with('error','Please add all Answers of Versus Question of this Quiz ( '.$quiz->title.' )');
        }

        //Get Quiz Questions answers accoring to user
        foreach($quiz->QuestionMc as $key=>$McqAnswer):
                if(!empty($McqAnswer->McqAnswers)){
                    foreach($McqAnswer->McqAnswers as $key=>$answercomes):
                        $answer = 'option'.$McqAnswer->answer;
                        //entry in result
                        $resultMcq= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                    'user_id' => $answercomes->user_id,]);
                                    $answercome = 'option'.$answercomes->answer;
                                    if($resultMcq->id) {
                                        if($McqAnswer->$answer == $McqAnswer->$answercome){
                                            if($McqAnswer->bonus == true){
                                                $resultMcq->mcqPoint += 20;
                                            }else{
                                                $resultMcq->mcqPoint += 10;
                                            }
                                            $resultMcq->user_id = $answercomes->user_id;
                                        }
                                    } 
                                    if(!$resultMcq->id) {
                                        if($McqAnswer->$answer == $McqAnswer->$answercome){
                                            if($McqAnswer->bonus == true){
                                                $resultMcq->mcqPoint += 20;
                                            }else{
                                                $resultMcq->mcqPoint += 10;
                                            }
                                            $resultMcq->user_id = $answercomes->user_id;
                                        }
                                    }
                             $resultMcq->save();
                    endforeach;    
                   
                }
        endforeach;

                //Get True/False Questions answers according to user
                foreach($quiz->QuestionTf as $key=>$Question):
                    if(!empty($Question->TfAnswers)){
                        foreach($Question->TfAnswers as $key=>$answerTf):
                            $answer = $Question->is_correct;
                            //entry in result
                            $resultTf= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                        'user_id' => $answerTf->user_id,]);
                                        if($resultTf->id) {
                                            if(json_encode($Question->is_correct) == $answerTf->answer){
                                                // print_r(json_encode($Question->is_correct) == $answerTf->answer);
                                                // dd(json_encode($Question->is_correct) == $answerTf->answer);
                                                if($Question->bonus == true){
                                                    $resultTf->tfPoint  += 20;
                                                }else{
                                                    $resultTf->tfPoint  += 10;
                                                }
                                                $resultTf->user_id = $answerTf->user_id;
                                            }
                                        } 
                                        if(!$resultTf->id) {
                                            if(json_encode($Question->is_correct) == $answerTf->answer){
                                                if($Question->bonus == true){
                                                    $resultTf->tfPoint  += 20;
                                                }else{
                                                    $resultTf->tfPoint  += 10;
                                                }
                                                $resultTf->user_id = $answerTf->user_id;
                                            }
                                        }
                                        $resultTf->save();
                                       
                        endforeach;    
                       
                    }
            endforeach;


            //Get Quiz Questions answers accoring to user
        foreach($quiz->QuestionVs as $key=>$questionVs):
            if(!empty($questionVs->VsAnswers)){
                foreach($questionVs->VsAnswers as $key=>$answercomes):
                    $answer = 'v'.$questionVs->answer.'Name';
                    //entry in result
                    $resultVs= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                'user_id' => $answercomes->user_id,]);
                                $answercomesUser = 'v'.$answercomes->answer.'Name';
                                if($resultVs->id) {
                                    if($questionVs->$answer == $questionVs->$answercomesUser){
                                        if($questionVs->bonus == true){
                                            $resultVs->vsPoint  += 20;
                                        }else{
                                            $resultVs->vsPoint  += 10;
                                        }
                                        $resultVs->user_id = $answercomes->user_id;
                                    }
                                } 
                                if(!$resultVs->id) {
                                    if($questionVs->answer == $questionVs->answercomesUser){
                                        if($questionVs->bonus == true){
                                            $resultVs->vsPoint  += 20;
                                        }else{
                                            $resultVs->vsPoint  += 10;
                                        }
                                        $resultVs->user_id = $answercomes->user_id;
                                    }
                                }
                                $resultVs->save();
                                  
                endforeach;    
            }
    endforeach;

    foreach($getSubscriptions as $key=>$value):
        $other= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                'user_id' => $value->user_id]);
                                if(!$other->id) {
                                    $resultVs->mcqPoint = 0;
                                    $resultVs->vsPoint = 0;
                                    $resultVs->tfPoint = 0;
                                    $resultVs->user_id = $value->user_id;
                                }
                                $other->save();

    endforeach;

    return redirect()->route('admin.showQuiz')->with('success','Quiz Result is Perform successfully');
     }

    public function showQuizResult($id){
        $check = Quiz::where('id',$id)
        ->first();
        if(empty($check)){
            return redirect()->back()->with('error','No Such quiz found.');
            }
        $quiz = QuizResult::where('quiz_id',$id)
        ->with(['user'])
        ->paginate(20);
        if(count($quiz) > 0){
            return view('admin.quiz.result',['results' => $quiz,'quiz'=>$check]);
        }
        return redirect()->back()->with('error','Quiz result not found.');

    }

    public function calculateQuizResult($id){
        $quiz = Quiz::where('id',$id)
        ->with(['QuestionMc','QuestionTf','QuestionVs','QuestionMc.McqAnswers','QuestionTf.TfAnswers','QuestionVs.VsAnswers'])
        ->first();
        if(empty($quiz)){
                return redirect()->back()->with('error','No Such quiz found.');
        }
        if($quiz->QuestionMc->isEmpty() && $quiz->QuestionMc->isEmpty() && $quiz->QuestionMc->isEmpty()){
            return redirect()->back()->with('error','No Such question found to calculate Result.');
        }
        $mcqStatus = true;
        $tfStatus = true;
        $vsStatus = true;
        //check Mcq
        foreach($quiz->QuestionMc as $key=>$mcq):
            if(is_null($mcq->answer)){
                $mcqStatus = false;
            }
        endforeach;
        if(!$mcqStatus){
            return redirect()->back()->with('error','Please add all Answers of Mcq Question of this Quiz ( '.$quiz->title.' )');
        }

          //check True/False
          foreach($quiz->QuestionTf as $key=>$tf):
            if(is_null($tf->is_correct)){
                $tfStatus = false;
            }
        endforeach;
        if(!$tfStatus){
            return redirect()->back()->with('error','Please add all Answers of True/False Question of this Quiz ( '.$quiz->title.' )');
        }

           //check Versus Question
           foreach($quiz->QuestionVs as $key=>$vs):
            if(is_null($vs->answer)){
                $vsStatus = false;
            }
        endforeach;
        if(!$vsStatus){
            return redirect()->back()->with('error','Please add all Answers of Versus Question of this Quiz ( '.$quiz->title.' )');
        }

        //Get Quiz Questions answers accoring to user
        foreach($quiz->QuestionMc as $key=>$McqAnswer):
                if(!empty($McqAnswer->McqAnswers)){
                    foreach($McqAnswer->McqAnswers as $key=>$answercomes):
                        $answer = 'option'.$McqAnswer->answer;
                        //entry in result
                        $resultMcq= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                    'user_id' => $answercomes->user_id,]);
                                    $answercome = 'option'.$answercomes->answer;
                                    if($resultMcq->id) {
                                        if($McqAnswer->$answer == $McqAnswer->$answercome){
                                            if($McqAnswer->bonus == true){
                                                $resultMcq->mcqPoint += 20;
                                            }else{
                                                $resultMcq->mcqPoint += 10;
                                            }
                                            $resultMcq->user_id = $answercomes->user_id;
                                        }
                                    } 
                                    if(!$resultMcq->id) {
                                        if($McqAnswer->$answer == $McqAnswer->$answercome){
                                            if($McqAnswer->bonus == true){
                                                $resultMcq->mcqPoint += 20;
                                            }else{
                                                $resultMcq->mcqPoint += 10;
                                            }
                                            $resultMcq->user_id = $answercomes->user_id;
                                        }
                                    }
                                    $resultMcq->save();
                             
                    endforeach;    
                   
                }
        endforeach;

                //Get True/False Questions answers according to user
                foreach($quiz->QuestionTf as $key=>$Question):
                    if(!empty($Question->TfAnswers)){
                        foreach($Question->TfAnswers as $key=>$answerTf):
                            $answer = $Question->is_correct;
                            // dd(json_encode($Question->is_correct) == $answerTf->answer);
                            //entry in result
                            $resultTf= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                        'user_id' => $answerTf->user_id,]);
                                        if($resultTf->id) {
                                            if(json_encode($Question->is_correct) == $answerTf->answer){
                                                print_r(json_encode($Question->is_correct) == $answerTf->answer);
                                                if($Question->bonus == true){
                                                    $resultTf->tfPoint  += 20;
                                                }else{
                                                    $resultTf->tfPoint  += 10;
                                                }
                                                $resultTf->user_id = $answerTf->user_id;
                                            }
                                        } 
                                        if(!$resultTf->id) {
                                            if(json_encode($Question->is_correct) == $answerTf->answer){
                                                if($Question->bonus == true){
                                                    $resultTf->tfPoint  += 20;
                                                }else{
                                                    $resultTf->tfPoint  += 10;
                                                }
                                                $resultTf->user_id = $answerTf->user_id;
                                            }
                                        }
                                        $resultTf->save();
                        endforeach;    
                       
                    }
            endforeach;


            //Get Quiz Questions answers accoring to user
        foreach($quiz->QuestionVs as $key=>$questionVs):
            if(!empty($questionVs->VsAnswers)){
                foreach($questionVs->VsAnswers as $key=>$answercomes):
                    $answer = 'v'.$questionVs->answer.'Name';
                    //entry in result
                    $resultVs= \App\QuizResult::firstOrNew(['quiz_id' => $quiz->id,
                                'user_id' => $answercomes->user_id,]);
                                $answercomesUser = 'v'.$answercomes->answer.'Name';
                                if($resultVs->id) {
                                    if($questionVs->$answer == $questionVs->$answercomesUser){
                                        if($questionVs->bonus == true){
                                            $resultVs->vsPoint  += 20;
                                        }else{
                                            $resultVs->vsPoint  += 10;
                                        }
                                        $resultVs->user_id = $answercomes->user_id;
                                    }
                                } 
                                if(!$resultVs->id) {
                                    if($questionVs->answer == $questionVs->answercomesUser){
                                        if($questionVs->bonus == true){
                                            $resultVs->vsPoint  += 20;
                                        }else{
                                            $resultVs->vsPoint  += 10;
                                        }
                                        $resultVs->user_id = $answercomes->user_id;
                                    }
                                }
                                $resultVs->save();
                                  
                endforeach;    
            }
    endforeach;
    return redirect()->back()->with('success','Quiz Result is Perform successfully');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $this->validate($request,[
            'name' => 'required|string|max:255',
            'competition_id' => 'required|string|max:255',
            'expireDate' => 'required|date',
           ]);
    
           $check = Competition::where('id',$request->competition_id)->first();
            if ($check) {
                $expire = strtotime($check->expired_at);
                $today = strtotime("today midnight");
                if($today >= $expire){
                  return redirect()->back()->with('error','Competition is expired so you are not allowed to add week quiz.');
                }
                $expired = strtotime($request->expireDate);
                $competitionEndDate = strtotime($check->expired_at);
                if($competitionEndDate >= $expired){
                    $Quiz = new Quiz();
                    $Quiz->title=$request->name;
                    $Quiz->competition_id=$request->competition_id;
                    $Quiz->expired_at=$request->expireDate;
                   $Quiz->save();
                   return redirect()->route('admin.showQuiz')->with('success','Quiz is added successfully');
                }else{
                    return redirect()->back()->with('error','Week quiz expire date is not more than competition expire date.');

                  }

            }
            return redirect()->route('admin.showQuiz')->with('error','Competition not found.');

               
    }

   /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = Quiz::where('id',$id)->with('competition')->first();
        if(!empty($check)){
            $expired = strtotime($check->expired_at);
            $competitionEndDate = strtotime($check->competition->expired_at);
            if($competitionEndDate >= $expired){
                return view('admin.quiz.edit',['quiz' => $check]);
            }
            return redirect()->back()->with('error','Competition date is expired so you cannot edit week quiz date.');


        }
        return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $check = Quiz::where('id',$id)->with('competition')->first();
        if(!empty($check)){
            $this->validate($request,[
                'name' => 'required|string|max:255',
                'expireDate' => 'required|date',
               ]);
                $expire = strtotime($request->expireDate);
                $today = strtotime("today midnight");
                if($today >= $expire){
                    return redirect()->back()->with('error','quiz expiration date  must be greater than today.');
                  }
                  $expired = strtotime($request->expireDate);
                  $competitionEndDate = strtotime($check->competition->expired_at);
                  if($competitionEndDate >= $expired){
                    $check->title = $request->name;
                    $check->expired_at=$request->expireDate;
                    $check->save();
                    return redirect()->route('admin.showQuiz')->with('success','Quiz updated successfully.');
                  }
                  return redirect()->back()->with('error','Week quiz expire date is not more than competition expire date.');

        }
        return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        $check = Quiz::where('id', $id)->first();
        if (!empty($check)) {
            // $MCQResultSubmit = McqSubmitAnswer::where('quiz_id',$id)->count();
            // $TfSubmitAnswer = TfSubmitAnswer::where('quiz_id',$id)->count();
            // $VsSubmitAnswer = VsSubmitAnswer::where('quiz_id',$id)->count();
            $MCQ = QuestionMc::where('quiz_id',$id)->count();
            $Tf = QuestionTf::where('quiz_id',$id)->count();
            $Vs = QuestionVs::where('quiz_id',$id)->count();
            if($MCQ > 0 || $Tf > 0 ||$Vs > 0){
                return redirect()->back()->with('error','You are not allow to delete Quiz because Questions are added agains this quiz');
            }
            $destroy = Quiz::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return redirect()->back()->with('error','Record not found.');
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\social;

class SocialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $social = social::paginate(30);
        return view('admin.social.list')->with('Socoals',$social);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'platform' => 'required|string|max:255',
            'link' => 'required|string|max:255|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
           ]);
           $check =  social::where('socialName',$request->platform)->count();
            if($check > 0){
                return redirect()->back()->with('error','Social Link are already exist.');
            }
           $Social = new social();
           $Social->socialName=$request->platform;
           $Social->link=$request->link;
           $Social->save();
          return redirect()->route('admin.social.list')->with('success','Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Social = social::where('id', $id)->first();
        if (!empty($Social)) {
            return view('admin.social.edit')->with('social',$Social);
        }
        return redirect()->back()->with('error','Not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Social = social::where('id', $id)->first();
        if (!empty($Social)) {
            $this->validate($request,[
                'platform' => 'required|string|max:255',
                'link' => 'required|string|max:255|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/',
               ]);
               $Social->socialName=$request->platform;
               $Social->link=$request->link;
               $Social->save();
              return redirect()->route('admin.social.list')->with('success','Updated successfully');
        }
        return redirect()->back()->with('error','Not found');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Social = social::where('id', $id)->first();
        if (!empty($Social)) {
            $destroy = social::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'Push Notification is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('error','Record not found.');
            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'Push Notification not found.',
            //     ]);
        }
    }
}

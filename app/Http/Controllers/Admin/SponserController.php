<?php

namespace App\Http\Controllers\Admin;
use App\Sponser;
use App\Competition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class SponserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competitions = Competition::orderBy('id', 'desc')->get();
        $sponser = Sponser::orderBy('id', 'desc')->with('competition')->paginate();
        return view('admin.sponser.list',['Sponsers' => $sponser,'competitions' => $competitions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'competition_id' => 'required|numeric',
            // 'image' => 'required|mimes:jpeg,png|max:1024|dimensions:min_width=100,max_width=250,min_height=250,max_height=500',
            'image' => 'required|mimes:jpeg,png|max:2000|dimensions:width=1008,height=1476',
            'effectivedate' => 'required|date',
        ],
        [
            'image.max'    => 'Allowed image size is 2 MB Maximum',
            'image.dimensions'    => 'Image dimension must be 1008x1476 .',
        ]
    );
           $competition = Competition::where('id', $request->competition_id)->first();
           if(empty($competition)){
            return redirect()->route('admin.showSponser')->with('error','Competition not found.');
           }
           $Sponser = Sponser::where('competition_id', $request->competition_id)->first();
           if(!empty($Sponser)){
            return redirect()->route('admin.showSponser')->with('error','Sponser is already added.');
           }
                 $sponser = new Sponser();
                 $sponser->competition_id=$request->competition_id;
                 $sponser->effective_date=$request->effectivedate;
                if($request->has('image')){
                    $sponser->image =$request->image->store('sponser');
                }
                $sponser->save();
                return redirect()->route('admin.showSponser')->with('success','Sponser added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner = Sponser::where('id', $id)->first();
        if (!empty($partner)) {
            @Storage::delete($partner->image);
            $destroy = Sponser::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully');
                // return response()->json(
                //     [
                //         'status'=>true,
                //         'message'=>'Sponser is deleted successfully.',
                //     ]);
            }
        }else{
            return redirect()->back()->with('success','Record not found');

            // return response()->json(
            //     [
            //         'status'=>false,
            //         'message'=>'Sponser not found.',
            //     ]);
        }
    }
}

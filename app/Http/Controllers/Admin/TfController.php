<?php

namespace App\Http\Controllers\Admin;
use App\Quiz;
use App\QuestionTf;
use App\QuestionMc;
use App\QuestionVs;
use App\TfSubmitAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $quiz = Quiz::where('id',$id)->with('QuestionMc')->first();
        if(!empty($quiz)){
            return view('admin.question.tf',compact('quiz'));
        }
        return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'question' => 'required|string|max:255',
            'Answer' => 'sometimes|string',
            'quiz_id' => 'required|string',
            'isBonus' => 'sometimes|in:on',
           ]);

           $check = Quiz::where('id',$request->quizID)->first();
            if (!$check) {
                $checking = $this->checkPostedQuestionCount($request->quiz_id);
                if($this->checkPostedQuestionCount($request->quiz_id) >= 10){
                    return redirect()->back()->with('error','Question limit reached. ');

                }
                $QuestionTf = new QuestionTf();
                $QuestionTf->Question=$request->question;
                if($request->isBonus){
                    if($this->checkBonusExist($request->quiz_id) >= 2){
                        return redirect()->back()->with('error','Only 2 bonus questions are allowed');

                    }
                    $QuestionTf->bonus=($request->isBonus == "on")?true:false;
                }
                // if($request->has('Answer')){
                //     $QuestionTf->is_correct=true;
                // }else{
                //     $QuestionTf->is_correct=false;
                //    }
                $QuestionTf->quiz_id=$request->quiz_id;
                $QuestionTf->save();
               return redirect()->route('admin.showTf',$request->quiz_id)->with('success','True/False question is added successfully');
            }
            return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = QuestionTf::where('id',$id)->first();
        if(!empty($check)){
            return view('admin.question.editTf',['tf' => $check]);

        }
        return redirect()->back()->with('error','Question not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $QuestionTf = QuestionTf::where('id',$id)->first();
        if(!empty($QuestionTf)){
            $this->validate($request,[
                'question' => 'required|string|max:255',
                'Answer' => 'sometimes|in:1,2',
                'isBonus' => 'sometimes|in:on',
               ]);
                
               $QuestionTf->Question=$request->question;
                if($request->isBonus){
                    if($QuestionTf->bonus != true){
                        if($this->checkBonusExist($QuestionTf->quiz_id) >= 2){
                            return redirect()->back()->with('error','Only 2 bonus questions are allowed');
                        }
                   }
                    $QuestionTf->bonus=($request->isBonus == "on")?true:false;
                }else{
                    $QuestionTf->bonus=false;
                }
               if($request->has('Answer')){
                   $QuestionTf->is_correct=true;
               }else{
                $QuestionTf->is_correct=false;
               }
               $QuestionTf->save();
              return redirect()->route('admin.showTf',$QuestionTf->quiz_id)->with('success','True/False question is updated successfully');
        }
        return redirect()->route('admin.showTf',$QuestionTf->quiz_id)->with('error','True/False question not found.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        $check = QuestionTf::where('id', $id)->first();
        if (!empty($check)) {
            $TfSubmitAnswer = TfSubmitAnswer::where('question_tf_id',$id)->count();
            // $VsSubmitAnswer = VsSubmitAnswer::where('quiz_id',$id)->count();
            if($TfSubmitAnswer > 0 ){
                
                return redirect()->back()->with('error','You are not allow to delete question because user submit answers against this question');
            }
            $destroy = QuestionTf::destroy($id);
            if($destroy){
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return  redirect()->back()->with('error','Record not found.');
        }
    }

    function checkPostedQuestionCount($id){
        $vsCount = QuestionVs::where('quiz_id',$id)->count();
        $mcqCount = QuestionMc::where('quiz_id',$id)->count();
        $tfCount = QuestionTf::where('quiz_id',$id)->count();
        return $mcqCount + $tfCount+$vsCount;
    }
    function checkBonusExist($id){
        $mcqCount = QuestionMc::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $tfCount = QuestionTf::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $vsCount = QuestionVs::where(['quiz_id'=>$id,'bonus'=>true])->count();
        return $mcqCount + $tfCount+$vsCount;
    }

}

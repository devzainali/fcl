<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\Competition;

class TransactionController extends Controller
{
    public function getCompetitionTransaction($id){
        $getTransactions = Transaction::where('competition_id',$id)->with(['competition','user'])->paginate(20);
    return view('admin.transaction.transaction',['transactions'=>$getTransactions]);
    }
    public function getTransactionByCompetition($competition_id){
        $competitions = Competition::orderBy('id', 'desc')->get();
        $check = Competition::where('id',$competition_id)->first();
        if($check){
            $getTransactions = Transaction::where('competition_id',$competition_id)->with(['competition','user'])->paginate(1);
            return view('admin.transaction.transactionByCompetitionSelect',['transactions'=>$getTransactions,'competitions' => $competitions,'quizCompetition'=>$competition_id]);    
        }
    return redirect()->back()->with('error','Competition Not found');
    }
    public function getCompetitionForSelectBox(){
        $competitions = Competition::orderBy('id', 'desc')->get();
        return view('admin.transaction.transactionByCompetitionSelect',['competitions' => $competitions]);
    }
    public function competitionList(){
        $competitions = Competition::orderBy('id', 'desc')->paginate(20);
        return view('admin.transaction.competitionList',['competitons' => $competitions]);
    }

}

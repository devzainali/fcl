<?php

namespace App\Http\Controllers\Admin;

use App\Quiz;
use App\QuestionVs;
use App\QuestionMc;
use App\QuestionTf;
use App\VsSubmitAnswer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class VerusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $quiz = Quiz::where('id',$id)->with('QuestionVs')->first();
        if(!empty($quiz)){
            return view('admin.question.vs',compact('quiz'));
        }
        return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'question' => 'required|string|max:255',
            'VerusName1' => 'required|string|max:255',
            'Vimage1' => 'required|mimes:jpeg,png',
            'VerusName2' => 'required|string|max:255',
            'Vimage2' => 'required|mimes:jpeg,png',
            'answer' => 'sometimes|string',
            'quiz_id' => 'required|string',
            'isBonus' => 'sometimes|in:on'
           ]);

           $check = Quiz::where('id',$request->quiz_id)->first();
            if (!empty($check)) {
                if($this->checkPostedQuestionCount($request->quiz_id) >= 10){
                    return redirect()->back()->with('error','Question limit reached.');
    
                }
                $QuestionVs = new QuestionVs();
                $QuestionVs->Question=$request->question;
                $QuestionVs->v1Name=$request->VerusName1;
                $QuestionVs->v2Name=$request->VerusName2;
                if($request->isBonus){
                    if($this->checkBonusExist($request->quiz_id) >= 2){
                        return redirect()->back()->with('error','Only 2 bonus questions are allowed');
    
                    }
                    $QuestionVs->bonus=($request->isBonus == "on")?true:false;
                   }
                if($request->has('Vimage1')){
                    $QuestionVs->v1image =$request->Vimage1->store('verus');
                }
                if($request->has('Vimage2')){
                    $QuestionVs->v2image =$request->Vimage2->store('verus');
                }
                if($request->has('answer')){
                    $QuestionVs->answer=$request->answer;
                }
                $QuestionVs->quiz_id=$request->quiz_id;
                $QuestionVs->save();
               return redirect()->route('admin.showVs',$request->quiz_id)->with('success','versus question is added successfully');
            }
            return redirect()->route('admin.showQuiz')->with('error','Quiz not found.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = QuestionVs::where('id',$id)->first();
        if(!empty($check)){
            return view('admin.question.editvs',['value' => $check]);

        }
        return redirect()->back()->with('error','versus Question not found.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $QuestionVs = QuestionVs::where('id',$id)->first();
        if(!empty($QuestionVs)){
            if($request->answer== null){
                $request->request->remove('answer');
           }
            $this->validate($request,[
                'question' => 'required|string|max:255',
                'VerusName1' => 'required|string|max:255',
                'Vimage1' => 'sometimes|mimes:jpeg,png',
                'VerusName2' => 'required|string|max:255',
                'Vimage2' => 'sometimes|mimes:jpeg,png',
                'answer' => 'sometimes|in:1,2',
                'quiz_id' => 'required|string',
                'isBonus' => 'sometimes|in:on'
               ]);
               $QuestionVs->Question=$request->question;
                $QuestionVs->v1Name=$request->VerusName1;
                $QuestionVs->v2Name=$request->VerusName2;
                if($request->isBonus){
                    if($QuestionVs->bonus != true){
                        if($this->checkBonusExist($QuestionVs->quiz_id) >= 2){
                            return redirect()->back()->with('error','Only 2 bonus questions are allowed');
                        }
                   }
                    $QuestionVs->bonus=($request->isBonus == "on")?true:false;
                   }else{
                    $QuestionVs->bonus=false;
                   }
                if($request->has('Vimage1')){
                    @Storage::delete($QuestionVs->Vimage1);
                    $QuestionVs->v1image =$request->Vimage1->store('verus');
                }
                if($request->has('Vimage2')){
                    @Storage::delete($QuestionVs->v2image);
                    $QuestionVs->v2image =$request->Vimage2->store('verus');
                }
                if($request->has('answer')){
                    $QuestionVs->answer=$request->answer;
                }
                $QuestionVs->save();
               return redirect()->route('admin.showVs',$QuestionVs->quiz_id)->with('success','versus question is updated successfully');
            }
            return redirect()->route('admin.showVs',$QuestionVs->quiz_id)->with('error','versus Question not found.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id){
        $check = QuestionVs::where('id', $id)->first();
        if (!empty($check)) {
            $VsSubmitAnswer = VsSubmitAnswer::where('question_vs_id',$id)->count();
            if($VsSubmitAnswer > 0 ){
                
                return redirect()->back()->with('error','You are not allow to delete question because user submit answers against this question');
            }
            $destroy = QuestionVs::destroy($id);
            if($destroy){
                @Storage::delete($check->v1image);
                @Storage::delete($check->v2Name);
                return redirect()->back()->with('success','Deleted successfully.');
             }
        }else{
            return  redirect()->back()->with('error','Record not found.');
        }
    }

    function checkPostedQuestionCount($id){
        $vsCount = QuestionVs::where('quiz_id',$id)->count();
        $mcqCount = QuestionMc::where('quiz_id',$id)->count();
        $tfCount = QuestionTf::where('quiz_id',$id)->count();
        return $mcqCount + $tfCount+$vsCount;
    }

    function checkBonusExist($id){
        $mcqCount = QuestionMc::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $tfCount = QuestionTf::where(['quiz_id'=>$id,'bonus'=>true])->count();
        $vsCount = QuestionVs::where(['quiz_id'=>$id,'bonus'=>true])->count();
        return $mcqCount + $tfCount+$vsCount;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\AppStat;
use App\AddStat;

class AppStatController extends Controller
{
    public function addStat(Request $request){
        $validator =  Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'deviceType' => 'required|string|max:255',
            'deviceModel' => 'required|string|max:255',
            'appVersion' => 'required|string|max:255',
            'appBuild' => 'required|string|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
                 $contact = AppStat::create([
                'name' => $request->name,
                'deviceType' => $request->deviceType,
                'deviceModel' => $request->deviceModel,
                'appVersion' => $request->appVersion,
                'appBuild' => $request->appBuild,
                'user_id' => auth("api")->user()->id,
            ]);
     
            return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => null,
            ], 200);
    }

    public function adStat(Request $request){
        $validator =  Validator::make($request->all(),[
            'type' => 'required|string|in:General,Competition|max:255',
            'deviceType' => 'required|string|max:255',
            'deviceModel' => 'required|string|max:255',
            'appVersion' => 'required|string|max:255',
            'appBuild' => 'required|string|max:255',
            'wasClicked' => 'required|boolean|max:255',
            'general_ad_id' => 'sometimes|numeric|max:255',
            'competition_native_ad_id' => 'sometimes|numeric|max:255',
        ],
        [   
            'type.in'    => 'Type is invalid. Allowed types are General and Competition.'
        ]
    );
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }

            $stat = new AddStat();
            $stat->type = $request->type;
            $stat->deviceType =$request->deviceType;
            $stat->deviceModel = $request->deviceModel;
            $stat->appVersion =$request->appVersion;
            $stat->appBuild = $request->appBuild;
            $stat->user_id =auth("api")->user()->id;
            $stat->wasClicked = $request->wasClicked;
            if(!empty($request->general_ad_id)){
                $stat->general_ad_id = $request->general_ad_id;
            }
            if(!empty($request->competition_native_ad_id)){
                $stat->competition_native_ad_id = $request->competition_native_ad_id;
            }
            $stat->save(); 
     
            return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => null,
            ], 200);
    }
}

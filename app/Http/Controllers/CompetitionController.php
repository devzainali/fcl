<?php

namespace App\Http\Controllers;
use App\Competition;
use App\Gift;
use App\Quiz;
use App\McqSubmitAnswer;
use App\TfSubmitAnswer;
use App\VsSubmitAnswer;
use App\CompetitionNativeAdd;
use App\userCompetition;
use App\Transaction;
use App\PushNotification;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class CompetitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $competition = Competition::where('status',true)->orderBy('id', 'desc')
        ->with(['subscribe' => function($q) {
            $q->where('user_id', '=', auth()->user()->id);
        }])
        ->paginate(20);
        return response()->json([
            "status"=>true,
            'data' => $competition,
            "errorMessage" => null,
            ], 200);
    }

    public function getLeagueScoreList(Request $request,$id){
        $itemsPaginated = userCompetition::where('competition_id',$id)->with(['user'])->paginate(20);
       
        $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
                $item->overAllTotal = $item->user->getUserScore($item->competition_id,$item->user->id); 
                $item->currentWeekScore = $item->user->getCurrentWeekScore($item->competition_id,$item->user->id); 
                return $item;
            })->toArray();
            usort($itemsTransformed, function ($item1, $item2) {
                return $item2['overAllTotal'] <=> $item1['overAllTotal'];
            });
            $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
                $itemsTransformed,
                $itemsPaginated->total(),
                $itemsPaginated->perPage(),
                $itemsPaginated->currentPage(), [
                    'path' => \Request::url(),
                    'query' => [
                        'page' => $itemsPaginated->currentPage()
                    ]
                ]
            );
        return response()->json([
            "status"=>true,
            'data' => $itemsTransformedAndPaginated,
            "errorMessage" => 'Competition not found.',
            ], 200);
    }

    public function getCompetitionScore(Request $request,$id)
    {
        $getCompetition = Competition::where('id',$id)->with(['subscriptions','subscriptions.user','winners','winners'])->first();
        if(!empty($getCompetition)){
            $itemsPaginated = userCompetition::where('competition_id',$getCompetition->id)->with('user')->paginate(30);
            $itemsTransformed = $itemsPaginated
            ->getCollection()
            ->map(function($item) {
                    $item->score = $item->user->getUserScore($item->competition_id,$item->user->id); 
                    return $item;
                })->toArray();
                $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $itemsPaginated->total(),
                    $itemsPaginated->perPage(),
                    $itemsPaginated->currentPage(), [
                        'path' => \Request::url(),
                        'query' => [
                            'page' => $itemsPaginated->currentPage()
                        ]
                    ]
                );
            return response()->json([
                "status"=>true,
                'data' => $itemsTransformedAndPaginated,
                "errorMessage" => null,
                ], 200);
            
        }
        return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => 'Competition not found.',
            ], 200);
    }

    public function subscribeCompetition(Request $request){

        $validator =  Validator::make($request->all(),[
            'competitionID' => 'required|numeric',
            'paymentID' => 'required|string',
            'period' => 'required|numeric',
            'method' => 'required|in:k-net',
            'amount' => 'required|string'
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $checkCompetitionExist = Competition::where('id', $request->competitionID)->first();
            if(empty($checkCompetitionExist)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Competition Not Found',
                    ], 200);
            }
            $checkAlreadySubscribed = userCompetition::where(['user_id' => auth("api")->user()->id,'competition_id' => $request->competitionID])->first();
            if(!empty($checkAlreadySubscribed)){
                $expire = strtotime($checkAlreadySubscribed->expireAt);
                $today = strtotime("today midnight");
                if($today >= $expire){
                    $checkAlreadySubscribed->destroy();
                    $transaction = Transaction::create([
                        'competition_id' => $request->competitionID,
                        'user_id' => auth("api")->user()->id,
                        'payment_id' => $request->paymentID,
                        'method' => $request->method,
                        'amount' => $request->amount,
                    ]);
                    $effectiveDate = strtotime("+".$request->period." months", strtotime(now()));
                    $effectiveDate = strftime ( '%Y-%m-%d' , $effectiveDate );
                    $subscribe = userCompetition::create([
                        'competition_id' => $request->competitionID,
                        'user_id' => auth("api")->user()->id,
                        'expireAt' => $effectiveDate,
                        'transaction_id' => $transaction->id,
                    ]);
                    return response()->json([
                    "status"=>true,
                    'data' => $subscribe,
                    "errorMessage" => null,
                    ], 200);
                } else {
                    return response()->json([
                        "status"=>false,
                        'data' => null,
                        "errorMessage" => 'subscription already exist',
                        ], 200);

                }
            }
            $transaction = Transaction::create([
                'competition_id' => $request->competitionID,
                'user_id' => auth("api")->user()->id,
                'payment_id' => $request->paymentID,
                'method' => $request->method,
                'amount' => $request->amount,
            ]);
            $effectiveDate = strtotime("+".$request->period." months", strtotime(now()));
            $effectiveDate = strftime ( '%Y-%m-%d' , $effectiveDate );
            $subscribe = userCompetition::create([
                'competition_id' => $request->competitionID,
                'user_id' => auth("api")->user()->id,
                'expireAt' => $effectiveDate,
                'transaction_id' => $transaction->id,
            ]);
     
            $notification = new PushNotification();
            $notification->message = 'You have successfully subscribe '.$request->period.' month subscription of competition ( '.$checkCompetitionExist->name.' ) ';
            $notification->user_id = $request->user_id;
            $notification->type = 'user';
            $notification->save();
            return response()->json([
            "status"=>true,
            'data' => $subscribe,
            "errorMessage" => null,
            ], 200);


    }

    public function subscribeFreeCompetition(Request $request){

        $validator =  Validator::make($request->all(),[
            'competitionID' => 'required|numeric'
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $checkCompetitionExist = Competition::where('id', $request->competitionID)->first();
            if(empty($checkCompetitionExist)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Competition Not Found',
                    ], 200);
            }
            $checkAlreadySubscribed = userCompetition::where(['user_id' => auth("api")->user()->id,'competition_id' => $request->competitionID])->first();
            if(!empty($checkAlreadySubscribed)){
                $expire = strtotime($checkAlreadySubscribed->expireAt);
                $today = strtotime("today midnight");
                if($today >= $expire){
                    $checkAlreadySubscribed->destroy();
                    $subscribe = userCompetition::create([
                        'competition_id' => $request->competitionID,
                        'user_id' => auth("api")->user()->id,
                        'expireAt' => $checkCompetitionExist->expired_at,
                        // 'transaction_id' => 6,
                    ]);
                    return response()->json([
                    "status"=>true,
                    'data' => $subscribe,
                    "errorMessage" => null,
                    ], 200);
                } else {
                    return response()->json([
                        "status"=>false,
                        'data' => null,
                        "errorMessage" => 'subscription already exist',
                        ], 200);

                }
            }
            $subscribe = userCompetition::create([
                'competition_id' => $request->competitionID,
                'user_id' => auth("api")->user()->id,
                'expireAt' => $checkCompetitionExist->expired_at,
                // 'transaction_id' => 6,
            ]);
     
            $notification = new PushNotification();
            $notification->message = 'You have successfully subscribe subscription of competition ( '.$checkCompetitionExist->name.' ) ';
            $notification->user_id = $request->user_id;
            $notification->type = 'user';
            $notification->save();
            return response()->json([
            "status"=>true,
            'data' => $subscribe,
            "errorMessage" => null,
            ], 200);


    }

    public function gifts($id)
    {
        //Storage::disk('public')->url($gift->image)
        $gift = Gift::where('competition_id',$id)
                ->orderBy('id', 'desc')
                ->paginate(20);
        return response()->json([
            "status"=>true,
            'data' => $gift,
            "errorMessage" => null,
            ], 200);
    }

    public function CompetitionWeeks($id)
    {
        $quiz = Quiz::where('competition_id',$id)
        ->paginate(20);
        return response()->json([
            "status"=>true,
            'data' => $quiz,
            "errorMessage" => null,
            ], 200);
    }


    public function CompetitionQuiz($id)
    {

        $quiz = Quiz::where('id',$id)
        ->with(['competition','QuestionMc','QuestionTf','QuestionVs'])
        ->first();
        if(empty($quiz)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'No Such quiz found.',
                ], 200);
        }
        $McqAnswer = McqSubmitAnswer::where(['user_id'=>auth()->user()->id,'quiz_id'=>$id])->get();
        $TfAnswer = TfSubmitAnswer::where(['user_id'=>auth()->user()->id,'quiz_id'=>$id])->get();
        $VsAnswer = VsSubmitAnswer::where(['user_id'=>auth()->user()->id,'quiz_id'=>$id])->get();
        $competitionNativeAdd = CompetitionNativeAdd::
        where(['competition_id'=>$quiz->competition_id])
        ->whereDate('startDate', '<=', date("Y-m-d"))
        ->whereDate('endDate', '>=', date("Y-m-d"))
        ->limit(5)
        ->get();
        $response = [];
        $response['UserMcqAnswer'] = $McqAnswer;
        $response['UserTfAnswer']  = $TfAnswer;
        $response['UserVsqAnswer'] = $VsAnswer;
        $response['competitionNativeAdd'] = $competitionNativeAdd;
        $array = array_merge($quiz->toArray(), $response);
        return response()->json([
            "status"=>true,
            'data' => $array,
            "errorMessage" => null,
            ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

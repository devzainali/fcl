<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LiveScore;

class LiveScoreController extends Controller
{
    public function index(){
        $score = LiveScore::first();
        return response()->json([
            "status"=>true,
            'data' => $score,
            "errorMessage" => null,
            ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PushNotification;

class PushNotificationController extends Controller
{
    
    public function getUserNotification(){
        $notification = PushNotification::orWhere([
            'type'=>'user',
            'type'=>'admin',
            'user_id'=> auth()->user()->id,
          ])
        ->orderBy('id', 'desc')->paginate(30);
        return response()->json([
            "status"=>true,
            'data' => $notification,
            "errorMessage" => null,
            ], 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use App\ResetPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;


class ResetPasswordController extends Controller
{
    public function request(Request $request){
        $validator =  Validator::make($request->all(),[
            'email' => 'required|string|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $user = User::where(function ($query) use ($request) {
                $query->orWhere('email', '=', $request->email)
                      ->orWhere('username', '=', $request->email);
            })->first();
            if(empty($user)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Email or username not exist',
                    ], 200);
            }

        ResetPassword::where('email',$user->email)->delete();
        $fourRandomDigit = mt_rand(1000,9999);
        Mail::send('emails.forgot', ['user' => $user,'code' => $fourRandomDigit], function ($m) use ($user) {
            $m->from('noreply@fclkw.com', 'FCL');
            $m->to($user->email, $user->name)->subject('Forgot Password');
            });
        $reset =  new ResetPassword();
        $reset->email = $user->email;
        $reset->token = $fourRandomDigit;
        $reset->created_at = now();
        $reset->save();
        return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => null,
            ], 200);
    }

    public function validateCode(Request $request){
        $validator =  Validator::make($request->all(),[
            'email' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $user = User::where(function ($query) use ($request) {
                $query->orWhere('email', '=', $request->email)
                      ->orWhere('username', '=', $request->email);
            })->first();
            if(empty($user)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Email or username not exist',
                    ], 200);
            }


           $reset =  ResetPassword::where('email',$user->email)->first();
           if(empty($reset)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Reset password Request not found',
                ], 200);    
           }

           if($reset->token != $request->code){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Code not Matched',
                ], 200); 
           }

           return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => null,
            ], 200); 
    }

    public function resetPassword(Request $request){
        $validator =  Validator::make($request->all(),[
            'email' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'newPassword' => 'required|string|min:6|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }

            $user = User::where(function ($query) use ($request) {
                $query->orWhere('email', '=', $request->email)
                      ->orWhere('username', '=', $request->email);
            })->first();
            if(empty($user)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Email or username not exist',
                    ], 200);
            }

            $reset =  ResetPassword::where('email',$user->email)->first();
           if(empty($reset)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Reset password Request not found',
                ], 200);    
           }

           if($reset->token != $request->code){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Code not Matched',
                ], 200); 
           }

           $user = User::where('email',$user->email)->first();
           if(empty($user)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'No User found against this email address',
                ], 200);  
           }
           $user->password = bcrypt($request->newPassword);
           $user->save();
           ResetPassword::where('email',$request->email)->delete();
           return response()->json([
            "status"=>true,
            'data' => null,
            "errorMessage" => null,
            ], 200);

    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\social;

class SocialController extends Controller
{
    public function index()
    {
        $social = social::orderBy('id', 'desc')->get();
        return response()->json([
            "status"=>true,
            'data' => $social,
            "errorMessage" => null,
            ], 200);
    }
}

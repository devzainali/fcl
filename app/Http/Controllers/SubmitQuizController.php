<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Quiz;
use App\Competition;
use App\QuestionMc;
use App\QuestionTf;
use App\QuestionVs;
use App\McqSubmitAnswer;
use App\TfSubmitAnswer;
use App\VsSubmitAnswer;
use App\QuizResult;

class SubmitQuizController extends Controller
{

    public function checkQuizScoreList(Request $request,$id){
        $quiz = Quiz::where('id',$id)->first();
        if(!$quiz){
            return response()->json([
                "status"=>false,
                "data"=>null,
                "errorMessage" => 'Quiz not found',
            ], 422);
        }
       $itemsPaginated=  QuizResult::where('quiz_id',$id)->with('user')->paginate(30);
        $itemsTransformed = $itemsPaginated
            ->getCollection()
            ->map(function($item) {
                    if($item->mcqPoint + $item->vsPoint +$item->tfPoint == 120){
                        $item->score = 150;
                    }else{
                        $item->score = $item->mcqPoint + $item->vsPoint +$item->tfPoint;
                    }
                    return $item;
                })->toArray();
                if(count($itemsPaginated) > 0){

                
                $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
                    $itemsTransformed,
                    $itemsPaginated->total(),
                    $itemsPaginated->perPage(),
                    $itemsPaginated->currentPage(), [
                        'path' => \Request::url(),
                        'query' => [
                            'page' => $itemsPaginated->currentPage()
                        ]
                    ]
                );
            return response()->json([
                "status"=>true,
                'data' => $itemsTransformedAndPaginated,
                "errorMessage" => null,
                ], 200);

            }
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Quiz result not calculated yet.check another time.',
                ], 200);

    }

    public function submitQuiz(Request $request){
        $validator =  Validator::make($request->all(),[
            "McqQuestionID"    => "required|array",
            "McqAnswer"         => "required|array",
            "TfQuestionID"    => "required|array",
            "TfAnswer"         => "required|array",
            "VsQuestionID"    => "required|array",
            "VsAnswer"         => "required|array",
            "QuizID"         => "required|numeric",
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }

            $quiz = Quiz::where('id',$request->QuizID)->first();
            if(!$quiz){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => 'Quiz not found',
                ], 422);
            }
            $expire = strtotime($quiz->expired_at);
            $today = strtotime("today midnight");
            if($today >= $expire){
				return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => 'Quiz time is over.',
                ], 422);
            }
            foreach($request->McqQuestionID as $key=>$value):
                $check = QuestionMc::where('id',$request->McqQuestionID[$key])->first();
                if($check){
                    $Mcq= McqSubmitAnswer::firstOrNew(['question_mc_id' => $request->McqQuestionID[$key],'user_id'=>auth()->user()->id]);
                    if($Mcq->id) {
                        $Mcq->question_mc_id = $request->McqQuestionID[$key];
                        $Mcq->user_id = auth()->user()->id;
                        $Mcq->answer = $request->McqAnswer[$key];
                        $Mcq->quiz_id = $request->QuizID;
                        $Mcq->save();
                    }
                    if(!$Mcq->id) {
                        $Mcq->question_mc_id = $request->McqQuestionID[$key];
                        $Mcq->user_id = auth()->user()->id;
                        $Mcq->answer = $request->McqAnswer[$key];
                        $Mcq->quiz_id = $request->QuizID;
                        $Mcq->save();
                    }
                }

            endforeach;

            foreach($request->TfQuestionID as $key=>$value):
                $check = QuestionTf::where('id',$request->TfQuestionID[$key])->first();
                if($check){
                $Tf= TfSubmitAnswer::firstOrNew(['question_tf_id' => $request->TfQuestionID[$key],'user_id'=>auth()->user()->id]);
                if($Tf->id) {
                    $Tf->question_tf_id  = $request->TfQuestionID[$key];
                    $Tf->user_id = auth()->user()->id;
                    $Tf->answer = $request->TfAnswer[$key];
                    $Tf->quiz_id = $request->QuizID;
                    $Tf->save();
                }
                if(!$Tf->id) {
                    $Tf->question_tf_id  = $request->TfQuestionID[$key];
                    $Tf->user_id = auth()->user()->id;
                    $Tf->answer = $request->TfAnswer[$key];
                    $Tf->quiz_id = $request->QuizID;
                    $Tf->save();
                }
                }
            endforeach;

            foreach($request->VsQuestionID as $key=>$value):
                $check = QuestionVs::where('id',$request->VsQuestionID[$key])->first();
                if($check){
                $Vs= VsSubmitAnswer::firstOrNew(['question_vs_id' => $request->VsQuestionID[$key],'user_id'=>auth()->user()->id]);
                if($Vs->id) {
                    $Vs->question_vs_id   = $request->VsQuestionID[$key];
                    $Vs->user_id = auth()->user()->id;
                    $Vs->answer = $request->VsAnswer[$key];
                    $Vs->quiz_id = $request->QuizID;
                    $Vs->save();
                }
                if(!$Vs->id) {
                    $Vs->question_vs_id   = $request->VsQuestionID[$key];
                    $Vs->user_id = auth()->user()->id;
                    $Vs->answer = $request->VsAnswer[$key];
                    $Vs->quiz_id = $request->QuizID;
                    $Vs->save();
                }
                }
            endforeach;

            return response()->json([
                "status"=>true,
                'data' => null,
                "errorMessage" => null,
                ], 200);
    }

    public function showResult(Request $request,$id){
        
        $quiz = Quiz::where('id',$id)
        ->with(['QuestionMc','QuestionTf','QuestionVs','QuestionMc.McqAnswers' => function($q) {
            $q->where('user_id', '=', auth()->user()->id);
        },'QuestionTf.TfAnswers' => function($q) {
            $q->where('user_id', '=', auth()->user()->id);
        },'QuestionVs.VsAnswers' => function($q) {
            $q->where('user_id', '=', auth()->user()->id);
        }])
        ->first();
        
       if(empty($quiz)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'No Such quiz found.',
                ], 200);
        }

            $expire = strtotime($quiz->expired_at);
            $today = strtotime("today midnight");
            if($today >= $expire){
             $resultMcqResult = [];
                foreach($quiz->QuestionMc as $key=>$question):
                    $resultMcq = [];
                    $answer = 'option'.$question->answer;
                    $resultMcq['question'] = $question->Question;
                    $resultMcq['option1'] = $question->option1;
                    $resultMcq['option2'] = $question->option2;
                    $resultMcq['option3'] = $question->option3;
                    $resultMcq['option4'] = $question->option4;
                    $resultMcq['answer'] = $question->$answer;
                    if(count($question->McqAnswers) > 0){
                        $answercome = 'option'.$question->McqAnswers[0]->answer;
                        if($question->$answer == $question->$answercome){
                            $resultMcq['answerStatus'] = true;
                        }else{
                            $resultMcq['answerStatus'] = false;
                        }
                        $resultMcq['userAnswer'] = $question->$answercome;
                       }else{
                        $resultMcq['answerStatus'] = false;
                        $resultMcq['userAnswer'] = 'Not attempted';
                       }
                    array_push($resultMcqResult,$resultMcq);
                endforeach;
                //True False
                $resultMTFResult = [];
                foreach($quiz->QuestionTf as $key=>$question):
                    $resultMTF = [];
                    $answer = $question->is_correct;
                    $resultMTF['question'] = $question->Question;
                    $resultMTF['answer'] = ($question->is_correct == 1)?true:false;
                    if(count($question->TfAnswers) > 0){
                        $answercome = $question->TfAnswers[0]->answer;
                        if(json_encode($question->is_correct) == $question->TfAnswers[0]->answer){
                            $resultMTF['answerStatus'] = true;
                        }else{
                            $resultMTF['answerStatus'] = false;
                        }
                        $resultMTF['userAnswer'] = ($question->TfAnswers[0]->answer== '1')?true:false;

                    }else{
                        $resultMTF['answerStatus'] = false;
                        $resultMTF['userAnswer'] = 'Not Attempted';
                    }   

                    array_push($resultMTFResult,$resultMTF);
                endforeach;

                //Verus Question
                $resultVerusResult = [];
                foreach($quiz->QuestionVs as $key=>$question):
                    $resultVerus = [];
                    $answer = 'v'.$question->answer.'Name';
                    $resultVerus['question'] = $question->Question;
                    $resultVerus['answer'] = $question->$answer;
                    if(count($question->VsAnswers) > 0){
                        $answercomesUser = 'v'.$question->VsAnswers[0]->answer.'Name';
                        if($question->$answer == $question->$answercomesUser){
                            $resultVerus['answerStatus'] = true;
                        }else{
                            $resultVerus['answerStatus'] = false;
                        }
                        $resultVerus['userAnswer'] = $question->$answercomesUser;
                    }else{
                        $resultVerus['answerStatus'] = false;
                        $resultVerus['userAnswer'] = 'Not Attempted';

                    }   

                    array_push($resultVerusResult,$resultVerus);
                endforeach;
                
                $response = [];
                $response['McqResult'] = $resultMcqResult;
                $response['TfResult'] = $resultMTFResult;
                $response['VsResult'] = $resultVerusResult;
                return response()->json([
                    "status"=>true,
                    "data"=>$response,
                    "errorMessage" => null,
                ], 422);
            }
            return response()->json([
                "status"=>false,
                "data"=>null,
                "errorMessage" => 'Quiz time is not over Yet.',
            ], 422);

    }

}

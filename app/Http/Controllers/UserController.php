<?php

namespace App\Http\Controllers;

use App\User;
use App\Verify;
use App\userCompetition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use DateTime;
use Auth;
use Hash;
class UserController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $validator =  Validator::make($request->all(),[
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'dob' => 'required|string|max:255',
            'countryCode' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'username' => 'required|string|max:255|unique:users,username',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
                 $user = User::create([
                'f_name' => $request->fname,
                'l_name' => $request->lname,
                'dob' => $request->dob,
                'phone' => $request->phone,
                'countryCode' => $request->countryCode,
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);
     
            $token = $user->createToken('TutsForWeb')->accessToken;
            $fourRandomDigit = mt_rand(1000,9999);
            $date = new DateTime();
            $verify = new Verify();
            $verify->user_id = $user->id;
            $verify->token = $fourRandomDigit;
            $verify->expired_at = $date->format('Y-m-d H:i:s').PHP_EOL;
            $verify->save();
            Mail::send('emails.activation', ['user' => $user,'code' => $fourRandomDigit], function ($m) use ($user) {
                        $m->from('noreply@fclkw.com', 'FCL');
                        $m->to($user->email, $user->name)->subject('Activation Email');
                        });
    
            return response()->json([
            "status"=>true,
            'data' => $token,
            "errorMessage" => null,
            ], 200);
    }
 
    public function resendVerification(Request $request){
        $validator =  Validator::make($request->all(),[
            'email' => 'required|string|email|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
        $user = User::where('email',$request->email)->first();
        if(!$user){
            return response()->json([
                "status"=>false,
                "data"=>null,
                "errorMessage" => 'No user found against this email.',
            ], 422);
        }
        if(!is_null($user->email_verified_at)){
            return response()->json([
                "status"=>false,
                "data"=>null,
                "errorMessage" => 'Email is already verified.',
            ], 422);
        }
            $date = new DateTime();
            $fourRandomDigit = mt_rand(1000,9999);
            $delete = Verify::where('user_id',$user->id)->get();
            Verify::destroy($delete);
            $verify = new Verify();
            $verify->user_id = $user->id;
            $verify->token = $fourRandomDigit;
            $verify->expired_at = $date->format('Y-m-d H:i:s').PHP_EOL;
            $verify->save();
             Mail::send('emails.activation', ['user' => $user,'code' => $fourRandomDigit], function ($m) use ($user) {
                        $m->from('noreply@fclkw.com', 'FCL');
                        $m->to($user->email, $user->name)->subject('Activation Email');
                        });

            return response()->json([
                "status"=>true,
                'data' => null,
                "errorMessage" => null,
                ], 200);
    }

    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $user = User::where(function ($query) use ($request) {
            $query->orWhere('email', '=', $request->email)
                  ->orWhere('username', '=', $request->email);
        })->first();
        if(empty($user)){
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'Email or username not exist',
                ], 200);
        }
        $credentials = [
            'email' => $user->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            if(is_null(auth()->user()->email_verified_at)){
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Your email address is not verified.',
                    ], 200);
            }
            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
            return response()->json([
                "status"=>true,
                'data' => $token,
                "errorMessage" => null,
                ], 200);
        } else {
            // return response()->json(['error' => 'UnAuthorised'], 401);
            return response()->json([
                "status"=>false,
                'data' => null,
                "errorMessage" => 'UnAuthorised',
                ], 200);
        }
    }
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function details()
    {
        return response()->json([
            "status"=>true,
            "data"=> auth("api")->user(),
            "errorMessage" =>null,
            ], 200);
    }

    public function changeProfileImage(Request $request){
        $validator =  Validator::make($request->all(),[
            'image' => 'required|mimes:jpeg,png'
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }

            $user = User::where('id', auth("api")->user()->id)->first();
            if($request->has('image')){
                @Storage::delete($user->image);
               $user->image =$request->image->store('profile');
           }
           $user->save();
            return response()->json([
                "status"=>true,
                "data"=> $user,
                "errorMessage" =>null,
                ], 200);
    }

    public function activateAccount(Request $request){
        $validator =  Validator::make($request->all(),[
            'code' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }

            $verify = Verify::where('token', $request->code)->with('User')->first();
            if(empty($verify)){
                return response()->json([
                    "status"=>false,
                    "data"=> null,
                    "errorMessage" =>'No such code exist',
                    ], 200);
            }

            if($verify->user->email == $request->email){
                $user = User::where('id',$verify->user->id)->first();
                $user->email_verified_at = now();
                $user->save();
                $destroy = Verify::destroy($verify->id);
                    return response()->json(
                        [
                            "status"=>true,
                            "data"=> null,
                            "errorMessage" =>null,
                        ]);

            }
            return response()->json([
                "status"=>false,
                "data"=> null,
                "errorMessage" =>'Email not Matched.',
                ], 200);

            
        //    $user->save();
            return response()->json([
                "status"=>true,
                "data"=> $user,
                "errorMessage" =>null,
                ], 200);

    }

    public function updateProfile(Request $request){
        $validator =  Validator::make($request->all(),[
            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'dob' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'countryCode' => 'required|string|max:255',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $user = User::where('id',auth("api")->user()->id)->first();
            $user->f_name = $request->fname;
            $user->l_name = $request->lname;
            $user->dob = $request->dob;
            $user->phone = $request->phone;
            $user->countryCode = $request->countryCode;
            $user->save();
            return response()->json([
            "status"=>true,
            'data' => $user,
            "errorMessage" => null,
            ], 200);
  
    }

    public function changePassword(Request $request){
        $validator =  Validator::make($request->all(),[
            'oldPassword' => 'required|string|min:6|max:255',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|string|min:6',
            ]);
        
            if($validator->fails()){
                return response()->json([
                    "status"=>false,
                    "data"=>null,
                    "errorMessage" => $validator->errors()->first(),
                ], 422);
            }
            $credentials = [
                'email' => auth("api")->user()->email,
                'password' => $request->oldPassword
            ];

            if ((Hash::check(request('oldPassword'), auth("api")->user()->password)) == true) {
                $user = User::where('id',auth("api")->user()->id)->first();
                $user->password = bcrypt($request->password);
                $user->save();
                return response()->json([
                    "status"=>true,
                    'data' => null,
                    "errorMessage" => null,
                    ], 200);
            } else {
                return response()->json([
                    "status"=>false,
                    'data' => null,
                    "errorMessage" => 'Old password is not correct.',
                    ], 200);
            }
    }

    public function getUserSubscription(){
        $getUserSubscriptions = userCompetition::where('user_id',auth()->user()->id)->with(['competition'])->paginate(20);
        return response()->json([
            "status"=>true,
            'data' => $getUserSubscriptions,
            "errorMessage" => null,
            ], 200);
    }


}

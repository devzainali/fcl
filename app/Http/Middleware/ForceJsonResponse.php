<?php

namespace App\Http\Middleware;

use Closure;
use Request;

class ForceJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Request::wantsJson()) {
            $request->headers->set('Accept', 'application/json');
        }
        return $next($request);
    }
    
}

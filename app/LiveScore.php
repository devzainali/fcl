<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveScore extends Model
{
    protected $fillable = ['url'];
    protected $table = 'live_scores';
}

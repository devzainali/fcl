<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class McqSubmitAnswer extends Model
{
    protected $fillable = ['user_id','question_mc_id','answer'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

   
}

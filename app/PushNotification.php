<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{
    protected $fillable = ['message','type','user_id'];
    protected $table = 'push_notifications';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionMc extends Model
{
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function McqAnswers()
    {
        return $this->hasMany('App\McqSubmitAnswer');
    }

}

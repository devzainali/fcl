<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionTf extends Model
{
    
    public function TfAnswers()
    {
        return $this->hasMany('App\TfSubmitAnswer');
    }
}

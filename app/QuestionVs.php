<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionVs extends Model
{
    public function VsAnswers()
    {
        return $this->hasMany('App\VsSubmitAnswer');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    protected $fillable = ['title','description','competition_id','expired_at'];
    protected $table = 'quizes';

    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }
    public function QuestionMc()
    {
        return $this->hasMany('App\QuestionMc');
    }
    public function QuestionTf()
    {
        return $this->hasMany('App\QuestionTf');
    }
    public function QuestionVs()
    {
        return $this->hasMany('App\QuestionVs');
    }

    public function result(){
        return $this->hasMany('App\QuizResult');
    }
    public function isCalculated()
    {
        return $this->result()->where('quiz_id', $this->id)->count();
    }
}

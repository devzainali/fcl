<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model
{
    protected $fillable = ['quiz_id','user_id','mcqPoint','vsPoint','tfPoint'];
    protected $table = 'quiz_results';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function quiz()
    {
        return $this->belongsTo('App\Quiz');
    }
    public function userCompetitionResult(){
        return $this->belongsTo('App\Quiz');   
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $table = 'password_resets';
    public $timestamps = false;
    protected $fillabel = ['email','token','created_at'];
}

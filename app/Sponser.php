<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponser extends Model
{
    protected $fillable = ['competition_id','image','effective_date'];

    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TfSubmitAnswer extends Model
{
    protected $fillable = ['user_id','question_tf_id','answer'];
}

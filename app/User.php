<?php

namespace App;

use App\Quiz;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Carbon;


class User extends Authenticatable implements MustVerifyEmail
// class User extends Authenticatable
{
    use Notifiable, HasApiTokens;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name','l_name','dob', 'email','countryCode','phone','username', 'password','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function QuestionMcAnswer()
    {
        return $this->hasMany('App\McqSubmitAnswer');
    }

    public function getUserScore($competitionID,$userID){
        $getQuizIDs = Quiz::where('competition_id',$competitionID)->select('id')->pluck('id');
        if(!empty($getQuizIDs)){
            $getResult = QuizResult::whereIn('quiz_id',$getQuizIDs)
            ->where('user_id',$userID)
            ->get();
            if(!empty($getResult)){
                $score = 0;
                foreach($getResult as $key=>$value):
                    $calculate = $value->mcqPoint + $value->vsPoint + $value->tfPoint;
                    if($calculate == 120){
                        $score = $score + 150;
                    }else{
                        $score = $score + $calculate;
                    }
                endforeach;
                return $score;
               
            }
            return 0;

        }else{
            return 0;
        }

    }

    public function getCurrentWeekScore($competitionID,$userID){
        $getQuizIDs = Quiz::where('competition_id',$competitionID)
        ->whereYear('expired_at', '=', Carbon\Carbon::parse(date('y-m-d'))->year)
                ->whereMonth('expired_at', '=', Carbon\Carbon::parse(date('y-m-d'))->month)
                ->first();
        if(!empty($getQuizIDs)){
            $getResult = QuizResult::where('quiz_id',$getQuizIDs->id)
            ->where('user_id',$userID)
            ->get();
            if(!empty($getResult)){
                $score = 0;
                foreach($getResult as $key=>$value):
                    $calculate = $value->mcqPoint + $value->vsPoint + $value->tfPoint;
                    if($calculate == 120){
                        $score = $score + 150;
                    }else{
                        $score = $score + $calculate;
                    }
                endforeach;
                return $score;
               
            }
            return 0;

        }else{
            return 0;
        }
    }


}

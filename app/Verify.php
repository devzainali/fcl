<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verify extends Model
{
    protected $table = 'verify_users';
    protected $fillabel = ['user_id','token','expired_at'];

    public function User()
    {
        return $this->belongsTo('App\User');
    }
}

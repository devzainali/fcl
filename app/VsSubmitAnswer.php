<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VsSubmitAnswer extends Model
{
    protected $fillable = ['user_id','question_vs_id','answer'];
}

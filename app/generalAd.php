<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class generalAd extends Model
{
    protected $fillable = ['title','url','startDate','endDate','image'];
    protected $table = 'general_ads';
}

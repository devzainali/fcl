<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class userCompetition extends Model
{
    use SoftDeletes;
    protected $fillable = ['competition_id','user_id','expireAt','transaction_id'];
    protected $table = 'user_competitions';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function competition()
    {
        return $this->belongsTo('App\Competition');
    }
    public function transaction()
    {
        return $this->belongsTo('App\Transaction');
    }
   
 
}

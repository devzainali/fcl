<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_vs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('Question');
            $table->string('v1Name')->nullable();
            $table->string('v1image')->nullable();
            $table->string('v2Name')->nullable();
            $table->string('v2image')->nullable();
            $table->bigInteger('answer')->nullable();
            $table->boolean('bonus')->default(false);
            $table->unsignedBigInteger('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('id')->on('quizes')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_vs');
    }
}

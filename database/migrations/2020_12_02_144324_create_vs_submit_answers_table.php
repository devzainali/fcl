<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVsSubmitAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vs_submit_answers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
            $table->unsignedBigInteger('question_vs_id')->unsigned();
            $table->foreign('question_vs_id')->references('id')->on('question_vs') ->onDelete('cascade');
            $table->unsignedBigInteger('quiz_id')->unsigned();
            $table->foreign('quiz_id')->references('id')->on('quizes')
            ->onDelete('cascade');
            $table->string('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vs_submit_answers');
    }
}

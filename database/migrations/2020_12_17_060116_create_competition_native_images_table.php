<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionNativeImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_native_images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('competition_native_ad_id')->unsigned();
            $table->foreign('competition_native_ad_id')->references('id')->on('competition_native_ads')
            ->onDelete('cascade');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_native_images');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitionWinersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_winers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('competition_id')->unsigned();
            $table->foreign('competition_id')->references('id')->on('competitions')
            ->onDelete('cascade');
             $table->unsignedBigInteger('winner1')->unsigned();
            $table->foreign('winner1')->references('id')->on('users')
            ->onDelete('cascade');
            $table->unsignedBigInteger('winner2')->unsigned();
            $table->foreign('winner2')->references('id')->on('users')
            ->onDelete('cascade');
            $table->unsignedBigInteger('winner3')->unsigned();
            $table->foreign('winner3')->references('id')->on('users')
            ->onDelete('cascade');
            $table->date('date')->nullable();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competition_winers');
    }
}

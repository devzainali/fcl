<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddStatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_stats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type', array('General','Competition'));
            $table->string('deviceType');
            $table->string('deviceModel');
            $table->string('appVersion');
            $table->string('appBuild');
            $table->boolean('wasClicked')->default(false);
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
            $table->integer('general_ad_id')->unsigned()->nullable();
            $table->foreign('general_ad_id')->references('id')->on('general_ads');
            $table->integer('competition_native_ad_id')->unsigned()->nullable();
            $table->foreign('competition_native_ad_id')->references('id')->on('competition_native_ads');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_stats');
    }
}

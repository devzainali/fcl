$(document).ready(function () {
  $(".dt-checkboxes-cell").remove();
  $.ajaxSetup({
    headers: {
      "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
  });

  $("#btn-add-cource").submit(function (e) {
    e.preventDefault();
    alert("dsds");
    $.ajax({
      type: "POST",
      url: "/dashboard/add/cource",
      processData: false,
      contentType: false,
      data: new FormData(this),
      success: function (data) {
        let tableRow;
        if (data.status == true) {
          toastr.success(data.message);
          tableRow = `
        <tr id="courceID${data.cource.title}">
        <td class="product-img"><img src="http://testing.test/storage/${data.cource.image}" alt="${data.cource.title}">
        </td>
        <td class="product-name">${data.cource.title}</td>
        <td class="product-price">${data.cource.price}</td>
        </tr>`;
          $("#courceListBody").prepend(tableRow);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $(".cancel-data-btn").click();
      },
    });
  });
});

@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">({{$type}})Ad statistics list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of ad statistics
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped" id="transactionTable1">
                                    <thead>
                                        <tr>
                                            <th>Statistics ID</th>
                                            <th>Statistics Type</th>
                                            <th>deviceType</th>
                                            <th>deviceModel</th>
                                            <th>appVersion</th>
                                            <th>appBuild</th>
                                            <th>user Name</th>
                                            <th>Clicked</th>
                                            <th>Date</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($stats as $key=>$stat)
                                    <tr id="statID{{$stat->id}}">
                                            <td>{{$stat->id}}</td>
                                            <td>{{$stat->type}}</td>
                                            <td>{{$stat->deviceType}}</td>
                                            <td>{{$stat->deviceModel}}</td>
                                            <td>{{$stat->appVersion}}</td>
                                            <td>{{$stat->appBuild}}</td>
                                            <td>{{$stat->user->f_name}}</td>
                                            <td>
                                            @php
                                            if($stat->wasClicked == 1){
                                                echo 'true';
                                            }else{
                                                echo 'false';
                                            }
                                            @endphp
                                            </td>
                                            <td>{{$stat->created_at}}</td>
                                            <td>
                                             <a data-toggle="modal" data-target="#deleteStat{{$stat->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>
                                             </td>
                                             <div class="modal fade text-left" id="deleteStat{{$stat->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete stat</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteadstat')}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your stat request? </h5>
                                                                <input type="hidden" name='statID' value="{{$stat->id}}">
                                                                <button  class="btn btn-danger mr-1 my-1">Yes</button>
                                                               
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>    
                                                    </div>
                                                </div>
                                            </div>
                                    </tr>        
                                    @endforeach
                                    </tbody>
                                </table>
                                {{ $stats->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

@endsection
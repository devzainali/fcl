@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">App statistics list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of app statistics
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
     <div class="row">
            <!-- one -->
            <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$mainPage}}</h2>
         <p class="mb-0">Application Main page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
<!-- two -->
<div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$newsPage}}</h2>
         <p class="mb-0">News Page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
        <!-- three -->
        <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$newsDetailPage}}</h2>
         <p class="mb-0">News Details Page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
 <!-- Four -->
 <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$competitionPage}}</h2>
         <p class="mb-0">Competition List Page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
    <!-- five -->
    <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$leaguePage}}</h2>
         <p class="mb-0">League page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
        <!-- six -->
        <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$questionPage}}</h2>
         <p class="mb-0">Question page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>
        <!-- Seven -->
        <div class="col-lg-3 col-sm-6 col-12">
   <div class="card">
      <div class="card-header d-flex flex-column align-items-start pb-0">
         <div class="avatar bg-rgba-primary p-40 m-0">
            <div class="avatar-content">
               <i class="feather icon-users text-primary font-medium-5"></i>
            </div>
         </div>
         <h2 class="text-bold-700 mt-1">{{$giftPage}}</h2>
         <p class="mb-0">Gift page</p>
      </div>
      {{-- 
      <div class="card-content">
         <div id="line-area-chart-1"></div>
      </div>
      --}}
   </div>
</div>

     </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="row">
                        <div class="col-md-4 mt-5 ml-2">
                            <form action="{{route('admin.statListByType','come')}}" method="get">
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Page</option>
                                            <option value="Application Main page" {{ 'Application Main page'== @$selectedValue ? 'selected' : '' }}>Application Main page</option>
                                            <option value="FCL News Page" {{ 'FCL News Page'== @$selectedValue ? 'selected' : '' }}>FCL News Page</option>
                                            <option value="FCL News Details Page" {{ 'FCL News Details Page'== @$selectedValue ? 'selected' : '' }}>FCL News Details Page</option>
                                            <option value="FCL competition List Page" {{ 'FCL competition List Page'== @$selectedValue ? 'selected' : '' }}>FCL competition List Page</option>
                                            <option value="FCL league page" {{ 'FCL league page'== @$selectedValue ? 'selected' : '' }}>FCL league page</option>
                                            <option value="FCL question page" {{ 'FCL question page'== @$selectedValue ? 'selected' : '' }}>FCL question page</option>
                                            <option value="FCL gift page" {{ 'FCL gift page'== @$selectedValue ? 'selected' : '' }}>FCL gift page</option>
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped" id="transactionTable1">
                                    <thead>
                                        <tr>
                                            <th>Statistics ID</th>
                                            <th>Page Name</th>
                                            <th>deviceType</th>
                                            <th>deviceModel</th>
                                            <th>appVersion</th>
                                            <th>appBuild</th>
                                            <th>user Name</th>
                                            <th>Date</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($stats))
                                    @foreach($stats as $key=>$stat)
                                    <tr id="statID{{$stat->id}}">
                                            <td>{{$stat->id}}</td>
                                            <td>{{$stat->name}}</td>
                                            <td>{{$stat->deviceType}}</td>
                                            <td>{{$stat->deviceModel}}</td>
                                            <td>{{$stat->appVersion}}</td>
                                            <td>{{$stat->appBuild}}</td>
                                            <td>{{$stat->user->f_name}}</td>
                                            <td>{{$stat->created_at}}</td>
                                            <td>
                                             <a data-toggle="modal" data-target="#deleteStat{{$stat->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>
                                             </td>
                                             <div class="modal fade text-left" id="deleteStat{{$stat->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete stat</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deletestat')}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your stat request? </h5>
                                                                <input type="hidden" name='statID' value="{{$stat->id}}">
                                                                <button  class="btn btn-danger mr-1 my-1">Yes</button>
                                                               
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>    
                                                    </div>
                                                </div>
                                            </div>
                                    </tr>        
                                    @endforeach
                                    @endif
                                
                                    </tbody>
                                </table>
                                @if(isset($stats))
                                {{ $stats->links() }}
                                @endif
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>
<script type='text/javascript'>
function select(item)
{
    let url = "{{route('admin.statListByType','come')}}";
    window.location.href=url.replace('come',item.value);
}
</script>
@endsection
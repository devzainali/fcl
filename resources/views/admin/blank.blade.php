@extends('admin.layouts.app')



@section('page_content')

 <!-- Dashboard Ecommerce Starts -->

 <section id="dashboard-analytics">



    <div class="row">
    

        <div class="col-lg-12 col-md-12 col-sm-12">

            <div class="card bg-analytics text-white">

                <div class="card-content">

                    <div class="card-body text-center">

                    <img src="{{asset('admin/app-assets/images/elements/decore-left.png')}}" class="img-left" alt="

card-img-left">

                    <img src="{{asset('admin/app-assets/images/elements/decore-right.png')}}" class="img-right" alt="

card-img-right">

                        <div class="avatar avatar-xl bg-primary shadow mt-0">

                            <div class="avatar-content">

                                <i class="feather icon-award white font-large-1"></i>

                            </div>

                        </div>

                        <div class="text-center">

                            <h1 class="mb-2 text-white">Congratulations {{Auth::user()->name}},</h1>

                            <p class="m-auto w-75">You have successfully <strong>Login</strong>.</p>

                        </div>

                    </div>

                </div>

            </div>

        </div>        


        <div class="col-lg-3 col-sm-6 col-12">

            <div class="card">

                <div class="card-header d-flex flex-column align-items-start pb-0">

                    <div class="avatar bg-rgba-primary p-40 m-0">

                        <div class="avatar-content">

                            <i class="feather icon-users text-primary font-medium-5"></i>

                        </div>

                    </div>

                    <h2 class="text-bold-700 mt-1">{{$totalUser}}</h2>

                    <p class="mb-0">Total Users in the application</p>

                </div>

                {{-- <div class="card-content">

                    <div id="line-area-chart-1"></div>

                </div> --}}

            </div>

        </div>
        <div class="col-lg-3 col-sm-6 col-12">

            <div class="card">

                <div class="card-header d-flex flex-column align-items-start pb-0">

                    <div class="avatar bg-rgba-primary p-40 m-0">

                        <div class="avatar-content">

                            <i class="feather icon-dollar-sign text-primary font-medium-5"></i>

                        </div>

                    </div>

                    <h2 class="text-bold-700 mt-1">{{$totalSale}}</h2>

                    <p class="mb-0">Total Earning in Application</p>

                </div>

                {{-- <div class="card-content">

                    <div id="line-area-chart-1"></div>

                </div> --}}

            </div>

        </div>

        <div class="col-lg-3 col-sm-6 col-12">

        <div class="card" style="height: 68px;">
                                <div class="card-header d-flex align-items-start pb-0" style="padding: 0.5rem 1.5rem 0;">
                                    <div>
                                        <h2 class="text-bold-700 mb-0">{{$monthSale}}</h2>
                                        <p>Current Month</p>
                                    </div>
                                    <div class="avatar bg-rgba-primary p-30 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-dollar-sign text-success font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card" style="height: 68px;     margin-top: -26px;">
                                <div class="card-header d-flex align-items-start pb-0" style="padding: 0.5rem 1.5rem 0;">
                                    <div>
                                        <h2 class="text-bold-700 mb-0">{{$lastMonthSale}}</h2>
                                        <p>Last Month</p>
                                    </div>
                                    <div class="avatar bg-rgba-primary p-30 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-user-check text-primary font-medium-5"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

        </div>

        <div class="col-lg-3 col-sm-6 col-12">

            <div class="card">

                <div class="card-header d-flex flex-column align-items-start pb-0">

                    <div class="avatar bg-rgba-danger p-20 m-0">

                        <div class="avatar-content">

                            <i class="feather icon-user-check text-danger font-medium-5"></i>

                        </div>

                    </div>

                    <h2 class="text-bold-700 mt-1">{{$totalSubscribed}}</h2>

                    <p class="mb-0">Total users Subscribed in Application</p>

                </div>

                {{-- <div class="card-content">

                    <div id="line-area-chart-3"></div>

                </div> --}}

            </div>

        </div>

        <div class="col-lg-3 col-sm-6 col-12">

            <div class="card">

                <div class="card-header d-flex flex-column align-items-start pb-0">

                    <div class="avatar bg-rgba-warning p-50 m-0">

                        <div class="avatar-content">

                            <i class="feather icon-package text-warning font-medium-5"></i>

                        </div>

                    </div>

                    <h2 class="text-bold-700 mt-1">{{$activeCompetition}}</h2>

                    <p class="mb-0">Active Competitions</p>

                </div>

                {{-- <div class="card-content">

                    <div id="line-area-chart-4"></div>

                </div> --}}

            </div>

        </div>
        <div class="col-lg-3 col-sm-6 col-12">

            <div class="card">

                <div class="card-header d-flex flex-column align-items-start pb-0">

                    <div class="avatar bg-rgba-warning p-50 m-0">

                        <div class="avatar-content">

                            <i class="feather icon-bar-chart text-warning font-medium-5"></i>

                        </div>

                    </div>

                    <h2 class="text-bold-700 mt-1">{{$AppStat}}</h2>

                    <p class="mb-0">Total App Stat</p>

                </div>

                {{-- <div class="card-content">

                    <div id="line-area-chart-4"></div>

                </div> --}}

            </div>

        </div>

    </div> 


</section>

<!-- Dashboard Ecommerce ends -->

@section('page_level_scripts')

@endsection
@endsection
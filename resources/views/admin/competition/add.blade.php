@extends('admin.layouts.app')

@section('page_content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Competition</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeCompetition')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title of Competition" value="{{ old('title') }}" required name="title">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number"  id="last-name-column" class="form-control" placeholder="Price" value="{{ old('price') }}" name="price" required >
                                            <label for="last-name-column">Price</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="description" required id="basicTextarea" rows="3" placeholder="Description about course">{{ old('description') }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Start Date                                      
                                        </div>
                                        <input type="date" id="last-name-column" class="form-control" placeholder="Date" value="{{ old('startdate') }}" name="startdate" required >

                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Expire Date                                      
                                        </div>
                                        <input type="date" id="last-name-column" class="form-control" placeholder="Date" value="{{ old('expireDate') }}" name="expireDate" required >

                                    </div>
                                    <div class="col-md-12 col-12 mt-1">
                                         <div class="custom-control custom-switch switch-lg custom-switch-success mr-2 mb-1">
                                            <input type="checkbox" class="custom-control-input" name="status" id="customSwitch100" checked="">
                                            <label class="custom-control-label" for="customSwitch100">
                                            <span class="switch-text-left">Active</span>
                                                <span class="switch-text-right">InActive</span>
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
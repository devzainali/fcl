@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">All Competition Quiz's summarized score list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Competition Candidate Score list
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="row">
                        <div class="col-md-6 mt-1 ml-1">
                        <form action="{{route('admin.showQuizByCompetition','come')}}" method="get" >
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach(Helper::getCompetitions() as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>Points</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($competitons))
                                    @foreach($competitons as $key=>$sub)
                                        <tr id="">
                                                <td>{{$sub->id}}</td>
                                                <td class="product-name">{{$sub->user->email}}
                                                @php
                                                    if(!is_null($winners)){
                                                        foreach($winners as $key=>$winner):
                                                            if($winner->winner1 == $sub->user->id){
                                                                echo '<span class="badge badge-success">Winner 1('.$winner->date.')</span>';
                                                            }
                                                            if($winner->winner2 == $sub->user->id){
                                                                echo '<span class="badge badge-primary">Winner 2("'.$winner->date.'")</span>';
                                                            }
                                                            if($winner->winner3 == $sub->user->id){
                                                                echo '<span class="badge badge-info">Winner 3("'.$winner->date.'")</span>';
                                                            }
                                                        endforeach;
                                                       
                                                    }
                                                @endphp
                                                </td>
                                                <td class="product-price">{{$sub->user->getUserScore($sub->competition_id,$sub->user->id)}}</td>
                                                <td>
                                                <button type="button" class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown">
                                                                Mark winners
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" onClick="markWinner({{$sub->competition_id}},{{$sub->user->id}},1,'{{route('admin.CompetitionMarkWinner')}}');">Winner 1</a>
                                                                <a class="dropdown-item" onClick="markWinner({{$sub->competition_id}},{{$sub->user->id}},2,'{{route('admin.CompetitionMarkWinner')}}');" >winner 2</a>
                                                                <a class="dropdown-item" onClick="markWinner({{$sub->competition_id}},{{$sub->user->id}},3,'{{route('admin.CompetitionMarkWinner')}}');" >winner 3 </a>
                                                            </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                           <th>#</th>
                                            <th>Title</th>
                                            <th>price</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                @if(isset($competitons))
                                {{$competitons->links()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <form action="" method='post' id='winningPersonForm'>
       @csrf
        <input type="hidden" name="winner" id='winnerNumber' value="">
        <input type="hidden" name="user_id" id='winnerUserID' value="">
        <input type="hidden" name="competition_id" id='winnerCompetition' value="">
        <button name="submit" id="submitFormButton" style='display:none'>
    </form>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript">
function select(item)
{
    let url = "{{route('admin.geCompetitionScore','come')}}";
    window.location.href=url.replace('come',item.value);
} 
function markWinner(id,userID,winner,action) {
    debugger;
    var form = document.getElementById("winningPersonForm");
    form.action = action; 
    $('#winnerNumber').val(winner);
    $('#winnerUserID').val(userID);
    $('#winnerCompetition').val(id);
    debugger;
    $("#submitFormButton").click();
    // $("#winningPersonForm").submit();

    // $('#cover-spin').show(0);
    // $.ajax({
    //   type: "POST",
    //   url: "/admin/competiton/markWinner/",
    //   processData: false,
    //   contentType: false,
    //   success: function (data) {
    //       debugger;
    //     if (data.status == true) {
    //       toastr.success(data.message);
    //     }
    //     if (data.error) {
    //       toastr.error(data.message);
    //     }
    //     $('#cover-spin').hide();
    //   },
    // });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">edit Competition</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.updateCompetition',$competiton->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" step="any" id="first-name-column" class="form-control" placeholder="Title of Competition" value="{{$competiton->name}}" required name="title">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number" id="last-name-column" class="form-control" placeholder="Price" value="{{ $competiton->subAmount }}" name="price" required >
                                            <label for="last-name-column">Price</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Start Date                                      
                                        </div>
                                        <input type="date" id="last-name-column" class="form-control" placeholder="Date" value="{{$competiton->start_date}}" name="startdate" required >

                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Expire Time                                
                                        </div>
                                        <input type="date" id="last-name-column" class="form-control" placeholder="Date" value="{{$competiton->expired_at}}" name="expireDate" required >
                                    </div>
                                    <div class="col-md-12 col-12 mt-1">
                                         <div class="custom-control custom-switch custom-switch-primary ml-auto">
                                            <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" {{ $competiton->status == true ?'checked':'' }}>
                                            <label class="custom-control-label" for="customSwitch1"></label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
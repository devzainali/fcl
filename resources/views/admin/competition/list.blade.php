@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Competition</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Competition List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
<a href="{{route('admin.addCompetition')}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light" ><i class="feather icon-plus"></i>Add Competition</a>
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Competition ID</th>
                                            <th>Name</th>
                                            <th>Subscription Price</th>
                                            <th>status<br> <small>(disable/active)</small> </th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($competitons as $key=>$competiton)
                                        <tr id="courceID{{$competiton->id}}">
                                                <td>{{$key+1}}</td>
                                                <td class="product-name">{{$competiton->name}}</td>
                                                <td class="product-price">{{$competiton->subAmount}}</td>
                                                <td>
                                                        <div class="custom-control custom-switch switch-lg custom-switch-success mr-2 mb-1">
                                                            <input type="checkbox" class="custom-control-input" onchange="changeCompetitionStatus({{$competiton->id}});" id="customSwitch9{{$key+1}}"
                                                            {{ $competiton->status == true ?'checked':'' }}
                                                        >
                                                            <label class="custom-control-label" for="customSwitch9{{$key+1}}">
                                                            <span class="switch-text-left">Active</span>
                                                            <span class="switch-text-right">inActive</span>
                                                            </label>
                                                        </div>
                                                </td>
                                                <td>
                                                <a href="{{route('admin.editCompetition',$competiton->id)}}" class="btn-primary btn-sm">edit</a>
                                                <a data-toggle="modal" data-target="#delete{{$competiton->id}}" href="{{route('admin.geteNativeAdd',$competiton->id)}}" class="btn-primary btn-sm">Delete</a>
                                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                               More..
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="{{route('admin.addgift',$competiton->id)}}">Gifts</a>
                                                                <a class="dropdown-item" href="{{route('admin.geteNativeAdd',$competiton->id)}}">Ads</a>
                                                                <a class="dropdown-item" href="{{route('admin.geCompetitionScore',$competiton->id)}}">Score List</a>
                                                                <a class="dropdown-item" href="{{route('admin.geCompetitionUserList',$competiton->id)}}">Subscription List </a>
                                                                <a class="dropdown-item" href="{{route('admin.transactionList',$competiton->id)}}">Transaction List </a>
                                                            </div>
                                                
                                                </td>
                                                <div class="modal fade text-left" id="delete{{$competiton->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Competition</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteCompetition',$competiton->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Competition? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>Competition ID</th>
                                            <th>Name</th>
                                            <th>Subscription Price</th>
                                            <th>status</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $competitons->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
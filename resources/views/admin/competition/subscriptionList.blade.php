@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Competition Subscription list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of users who subscribed competition
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="row">
                        <div class="col-md-6 mt-1 ml-1">
                        <form action="{{route('admin.showQuizByCompetition','come')}}" method="get" >
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach(Helper::getCompetitions() as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Subscription ID</th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Method</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($competitons))
                                    @foreach($competitons as $key=>$sub)
                                        <tr id="">
                                                <td>{{$sub->id}}</td>
                                                <td class="product-name">{{$sub->user->email}}</td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($sub->transaction)){
                                                    echo $sub->transaction->amount;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($sub->transaction)){
                                                    echo $sub->transaction->method;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td class="product-price">
                                                @php 
                                                $expire = strtotime($sub->expireAt);
                                                $today = strtotime("today midnight");
                                                if($today >= $expire){
                                                    echo '<span class="badge badge-danger">Expired</span>';
				
                                                }else{
                                                    echo '<span class="badge badge-success">Active</span>';
                                                }
                                                @endphp
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                         <th>Subscription ID</th>
                                            <th>Email</th>
                                            <th>Amount</th>
                                            <th>Method</th>
                                            <th>Status</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                @if(isset($competitons))
                                {{$competitons->links()}}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function select(item)
{
    let url = "{{route('admin.geCompetitionUserList','come')}}";
    window.location.href=url.replace('come',item.value);
} 
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Contact Request</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Contact Request List 
                        </li>
                        <li class="breadcrumb-item active">Total Contacts ({{$count}})
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                   
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>FeedBack ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($contacts as $key=>$contact)
                                        <tr id="contactID{{$contact->id}}">
                                            <td>{{$contact->id}}</td>
                                            <td class="product-name" id="full_name{{$contact->id}}">{{$contact->full_name}}</td>
                                            <td class="product-price" id="email{{$contact->id}}">{{$contact->email}}</td>
                                            <td class="product-price" id="msg{{$contact->id}}">{{$contact->message}}</td>
                                            <p id="message{{$contact->id}}" style="display: none">{{$contact->message}}</p>
                                             <td>
                                                    <!-- <a ><i class="fa fa-eye fonticon-container" aria-hidden="true" onclick="contactdetail({{$contact->id}});" style="color:blue"></i></a> -->
                                                    <a data-toggle="modal" data-target="#deleteContact{{$contact->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>
                                                    <!-- <a data-toggle="modal" data-target="#replayBox{{$contact->id}}"><i class="fa fa-reply fonticon-container" aria-hidden="true" style="color: green"></i></a> -->
                                                    <a href="mailto:{{$contact->email}}"><i class="fa fa-reply fonticon-container" aria-hidden="true" style="color: green"></i></a>

                                                </td>
                                            </tr>
                                            <div class="modal fade text-left" id="deleteContact{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Contact Request</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteContacts',$contact->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your contact request? </h5>
                                                                <button  class="btn btn-danger mr-1 my-1">Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade text-left" id="replayBox{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h5 class="modal-title" id="myModalLabel160">Reply</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <form class="form" action="{{route('admin.contactreply')}}" method="post" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                   <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" rows="3" name="description"> </textarea>
                                            <input type="hidden" name="user_id" value="{{$contact->user_id}}">
                                        </div>
                                    </div>
                                <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        
            </div>
            <div class="modal-footer">
            </div>
            </form>
        </div>
    </div>
</div>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>FeedBack ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Message</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{$contacts->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->
</div>

 <div class="modal fade text-left" id="contactDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h5 class="modal-title" id="myModalLabel160">Contact Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <strong>Name  </strong><p id="q_firstName">Muhammad Zain Ali</p>    
                <strong> Email  </strong><p id="q_email">dev.zainAli@gamil.com</p>
                <h3>Message</h3>
                <hr>
                <p id="q_message">This is demo</p>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button> --}}
            </div>
        </div>
    </div>
</div>




@endsection

@section('page_level_scripts')
<script type="text/javascript">
function contactdetail(id){
    $('#cover-spin').show(0);
    $(`#q_firstName`).text($(`#full_name${id}`).text());
    $(`#q_email`).text($(`#email${id}`).text());
    $(`#q_phone`).text($(`#phone${id}`).text());
    $(`#q_message`).text($(`#message${id}`).text());
    $('#contactDetail').modal('show'); 
    $('#cover-spin').hide();

}
function deleteContact(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/contact/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
        $(`#deleteContact${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#contactID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">General Native Ad</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Ad
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Ad</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeGeneralAdd')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ old('title') }}" name="title">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Url" value="{{ old('url') }}" required name="url">
                                            <label for="first-name-column">Url</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="date" id="first-name-column" class="form-control datepicker" placeholder="start Date" value="{{ old('startDate') }}" required name="startDate">
                                            <label for="first-name-column">start Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="date" id="first-name-column" class="form-control" placeholder="end Date" value="{{ old('endDate') }}" required name="endDate">
                                        <label for="first-name-column">End Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image"  required >
                                        </div>
                                    </div>
                                    
                                <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

@endsection

@section('page_level_scripts')
<script type="text/javascript">
   
    $('.datepicker').datepicker({ 
        startDate: new Date()
    });
  
</script>
<script type='text/javascript'>
    $("#add").click(function (e) {
        e.preventDefault();
    //Append a new row of code to the "#items" div
  $("#items").append('<div class="form-label"><input type="file" name="image[]"  required ></div>');
});
</script>
<script type="text/javascript">
function deleteQuiz(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/quiz/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deleteQuiz${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#quizID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
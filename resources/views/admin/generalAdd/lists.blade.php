@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">General Ads</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">General Ads List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
<a href="{{route('admin.addGeneralAdd')}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light" ><i class="feather icon-plus"></i>Add General Ad</a>
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>General Ad ID</th>
                                            <th>Title</th>
                                            <th>url</th>
                                            <th>Date</th>
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($Ads as $key=>$ad)
                                        <tr id="adID{{$ad->id}}">
                                                <td>{{$ad->id}}</td>
                                                <td class="product-name"><small>{{$ad->title}}</small></td>
                                                <td class="product-price"><small>{{$ad->url}}</small></td>
                                                <td class="product-price"><small>{{$ad->startDate}} - {{$ad->endDate}}</small></td>
                                                <!-- <td class="product-price">{{$ad->endDate}}</td> -->
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($ad->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                                 <td>
                                                <a href="{{route('admin.adStatistics',[$ad->id,'General'])}}" class="btn-primary btn-sm">Statistics</a>|
                                                <a data-toggle="modal" data-target="#delete{{$ad->id}}" href="{{route('admin.geteNativeAdd',$ad->id)}}" class="btn-primary btn-sm">Delete</a>
                                                </td>
                                                <div class="modal fade text-left" id="delete{{$ad->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Add</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleeNativeAdd',$ad->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Ad? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>General Ad ID</th>
                                            <th>Title</th>
                                            <th>url</th>
                                            <th>startDate</th>
                                            <!-- <th>endDate</th> -->
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $Ads->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function deleteAdd(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/GeneralAdd/native/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#delete${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#adID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
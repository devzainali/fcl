@extends('admin.layouts.app')

@section('page_content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
            <div class="row">
                        <div class="col-md-6 mt-1 ml-1">
                        <form action="{{route('admin.showQuizByCompetition','come')}}" method="get" >
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach(Helper::getCompetitions() as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                <div class="card-header">
                    <h4 class="card-title">Add Gifts for competition ({{@$competitionName}})</h4>
                </div>
                <div class="card-header">
                    <h4 class="card-title">Winner 1 gift</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeGift')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <input type="hidden" name="competitionID" value="{{$quizCompetition}}">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ old('titleGiftOne') }}" required name="titleGiftOne">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number" id="first-name-column" class="form-control" placeholder="winner points" value="{{ old('pointdGiftOne') }}" required name="pointdGiftOne">
                                            <label for="first-name-column">Points</label>
                                        </div>
                                    </div> -->
                                  
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="descriptionOne" required id="basicTextarea" rows="3" placeholder="Desctiption about gift">{{ old('descriptionOne') }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <input type="file" name='imageForGiftOne'>
                                        <!-- <label for="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Browse for file ...
                                            <input type="file" name='imageForGiftOne'>
                                        </label> -->
                                    </div>
                                 </div>
                                 
                                 <!-- gift one ends -->
                                 <div class="card-header">
                                    <h4 class="card-title">Winner 2 gift</h4>
                                </div>
                                 <div class="row">
                                 <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ old('titleGiftTwo') }}" required name="titleGiftTwo">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number" id="first-name-column" class="form-control" placeholder="winner points" value="{{ old('pointdGiftTwo') }}" required name="pointdGiftTwo">
                                            <label for="first-name-column">Points</label>
                                        </div>
                                    </div> -->
                                  
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="descriptionTwo" required id="basicTextarea" rows="3" placeholder="Desctiption about gift">{{ old('descriptionTwo') }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <input  type="file" name="imageForGiftTwo">
                                        <!-- <label for="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Browse for file ...
                                            <input  type="file" name="imageForGiftTwo">
                                        </label> -->
                                    </div>
                                  
                                </div>
                                <!-- gift 2 ends -->
                                <div class="card-header">
                                    <h4 class="card-title">Winner 3 gift</h4>
                                </div>
                                <div class="row">
                                <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ old('titleGiftThree') }}" required name="titleGiftThree">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number" id="first-name-column" class="form-control" placeholder="winner points" value="{{ old('pointdGiftThree') }}" required name="pointdGiftThree">
                                            <label for="first-name-column">Points</label>
                                        </div>
                                    </div> -->
                                  
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="descriptionThree" required id="basicTextarea" rows="3" placeholder="Description about gift">{{ old('descriptionThree') }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <input  type="file" name="imageForGiftThree">
                                        <!-- <label for="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Browse for file ...
                                            <input  type="file" name="imageForGiftThree">
                                        </label> -->
                                    </div>
                                  
                                    <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                


            </div>
        </div>
    </div>
</section>
@endsection


@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript">
function select(item)
{
    let url = "{{route('admin.showGift','come')}}";
    window.location.href=url.replace('come',item.value);
}
</script> 
@endsection

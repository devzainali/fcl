@extends('admin.layouts.app')

@section('page_content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Gifts</h4>
                </div>
                <div class="card-header">
                    <h4 class="card-title">edit gift</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.updategift',$gift->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12"> 
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ $gift->title }}" required name="titleGiftOne">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="number" id="first-name-column" class="form-control" placeholder="winner points" value="{{ $gift->winPoint }}" required name="pointdGiftOne">
                                            <label for="first-name-column">Points</label>
                                        </div>
                                    </div> -->
                                  
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="descriptionOne" required id="basicTextarea" rows="3" placeholder="Desctiption about gift">{{ $gift->description }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div>
                                   
                                    <div class="col-md-6 col-12">
                                    <input type="file" name='imageForGiftOne'>
                                        <!-- <label for="fileUpload" class="file-upload btn btn-primary btn-block rounded-pill shadow"><i class="fa fa-upload mr-2"></i>Browse for file ...
                                            <input type="file" name='imageForGiftOne'>
                                        </label> -->
                                    </div> 
                                    <div class="col-md-12 col-12">
                                    <p>current image</p>
                                    <div class="w-25">
                                       <img src="{{Storage::disk('public')->url($gift->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                    </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <!-- gift one ends -->
                                 
                                    <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                


            </div>
        </div>
    </div>
</section>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Competition ({{@$competiton->name}}) gifts</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Gift List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="row">
                        <div class="col-md-6 mt-1 ml-1">
                        <form action="{{route('admin.showQuizByCompetition','come')}}" method="get" >
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach(Helper::getCompetitions() as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>Gift ID</th>
                                            <th>Title</th>
                                            <!-- <th>win Points</th> -->
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($gifts))
                                    @foreach($gifts as $key=>$gift)
                                        <tr id="courceID{{$gift->id}}">
                                                <td>{{$gift->id}}</td>
                                                <td class="product-name">{{$gift->title}}</td>
                                                <!-- <td class="product-price">{{$gift->winPoint}}</td> -->
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($gift->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                                <td>
                                                <a href="{{route('admin.editgift',$gift->id)}}"><i class="fa fa-pencil-square-o fonticon-container" style="color: blue;" aria-hidden="true"></i></a>
                                                    {{-- <a href=""><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a> --}}
                                                    <!-- Modal: modalPoll -->
                                                </td>
                                            </tr>
                                            @endforeach
                                    @endif
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Gift ID</th>
                                            <th>Title</th>
                                            <!-- <th>win Points</th> -->
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript">
function select(item)
{
    let url = "{{route('admin.showGift','come')}}";
    window.location.href=url.replace('come',item.value);
} 
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        @include('admin.layouts.head')
    </head>
    <!-- END HEAD -->

    <body class="vertical-layout vertical-menu-modern semi-dark-layout 2-columns  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="2-columns" data-layout="semi-dark-layout">
        <div id="cover-spin"></div>
        @include('admin.layouts.sidebar')
        <div class="app-content content">
        @include('admin.layouts.header')
            <div class="content-wrapper">
                    @yield('page_content')
                    @show
            </div>
    </div>
        
        @include('admin.layouts.footer')
       
        @include('admin.layouts.scripts')
    </body>

</html>
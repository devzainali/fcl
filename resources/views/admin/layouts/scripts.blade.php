    <!-- BEGIN: Vendor JS-->
    <script type="text/javascript">
    setTimeout(function(){ 
        $(".tippy-popper").remove();
     }, 900);
    </script>
    <script src="{{asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->
    <!-- BEGIN: Page Vendor JS-->
<script src="{{asset('admin/app-assets/vendors/js/extensions/dropzone.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/pdfmake.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/vfs_fonts.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/datatables.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/buttons.html5.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/buttons.print.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/charts/apexcharts.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/extensions/tether.min.js')}}"></script>
<script src="{{asset('admin/app-assets/vendors/js/extensions/shepherd.min.js')}}"></script>

<!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
<script src="{{asset('admin/app-assets/js/core/app-menu.js')}}"></script>
<script src="{{asset('admin/app-assets/js/core/app.js')}}"></script>
<script src="{{asset('admin/app-assets/js/scripts/components.js')}}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
{{-- <script src="{{asset('admin/app-assets/js/scripts/pages/dashboard-analytics.js')}}"></script> --}}
<script src="{{asset('admin/app-assets/js/scripts/pages/dashboard-ecommerce.js')}}"></script>

<script src="{{asset('admin/app-assets/js/scripts/ui/data-list-view.js')}}"></script>
<script src="{{asset('admin/assets/js/toaster.js')}}"></script>
{{-- <script src="{{asset('admin/app-assets/js/scripts/extensions/dropzone.js')}}"></script> --}}
<script src="{{asset('admin/app-assets/js/scripts/datatables/datatable.js')}}"></script>
{{-- <script src="{{asset('admin/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('admin/app-assets/js/scripts/forms/validation/form-validation.js')}}"></script> --}}
<script type="text/javascript">
        $(function () {
            $('#txtDate').datepicker();
        });

    </script>

    <!-- END: Page JS-->
 @section('page_level_scripts')
@show
<script type="text/javascript">
    $('input[type=file]').addClass('btn btn-outline-primary mr-1 mb-1 waves-effect waves-light');

</script>
<script>
	 @if(Session::has('success'))
  		toastr.success("{{ Session::get('success') }}");
  @endif


  @if(Session::has('info'))
  		toastr.info("{{ Session::get('info') }}");
  @endif


  @if(Session::has('warning'))
  		toastr.warning("{{ Session::get('warning') }}");
  @endif


  @if(Session::has('error'))
  		toastr.error("{{ Session::get('error') }}");
  @endif

  @if(count($errors) > 0)
        @foreach($errors->all() as $error)
            toastr.error("{{ $error }}");
        @endforeach
    @endif
</script>
        <script src="{{asset('admin/assets/js/ajaxRequests/cources.js')}}"></script>


    {{-- ajax call ends --}}

    <!-- BEGIN: Main Menu-->
    <div class="main-menu menu-fixed menu-dark menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto">
                    <a class="navbar-brand" href="{{route('admin.profile')}}">
                        {{-- <div class="brand-logo"></div> --}}
                        <i class="fa fa-cog"></i>
                        <h2 class="brand-text mb-0">FCL</h2>
                    </a></li>
                <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i></a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <!-- <li class=" nav-item"><a href="/admin"><i class="feather icon-home"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a></li> -->
            <li class=" nav-item"><a href="{{route('admin.dashboard')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Dashboard</span></a>
                </li>
            <li class=" nav-item"><a href="{{route('admin.profile')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Profile</span></a>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Users</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.userList')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Feedback</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.showContacts')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li> 
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">News</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.addNews')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Add</span></a>
                        </li>
                        <li><a href="{{route('admin.newsStorelist')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">List</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Notifications</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.NotificationList')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">add/list</span></a>
                        </li>
                    </ul>
                </li>
                
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Competitions</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.addCompetition')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Add</span></a>
                        </li>
                        <li><a href="{{route('admin.showCompetition')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">List</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Quiz</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.showQuiz')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <!-- <li class=" nav-item"><a href="{{route('admin.showQuiz')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Quiz</span></a>
                </li> -->
                <!-- <li class=" nav-item"><a href="{{route('admin.QuestionPage')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Question</span></a>
                </li> -->
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Question</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.QuestionPage')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">add/list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Score List</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.geCompetitionScoreLoad')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">add/list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Gifts</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.showGiftList')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Competitions Ads</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.addNativeAdd')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Add</span></a>
                        </li>
                        <li><a href="{{route('admin.addNativeCompetition')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">List</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">General Add</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.addGeneralAdd')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Add</span></a>
                        </li>
                        <li><a href="{{route('admin.geteGeneralNativeSave')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">List</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Subscriptions</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.geCompetitionUsers')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Transaction</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.getCompetitionForSelectBox')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Statistics</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.statList')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Live Score</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.livescoreList')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">add/list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Social Links</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.social.list')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">add/list</span></a>
                        </li>
                    </ul>
                </li>
                <li class=" nav-item"><a><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Sponsors</span></a>
                    <ul class="menu-content">
                        <li><a href="{{route('admin.showSponser')}}"><i class="feather icon-circle"></i><span class="menu-item" data-i18n="Analytics">Add/list</span></a>
                        </li>
                    </ul>
                </li>
            <!-- <li class=" nav-item"><a href="{{route('admin.showSponser')}}"><i class="feather icon-user"></i><span class="menu-title" data-i18n="Dashboard">Sponsors</span></a>
                </li> -->
            <!-- <li class=" nav-item"><a href="{{route('admin.userList')}}"><i class="feather icon-users"></i><span class="menu-title" data-i18n="Dashboard">Users</span></a>
                </li>
                </li> -->

            <!-- <li class=" nav-item"><a href="{{route('admin.showContacts')}}"><i class="fa fa-list-alt"></i><span class="menu-title" data-i18n="Dashboard">Feedback</span></a>
                </li> -->
  
                <!-- <li class=" nav-item"><a href="{{route('admin.getCompetitionForSelectBox')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Transaction</span></a>
                </li> -->
 
                <!-- <li class=" nav-item"><a href="{{route('admin.showGiftList')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Gifts</span></a>
                </li> -->

                <!-- <li class=" nav-item"><a href="{{route('admin.geCompetitionUsers')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Subscriptions</span></a>
                </li> -->

                <!-- <li class=" nav-item"><a href="{{route('admin.NotificationList')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Push Notification</span></a>
                </li> -->
   
                <!-- <li class=" nav-item"><a href="{{route('admin.geCompetitionScoreLoad')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Score List</span></a>
                </li> -->
             
                <!-- <li class=" nav-item"><a href="{{route('admin.livescoreList')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Live Score</span></a>

                </li> -->

                <!-- <li class=" nav-item"><a href="{{route('admin.social.list')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Social Links</span></a>
                </li> -->

                <!-- <li class=" nav-item"><a href="{{route('admin.statList')}}"><i class="feather icon-book"></i><span class="menu-title" data-i18n="Dashboard">Stat</span></a>
                </li> -->

          
          
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

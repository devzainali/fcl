@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Native Ad</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Ad 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Ad</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeNativeAdd')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title" value="{{ old('title') }}" name="title">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                            <select class="select2-size-lg form-control" id="large-select" required name="competitionID">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}">{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Url" value="{{ old('url') }}" required name="url">
                                            <label for="first-name-column">Url</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="date" id="starting" class="form-control datepicker" placeholder="start Date" value="{{ old('startDate') }}" required name="startDate">
                                            <label for="first-name-column">start Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="date" id="first-name-column" class="form-control" placeholder="end Date" value="{{ old('endDate') }}" required name="endDate">
                                        <label for="first-name-column">End Date</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image"  required >
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image2"  required >
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image3"  required >
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image4">
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-12">
                                    <div class="form-label">
                                            <input type="file" name="image5">
                                        </div>
                                    </div>
                                    
                                <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>

@endsection

@section('page_level_scripts')
<script type="text/javascript">
   
    $('.datepicker').datepicker({ 
        startDate: new Date()
    });
  
</script>
<script type='text/javascript'>
    $("#add").click(function (e) {
        e.preventDefault();
    //Append a new row of code to the "#items" div
  $("#items").append('<div class="form-label"><input type="file" name="image[]"  required ></div>');
});
</script>
<script type="text/javascript"> 
function detail(id){
    $('#cover-spin').show(0);
    $(`#imageshow1`).attr("src", $(`#srcimage1-${id}`).text());
    $(`#imageshow2`).attr("src", $(`#srcimage2-${id}`).text());
    $(`#imageshow3`).attr("src", $(`#srcimage3-${id}`).text());
    $(`#imageshow4`).attr("src", $(`#srcimage4-${id}`).text());
    $(`#imageshow5`).attr("src", $(`#srcimage5-${id}`).text());
    $('#ImageDetail').modal('show'); 
    $('#cover-spin').hide();

}
function deleteAdd(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/NativeAdd/competition/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#delete${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#adID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
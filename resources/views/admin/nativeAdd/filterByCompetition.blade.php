@extends('admin.layouts.app')

@section('page_content')

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Competition ({{@$competition->name}}) Ads</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Competition Ads List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
<a href="{{route('admin.addNativeAdd')}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light" ><i class="feather icon-plus"></i>Add Competition Ad</a>
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                   <div class="row">
                        <div class="col-md-6 mt-1 ml-1">
                        <form action="{{route('admin.showAdsByCompetition','come')}}" method="get" >
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach(Helper::getCompetitions() as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Ad ID</th>
                                            <th>Title</th>
                                            <th>url</th>
                                            <th>Date</th>
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($Ads))
                                    @foreach($Ads as $key=>$ad)
                                        <tr id="adID{{$ad->id}}">
                                                <td>{{$ad->id}}</td>
                                                <td class="product-name"><small>{{$ad->title}}</small></td>
                                                <td class="product-price"><small>{{$ad->url}}</small></td>
                                                <td class="product-price"><small>{{$ad->startDate}} - {{$ad->endDate}}</small></td>
                                                <!-- <td class="product-price">{{$ad->endDate}}</td> -->
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($ad->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                    <p id="srcimage1-{{$ad->id}}" style="display:none">{{Storage::disk('public')->url($ad->image)}}</p>
                                                    <p id="srcimage2-{{$ad->id}}" style="display:none">{{Storage::disk('public')->url($ad->image2)}}</p>
                                                    <p id="srcimage3-{{$ad->id}}" style="display:none">{{Storage::disk('public')->url($ad->image3)}}</p>
                                                    <p id="srcimage4-{{$ad->id}}" style="display:none">{{Storage::disk('public')->url($ad->image4)}}</p>
                                                    <p id="srcimage5-{{$ad->id}}" style="display:none">{{Storage::disk('public')->url($ad->image5)}}</p>
                                                </td>
                                                 <td>
                                                <!-- <a href="{{route('admin.editCompetition',$ad->id)}}" class="btn-primary btn-sm">edit</a>| -->
                                                <a href="{{route('admin.adStatistics',[$ad->id,'Competition'])}}" class="btn-primary btn-sm">Statistics</a>
                                                <a ><i class="fa fa-eye fonticon-container" aria-hidden="true" onclick="detail({{$ad->id}});" style="color:blue"></i></a>
                                                <a data-toggle="modal" data-target="#delete{{$ad->id}}" href="{{route('admin.geteNativeAdd',$ad->id)}}" class="btn-primary btn-sm">Delete</a>
                                                </td>
                                                <div class="modal fade text-left" id="delete{{$ad->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Ad</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleeCompetitionNativeAdd',$ad->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Ad? </h5>
                                                                <button  class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                            @endforeach

                                            @endif
           
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Ad ID</th>
                                            <th>Title</th>
                                            <th>url</th>
                                            <th>Date</th>
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                @if(isset($Ads))
                                {{ $Ads->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>

<div class="modal fade text-left" id="ImageDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h5 class="modal-title" id="myModalLabel160">Ad images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <table>
            <thead>
                <tr>
                <th>Image 1</th>
                <th>Image 2</th>
                <th>Image 3</th>
                <th>Image 4</th>
                <th>Image 5</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td class="w-25"><img src="" id="imageshow1" class="img-fluid img-thumbnail" alt="image"></td>
                <td class="w-25"><img src="" id="imageshow2" class="img-fluid img-thumbnail" alt="image2"></td>
                <td class="w-25"><img src="" id="imageshow3" class="img-fluid img-thumbnail" alt="image3"></td>
                <td class="w-25"><img src="" id="imageshow4" class="img-fluid img-thumbnail" alt="image4"></td>
                <td class="w-25"><img src="" id="imageshow5" class="img-fluid img-thumbnail" alt="image5"></td>
                </tr>
            </tbody>
            </table>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button> --}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_level_scripts')
<script type="text/javascript">
function select(item)
{
    let url = "{{route('admin.showAdsByCompetition','come')}}";
    window.location.href=url.replace('come',item.value);
}
   
    $('.datepicker').datepicker({ 
        startDate: new Date()
    });
  
</script>
<script type='text/javascript'>
    $("#add").click(function (e) {
        e.preventDefault();
    //Append a new row of code to the "#items" div
  $("#items").append('<div class="form-label"><input type="file" name="image[]"  required ></div>');
});
</script>
<script type="text/javascript"> 
function detail(id){
    $('#cover-spin').show(0);
    $(`#imageshow1`).attr("src", $(`#srcimage1-${id}`).text());
    $(`#imageshow2`).attr("src", $(`#srcimage2-${id}`).text());
    $(`#imageshow3`).attr("src", $(`#srcimage3-${id}`).text());
    $(`#imageshow4`).attr("src", $(`#srcimage4-${id}`).text());
    $(`#imageshow5`).attr("src", $(`#srcimage5-${id}`).text());
    $('#ImageDetail').modal('show'); 
    $('#cover-spin').hide();

}
function deleteAdd(id) {
    debugger;
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/NativeAdd/competition/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
        debugger;
          $(`#delete${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#adID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
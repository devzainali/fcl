@extends('admin.layouts.app')

@section('page_content')
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add news</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.newsStore')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Title of news" value="{{ old('title') }}" required name="title">
                                            <label for="first-name-column">Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <textarea class="form-control" name="description" required id="basicTextarea" rows="3" placeholder="Description about news">{{ old('description') }}</textarea>
                                            <label for="city-column">Description</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <input type="file" name="image"  required id="imageCource">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
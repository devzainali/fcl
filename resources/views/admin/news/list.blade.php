@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">News</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">News List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>News ID</th>
                                            <th>Title</th>
                                            <th>image</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($news as $key=>$new)
                                        <tr id="newsID{{$new->id}}">
                                                <td>{{$new->id}}</td>
                                                <td class="product-name">{{$new->title}}</td>
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($new->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                                <td>
                                                <a href="{{route('admin.editNews',$new->id)}}"><i class="fa fa-pencil-square-o fonticon-container" style="color: blue;" aria-hidden="true"></i></a>
                                                <a  data-toggle="modal" data-target="#deleteNews{{$new->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>
                                                    <!-- Modal: modalPoll -->
                                                </td>
                                            </tr>
                                            <div class="modal fade text-left" id="deleteNews{{$new->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete News</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteNews',$new->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete this News? </h5>
                                                                <button class="btn btn-danger mr-1 my-1">Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>News ID</th>
                                            <th>Title</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $news->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function deleteNews(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/news/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deleteNews${id}`).modal('hide');
        if (data.status == true) {
         $(`#newsID${id}`).remove();
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Profile</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="">profile</a>
                        </li>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="content-body">
       <!--Settings-begins -->
       <section>
        <!-- Account-begins -->
        <div class="settings-account">
            <!-- <h6 class="mb-1">Account</h6> -->
            <div class="card user-form">
                <div class="card-header">
                    <h4 class="card-title">Account</h4>
                </div>
                <div class="card-body">
                    <div class="collapse-header">
                        <div id="headingCollapse1">
                            <div class="lead collapse-title" data-toggle="collapse" role="button" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                <div class="media">
                                   
                                    <div class="media-body mt-1">
                                    <h5 class="media-heading mb-0">{{Auth::guard('admin')->user()->name}}</h5>
                                        <a class="text-muted"><small>{{Auth::guard('admin')->user()->email}}</small></a>
                                   </div>
                                   
                                
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            
            </div>
        </div>
        <!-- Account-end -->
            
        <!-- Security-begins -->
        <div class="settings-security">
            <div class="card collapse-icon accordion-icon-rotate">
                <div class="card-header">
                    <h4 class="card-title">Security</h4>
                </div>
                <div class="card-body">
                    <div class="card collapse-header">
                        <div id="headingCollapse2" class="">
                            <div class="lead collapse-title" data-toggle="collapse" role="button" data-target="#collapse2" aria-expanded="false" aria-controls="collapse1">
                                <div class="change-password">
                                    <span>Change password</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="collapse2" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                        <div class="card-content">
                        <!-- <form action="{{route('admin.changePassword')}}" method="post" class="form form-vertical" > -->
                        <form action="{{route('admin.changePassword')}}" method="post" class="form form-vertical" >
                            @csrf    
                            <div class="form-group">
                                    <label for="password-vertical1">Old Password</label>
                                    <div class="position-relative has-icon-left">
                                        <input type="password" class="form-control" name="oldpassword" required placeholder="Old Password">
                                        <div class="form-control-position">
                                            <i class="feather icon-at-sign"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password-vertical2">New Password</label>
                                    <div class="position-relative has-icon-left">
                                        <input type="password" class="form-control" name="password" required placeholder="New Password">
                                        <div class="form-control-position">
                                            <i class="feather icon-at-sign"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password-vertical3">New Password again</label>
                                    <div class="position-relative has-icon-left">
                                        <input type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">
                                        <div class="form-control-position">
                                            <i class="feather icon-at-sign"></i>
                                        </div>
                                    </div>
                                </div>
                                <button  class="btn btn-primary mr-1 mb-1">Update</button>
                            </form>
                        </div>
                    </div>
                    <div class="delete-account border-top pt-1">
                        {{-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteForm">
                            Delete Account ?
                        </button> --}}
                        <div class="modal fade text-left" id="deleteForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel33">Delete Account</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form action="#">
                                        <div class="modal-body">
                                            <h5>Are you sure to delete your account? </h5>
                                            <button type="button" class="btn btn-danger mr-1 my-1" data-dismiss="modal">Yes</button>
                                            <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Security-end -->
    </section>
    <!-- Settings-end
</div>

 <div class="modal fade text-left" id="contactDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h5 class="modal-title" id="myModalLabel160">Quote Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <strong>Name  </strong><p id="q_firstName">Muhammad Zain Ali</p>    
                <strong> Email  </strong><p id="q_email">dev.zainAli@gamil.com</p>
                <strong> Subject  </strong><p id="q_subject">03324571945</p>
                <h3>Message</h3>
                <hr>
                <p id="q_message">This is demo</p>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button> --}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_level_scripts')
<script type="text/javascript">

</script>
@endsection
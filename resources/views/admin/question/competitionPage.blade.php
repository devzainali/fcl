@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Question List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
     <!-- Column selectors with Export Options and print table -->
     <section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Get Quiz Questions</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.loadQuestions')}}" method="GET" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                         <select class="select2-size-lg form-control" id="large-select" required onchange="getval(this);" name="competition_id">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}">{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                            <select class="select2-size-lg form-control"  required name="quiz_id" id="loadQuizes" onchange="this.form.submit()">
                                                <option value selected style="display: none">Select Quiz</option>
                                                @foreach($quizes as $key=>$quize)
                                            <option value="{{$quize->id}}">{{$quize->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if(isset($quiz))
<section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showMcq',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add Mcq</a>

                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                        @foreach($quiz->QuestionMc as $key=>$mcq)
                                        <tr id="quizID{{@$mcq->id}}">
                                                <td>{{$mcq->id}}</td>
                                                <td class="product-name">{{$mcq->Question}}</td>
                                                <td class="product-name">{{$mcq->option1}}</td>
                                                <td class="product-name">{{$mcq->option2}}</td>
                                                <td class="product-name">{{$mcq->option3}}</td>
                                                <td class="product-name">{{$mcq->option4}}</td>
                                                <td class="product-name">{{$mcq->answer}}</td>
                                                <td>
                                                    <a href="{{route('admin.editMcq',$mcq->id)}}" class="btn-primary btn-sm">Edit</a>
                                                    <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deleteMcq{{$mcq->id}}">Delete</a>
                                                </td>
                                                <div class="modal fade text-left" id="deleteMcq{{$mcq->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Question</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteMcq',$mcq->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Question? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>

                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showTf',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add True/False</a>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionTf as $key=>$tf)
                                        <tr id="quizID{{$tf->id}}">
                                                <td>{{$tf->id}}</td>
                                                <td class="product-name">{{$tf->Question}}</td>
                                                <td class="product-name">{{($tf->is_correct == true)?'True':'False'}}</td>
                                                <td>
                                                    <a href="{{route('admin.editTf',$tf->id)}}" class="btn-primary btn-sm">Edit</a>
                                                    <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deleteTf{{$tf->id}}">Delete</a>
                                                </td>
                                                <div class="modal fade text-left" id="deleteTf{{$tf->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Question</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteTf',$tf->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Question? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showVs',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add Versus</a>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionVs as $key=>$Vs)
                                        <tr id="quizID{{$Vs->id}}">
                                                <td>{{$Vs->id}}</td>
                                                <td class="product-name">{{$Vs->Question}}</td>
                                                <td class="product-name">{{$Vs->v1Name}}</td>
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($Vs->v1image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                                <td class="product-name">{{$Vs->v2Name}}</td>
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($Vs->v2image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>                                                <td class="product-name">{{$Vs->answer}}</td>
                                                <td>
                                                    <a href="{{route('admin.editVs',$Vs->id)}}" class="btn-primary btn-sm">Edit</a>
                                                    <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deleteVs{{$Vs->id}}">Delete</a>
                                                </td>
                                                <div class="modal fade text-left" id="deleteVs{{$Vs->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Question</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteVs',$Vs->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Question? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')

<script type="text/javascript"> 
function getval(select)
{
    $('#cover-spin').show(0);
    $.ajax({
      type: "GET",
      url: "/admin/question/competition/quiz/get/"+select.value,
      processData: false,
      contentType: false,
      success: function (data) {
        if (data.status == true) {
            var len = data.data.length;
            $("#loadQuizes").empty();
            $("#loadQuizes").append(`<option value selected style="display: none">Select Quiz</option>`);
            for( var i = 0; i<len; i++){
                    var id = data.data[i]['id'];
                    var name = data.data[i]['title'];
                    $("#loadQuizes").append("<option value='"+id+"'>"+name+"</option>");
                }
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
}
function changeCompetition(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "GET",
      url: "/admin/question/competition/quiz/get/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Mcq's Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Edit Mcq's Questions 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Mcq's</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.updateMcq',$value->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ $value->Question }}" required name="question">
                                            <label for="first-name-column">Enter Question Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 1" value="{{ $value->option1 }}" required name="option1">
                                            <label for="first-name-column">Option 1</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 2" value="{{ $value->option2 }}" required name="option2">
                                            <label for="first-name-column">Option 2</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 3" value="{{ $value->option3 }}"  name="option3">
                                            <label for="first-name-column">Option 3</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 4" value="{{ $value->option4 }}"  name="option4">
                                            <label for="first-name-column">Option 4</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="number" id="first-name-column" class="form-control" placeholder="Answer" value="{{ $value->answer }}"  name="answer">
                                            <label for="first-name-column">Correct Option Number</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                   <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" {{ $value->bonus == true ?'checked':'' }} name="isBonus" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Bonus Question</label>
                                                    </div>
                                   </div>
                                <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


@endsection

@section('page_level_scripts')

@endsection
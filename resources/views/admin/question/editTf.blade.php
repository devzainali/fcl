@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Edit Quiz Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">EditTrue/False Questions 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">edit True/False</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.updateTf',$tf->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ $tf->Question }}" required name="question">
                                            <label for="first-name-column">Enter Question Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                         <div class="custom-control custom-switch switch-lg custom-switch-success mr-2 mb-1">
                                            <input type="checkbox" class="custom-control-input" name="Answer" id="customSwitch100"  {{ $tf->is_correct == true ?'checked':'' }}>
                                            <label class="custom-control-label" for="customSwitch100">
                                                <span class="switch-text-left">true</span>
                                                <span class="switch-text-right">false</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                   <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" {{ $tf->bonus == true ?'checked':'' }} name="isBonus" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Bonus Question</label>
                                                    </div>
                                   </div>
                                <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</div>


@endsection

@section('page_level_scripts')


@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Versus Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Edit Versus Questions 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Versus</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.updateVs',$value->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{$value->Question }}" required name="question">
                                            <label for="first-name-column">Enter Question Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Versus Name 1" value="{{ $value->v1Name }}" required name="VerusName1">
                                            <label for="first-name-column">Versus Name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="file" name="Vimage1" id="imageSponser">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Versus Name 2" value="{{ $value->v2Name }}" required name="VerusName2">
                                            <label for="first-name-column">Versus Name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="file" name="Vimage2"  id="imageSponser">
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                    <div class="form-label-group">
                                    <input type="number" id="first-name-column" class="form-control" placeholder="answer" value="{{ $value->answer }}"  name="answer">
                                    <input type="hidden" class="form-control"  value="{{ $value->id }}"  name="quiz_id">
                                            <label for="first-name-column">Correct Option Number</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-12">
                                       <label for="">Current Versus Image 1</label>
                                       <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($value->v1image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                    </div>            
                                    <div class="col-md-3 col-12">
                                       <label for="">Current Versus Image 2</label>
                                       <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($value->v2image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                    </div>  
                                    <div class="col-md-12 col-12">
                                   <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" {{ $value->bonus == true ?'checked':'' }} name="isBonus" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Bonus Question</label>
                                                    </div>
                                   </div>          
                                <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

</div>


@endsection

@section('page_level_scripts')

<script type="text/javascript">
function deleteQuiz(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/quiz/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deleteQuiz${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#quizID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Quiz Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Quiz ({{$quiz->title}}) Questions 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Mcq's</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeMcq')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ old('question') }}" required name="question">
                                            <label for="first-name-column">Enter Question Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 1" value="{{ old('option1') }}" required name="option1">
                                            <label for="first-name-column">Option 1</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 2" value="{{ old('option2') }}" required name="option2">
                                            <label for="first-name-column">Option 2</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 3" value="{{ old('option3') }}"  name="option3">
                                            <label for="first-name-column">Option 3</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="text" id="first-name-column" class="form-control" placeholder="Option 4" value="{{ old('option4') }}"  name="option4">
                                            <label for="first-name-column">Option 4</label>
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control"  value="{{ $quiz->id }}" required name="quiz_id">
                                   <div class="col-md-12 col-12">
                                   <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="isBonus" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Bonus Question</label>
                                                    </div>
                                   </div>
                                    <!-- <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                    <input type="number" id="first-name-column" class="form-control" placeholder="Answer" value="{{ old('correct') }}"  name="answer">
                                            <label for="first-name-column">Correct Option Number</label>
                                        </div>
                                    </div> -->
                                    
                                <div class="col-12">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionMc as $key=>$mcq)
                                        <tr id="quizID{{$mcq->id}}">
                                                <td>{{$mcq->id}}</td>
                                                <td class="product-name">{{$mcq->Question}}</td>
                                                <td class="product-name">{{$mcq->option1}}</td>
                                                <td class="product-name">{{$mcq->option2}}</td>
                                                <td class="product-name">{{$mcq->option3}}</td>
                                                <td class="product-name">{{$mcq->option4}}</td>
                                                <td class="product-name">{{$mcq->answer}}</td>
                                                <td>
                                                    <a href="{{route('admin.editMcq',$mcq->id)}}" class="btn-primary btn-sm">Edit</a>
                                                    <a data-toggle="modal" class="btn-danger btn-sm" data-target="#delete{{$mcq->id}}">Delete</a>

                                                </td>
                                                <div class="modal fade text-left" id="delete{{$mcq->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Question</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteMcq',$mcq->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Question? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>Correct</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')

<script type="text/javascript">
function deleteQuiz(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/quiz/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deleteQuiz${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#quizID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
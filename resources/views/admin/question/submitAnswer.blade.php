@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Question List
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<form action="{{route('admin.submitAnswerStore')}}" method="post">
@csrf
<input type="hidden" name="quiz_id" value="{{$quiz->id}}">
<section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showMcq',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add Mcq</a>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                   
                                        @foreach($quiz->QuestionMc as $key=>$mcq)
                                        <tr id="quizID{{@$mcq->id}}">
                                                <td>{{$mcq->id}}</td>
                                                <td class="product-name">{{$mcq->Question}}</td>
                                                <td class="product-name">{{$mcq->option1}}</td>
                                                <td class="product-name">{{$mcq->option2}}</td>
                                                <td class="product-name">{{$mcq->option3}}</td>
                                                <td class="product-name">{{$mcq->option4}}</td>
                                                <td>
                                                <input type="hidden" name="mcqID[]" value="{{$mcq->id}}" required>
                                                 <select name="mcq[]" id="" class="select2-size-lg form-control" required>
                                                    <option value selected style="display:none;">Select Answer</option>
                                                    <option value="1">{{$mcq->option1}}</option>
                                                    <option value="2">{{$mcq->option2}}</option>
                                                    <option value="3">{{$mcq->option3}}</option>
                                                    <option value="4">{{$mcq->option4}}</option>
                                                 </select>                     
                                                    <!-- <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deletePartner{{$quiz->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a> -->

                                                </td>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Option1</th>
                                            <th>Option2</th>
                                            <th>Option3</th>
                                            <th>Option4</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showTf',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add True/False</a>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionTf as $key=>$tf)
                                        <tr id="quizID{{$tf->id}}">
                                                <td>{{$tf->id}}</td>
                                                <td class="product-name">{{$tf->Question}}</td>
                                                <td class="product-name">{{($tf->is_correct == true)?'True':'False'}}</td>
                                                <td>
                                                <input type="hidden" name="tfID[]" value="{{$tf->id}}" required>
                                                 <select name="tf[]" id="" class="select2-size-lg form-control" required>
                                                    <option value selected style="display:none;">Select Answer</option>
                                                    <option value="1">True</option>
                                                    <option value="0">False</option>
                                                 </select>         
                                                    <!-- <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deletePartner{{$quiz->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a> -->
                                                </td>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="column-selectors">
        <div class="row">
            <div class="col-12">
            <a href="{{route('admin.showVs',$quiz->id)}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus"></i>Add Versus</a>
                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Name</th>
                                            <th>Name</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionVs as $key=>$Vs)
                                        <tr id="quizID{{$Vs->id}}">
                                                <td>{{$Vs->id}}</td>
                                                <td class="product-name">{{$Vs->Question}}</td>
                                                <td class="product-name">{{$Vs->v1Name}}</td>
                                                <!-- <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($Vs->v1image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td> -->
                                                <td class="product-name">{{$Vs->v2Name}}</td>
                                                <!-- <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($Vs->v2image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>                             -->
                                                <td>
                                                <input type="hidden" name="vsID[]" value="{{$Vs->id}}" required>
                                                 <select name="vs[]" id="" class="select2-size-lg form-control" required>
                                                    <option value selected style="display:none;">Select Answer</option>
                                                    <option value="1">{{$Vs->v1Name}}</option>
                                                    <option value="2">{{$Vs->v2Name}}</option>
                                                 </select> 
                                                    <!-- <a data-toggle="modal" class="btn-danger btn-sm" data-target="#deletePartner{{$quiz->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a> -->

                                                </td>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>Question</th>
                                            <th>Name</th>
                                            <th>Name</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->
    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light" style="float:right;">submit Answers</button>

</div>
</form>
@endsection

@section('page_level_scripts')

<script type="text/javascript"> 
function getval(select)
{
    $('#cover-spin').show(0);
    $.ajax({
      type: "GET",
      url: "/admin/question/competition/quiz/get/"+select.value,
      processData: false,
      contentType: false,
      success: function (data) {
        if (data.status == true) {
            var len = data.data.length;
            $("#loadQuizes").empty();
            $("#loadQuizes").append(`<option value selected style="display: none">Select Quiz</option>`);
            for( var i = 0; i<len; i++){
                    var id = data.data[i]['id'];
                    var name = data.data[i]['title'];
                    $("#loadQuizes").append("<option value='"+id+"'>"+name+"</option>");
                }
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
}
function changeCompetition(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "GET",
      url: "/admin/question/competition/quiz/get/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Quiz Question</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Quiz ({{$quiz->title}}) True/False Questions 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add True/False</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeTf')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ old('question') }}" required name="question">
                                            <input type="hidden" value="{{ $quiz->id }}" required name="quiz_id">
                                            <label for="first-name-column">Enter Question Title</label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                   <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" name="isBonus" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Bonus Question</label>
                                                    </div>
                                   </div>
                                    <!-- <div class="col-md-12 col-12">
                                         <div class="custom-control custom-switch custom-switch-primary ml-auto">
                                            <input type="checkbox" class="custom-control-input" name="Answer" id="customSwitch1" checked="">
                                            <label class="custom-control-label" for="customSwitch1"></label>
                                        </div>
                                    </div> -->
                                <div class="col-12 mt-1">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quiz->QuestionTf as $key=>$tf)
                                        <tr id="quizID{{$tf->id}}">
                                                <td>{{$tf->id}}</td>
                                                <td class="product-name">{{$tf->Question}}</td>
                                                <td class="product-name">
                                                @php
                                                if(isset($tf->is_correct)){
                                                   echo ($tf->is_correct == true)?'True':'False';
                                                }else{
                                                    echo 'Not specified';
                                                }
                                                @endphp
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.editTf',$tf->id)}}" class="btn-primary btn-sm">Edit</a>
                                                    <a data-toggle="modal" class="btn-danger btn-sm" data-target="#delete{{$tf->id}}">Delete</a>

                                                    </td>
                                                    <div class="modal fade text-left" id="delete{{$tf->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title" id="myModalLabel33">Delete Question</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <form action="{{route('admin.deleteTf',$tf->id)}}" method="post">
                                                            @csrf
                                                                <div class="modal-body">
                                                                    <h5>Are you sure to delete your Question? </h5>
                                                                    <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                    <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>#</th>
                                            <th>Question</th>
                                            <th>Answer</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')


@endsection
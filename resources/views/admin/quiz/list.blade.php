@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Quiz</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Quiz 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Quiz</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.storeQuiz')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ old('name') }}" required name="name">
                                            <label for="first-name-column">Enter Name</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                            <select class="select2-size-lg form-control" id="large-select" required name="competition_id">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}">{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Expire Time                                      
                                        </div>
                                        <input type="date" id="last-name-column" class="form-control" placeholder="Date" value="{{ old('expireDate') }}" name="expireDate" required >

                                    </div>
                                   
                                    </div>
                                <div class="col-12 mt-5">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="row">
                        <div class="col-md-4 mt-5 ml-2">
                        <h4>Select Competition to load quiz's</h4>
                            <form action="{{route('admin.showQuizByCompetition','come')}}" method="get">
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id_for_quiz">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Quiz ID</th>
                                            <th>Name</th>
                                            <th>Competition Name</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($quizes as $key=>$quiz)
                                        <tr id="quizID{{$quiz->id}}">
                                                <td>{{$quiz->id}}</td>
                                                <td class="product-name">{{$quiz->title}}</td>
                                                <td class="product-name">{{$quiz->competition->name}}</td>
                                                <td>
                                                    @php
                                                            $expire = strtotime($quiz->expired_at);
                                                            $today = strtotime("today midnight");
                                                            @endphp
                                                             @if($today >= $expire)
                                                                @if(!$quiz->isCalculated() > 0)
                                                                <a href="{{route('admin.submitAnswer',$quiz->id)}}" class="btn-success btn-sm">Submit Answers</a>
                                                                @else
                                                                <a href="{{route('admin.showQuizResult',$quiz->id)}}" class="btn-info btn-sm">View Result</a>
                                                                @endif
                                                             @else
                                                             <!-- <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                                Add question
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                <a class="dropdown-item" href="{{route('admin.showMcq',$quiz->id)}}">MCQ's</a>
                                                                <a class="dropdown-item" href="{{route('admin.showTf',$quiz->id)}}">True/False</a>
                                                                <a class="dropdown-item" href="{{route('admin.showVs',$quiz->id)}}">Versus </a>
                                                            </div> -->
                                                             <a href="{{route('admin.editQuiz',$quiz->id)}}" class="btn-primary btn-sm">Edit</a>
                                                             <a data-toggle="modal" data-target="#delete{{$quiz->id}}" href="{{route('admin.geteNativeAdd',$quiz->id)}}" class="btn-primary btn-sm">Delete</a>
                                                             @endif
                                                        </td>
                                                        <div class="modal fade text-left" id="delete{{$quiz->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Competition</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.deleteQuiz',$quiz->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Quiz? </h5>
                                                                <button class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Quiz ID</th>
                                            <th>Name</th>
                                            <th>Competition Name</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ @$quizes->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')

<script type="text/javascript">
function select(item)
{
    let url = "{{route('admin.showQuizByCompetition','come')}}";
    window.location.href=url.replace('come',item.value);
}
// function getval(id)
// {
//     $('#cover-spin').show(0);
//     $.ajax({
//       type: "GET",
//       url: "/admin/quiz/delete/"+id,
//       processData: false,
//       contentType: false,
//       success: function (data) {
//           $(`#deleteQuiz${id}`).modal('hide');
//         if (data.status == true) {
//           toastr.success(data.message);
//           $(`#quizID${id}`).remove();
//         }
//         if (data.status == false) {
//           toastr.error(data.message);
//         }
//         $('#cover-spin').hide();
//       },
//     });
// }
// $('#selectCompetition').on('change', function (e) {
//     debugger;
//     var optionSelected = $("option:selected", this);
//     var valueSelected = this.value;
//     alert(valueSelected);
//     console.log(valueSelected);
// });
function deleteQuiz(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/quiz/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deleteQuiz${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#quizID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
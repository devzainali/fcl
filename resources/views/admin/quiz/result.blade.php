@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Quiz ( {{$quiz->title}} )</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Quiz result 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>User Name</th>
                                            <th>Point</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($results as $key=>$result)
                                        <tr id="quizID{{$result->id}}">
                                                <td>{{$key + 1}}</td>
                                                <td class="product-name">{{$result->user->f_name}}</td>
                                                <td class="product-name">
                                                @php
                                                $score = $result->mcqPoint + $result->vsPoint + $result->tfPoint;
                                               if($score == 120){
                                               $score = $score+30;
                                               } 
                                                @endphp
                                                {{$score}}
                                                </td>
                                            </tr>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>User Name</th>
                                            <th>Point</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{$results->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Social</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Edit Social link
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Edit Social Link</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.social.update',$social->id)}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                <div class="col-md-12 col-12">
                                                    <select class="select2-size-lg form-control" id="large-select" required name="platform">
                                                        <option value selected style="display: none">Select Social Platform</option>
                                                        <!-- <option value="Facebook" {{ 'Facebook' == $social->socialName ? 'selected' : '' }}>Facebook</option> -->
                                                        <option value="WhatsApp" {{ 'WhatsApp' == $social->socialName ? 'selected' : '' }}>WhatsApp</option>
                                                        <option value="Instagram" {{ 'Instagram' == $social->socialName ? 'selected' : '' }}>Instagram</option>
                                                        <option value="Twitter" {{ 'Twitter' == $social->socialName ? 'selected' : '' }}>Twitter</option>
                                                        <option value="LinkedIn" {{ 'LinkedIn' == $social->socialName ? 'selected' : '' }}>LinkedIn</option>
                                                    </select>
                                    </div>
                                <div class="col-md-12 col-12 mt-2">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" name="link" required  placeholder="Enter Link" value="{{ $social->link }}" />
                                            <label for="city-column">Link</label>
                                        </div>
                                    </div>
                                <div class="col-12 mt-2">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')

@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Social</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Social links
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Social Link</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.social.store')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                <div class="col-md-12 col-12">
                                                    <select class="select2-size-lg form-control" id="large-select" required name="platform">
                                                        <option value selected style="display: none">Select Social Platform</option>
                                                        <!-- <option value="Facebook">Facebook</option> -->
                                                        <option value="WhatsApp">WhatsApp</option>
                                                        <option value="Instagram">Instagram</option>
                                                        <option value="Twitter">Twitter</option>
                                                        <option value="LinkedIn">LinkedIn</option>
                                                    </select>
                                    </div>
                                <div class="col-md-12 col-12 mt-2">
                                        <div class="form-label-group">
                                            <input type="text" class="form-control" name="link" required  placeholder="Enter Link" value="{{ old('link') }}" />
                                            <label for="city-column">Link</label>
                                        </div>
                                    </div>
                                <div class="col-12 mt-2">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Social ID</th>
                                            <th>Social Platform</th>
                                            <th>Social Link</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($Socoals as $key=>$social)
                                        <tr id="socialID{{$social->id}}">
                                                <td>{{$social->id}}</td>
                                                <td class="product-name">{{$social->socialName}}</td>
                                                <td class="product-name">{{$social->link}}</td>
                                                <td>
                                                <a data-toggle="modal" data-target="#deletesocial{{$social->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>
                                                <a href="{{route('admin.social.edit',$social->id)}}" ><i class="fa fa-pencil fonticon-container" aria-hidden="true"  style="color:blue"></i></a>                                                </td>
                                            </tr>
                                            <div class="modal fade text-left" id="deletesocial{{$social->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete social</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="{{route('admin.social.delete',$social->id)}}" method="post">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your social links? </h5>
                                                                <button  class="btn btn-danger mr-1 my-1" >Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Social ID</th>
                                            <th>Social Platform</th>
                                            <th>Social Link</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $Socoals->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>
<div class="modal fade text-left" id="NotificationDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary white">
                <h5 class="modal-title" id="myModalLabel160">Notification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Message</h3>
                <hr>
                <p id="q_message">This is demo</p>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-primary" data-dismiss="modal">Accept</button> --}}
            </div>
        </div>
    </div>
</div>

@endsection

@section('page_level_scripts')

<script type="text/javascript">
function detail(id){
    $('#cover-spin').show(0);
    $(`#q_message`).text($(`#message${id}`).text());
    $('#NotificationDetail').modal('show'); 
    $('#cover-spin').hide();

}
function deletesocial(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/pushNotification/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deletesocial${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#socialID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
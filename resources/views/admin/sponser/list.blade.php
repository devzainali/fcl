@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Sponsors</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">Add Sponsor 
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Add Sponsor</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                    <form class="form" action="{{route('admin.SponserStore')}}" method="post" enctype="multipart/form-data" >
                            @csrf
                            <div class="form-body">
                                <div class="row">
                                    <!-- <div class="col-md-6 col-12">
                                        <div class="form-label-group">
                                            <input type="text" id="first-name-column" class="form-control" placeholder="Name" value="{{ old('name') }}" required name="name">
                                            <label for="first-name-column">Enter Name</label>
                                        </div>
                                    </div> -->
                                    <div class="col-md-6 col-12">
                                    <div class="form-label-group">
                                            <select class="select2-size-lg form-control" id="large-select" required name="competition_id">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}">{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12">
                                        <div class="form-label-group">
                                            <input type="file" name="image"  required id="imageSponser">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-label-group">Effective Date                                      
                                        </div>
                                        <input type="date" id="last-name-column txtDate" class="form-control" placeholder="Date" value="{{ old('effectivedate') }}" name="effectivedate" required >

                                    </div>
                                <div class="col-12 mt-5">
                                        <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Submit</button>
                                        <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light">Reset</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sponser ID</th>
                                            <th>Competition Name</th>
                                            <th>Effective Date</th>
                                            <th>Image</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($Sponsers as $key=>$Sponser)
                                        <tr id="partnerID{{$Sponser->id}}">
                                                <td>{{$Sponser->id}}</td>
                                                <td class="product-name">{{$Sponser->competition->name}}</td>
                                                <td class="product-name">{{$Sponser->effective_date}}</td>
                                                <td class="w-25">
                                                    <img src="{{Storage::disk('public')->url($Sponser->image)}}" class="img-fluid img-thumbnail" alt="Sheep">
                                                </td>
                                                <td>
                                                    <a data-toggle="modal" data-target="#deletePartner{{$Sponser->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>

                                                </td>
                                                
                                            </tr>
                                            <div class="modal fade text-left" id="deletePartner{{$Sponser->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete Sponser</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form method="post" action="{{route('admin.SponserDelete',$Sponser->id)}}">
                                                        @csrf
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your Sponser? </h5>
                                                                <button class="btn btn-danger mr-1 my-1">Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                           @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Effective Date</th>
                                            <th>Image</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $Sponsers->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')

<script type="text/javascript">
function deletePartner(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/sponser/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          $(`#deletePartner${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#partnerID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
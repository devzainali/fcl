@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Competition Transaction</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">check Transaction against competition
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
<a href="{{route('admin.addCompetition')}}" class="btn btn-icon btn-outline-primary mr-1 mb-1 waves-effect waves-light" ><i class="feather icon-plus"></i>Add Competition</a>
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                        <th>Competition ID</th>
                                            <th>Title</th>
                                            <th>price</th>
                                            <th>action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($competitons as $key=>$competiton)
                                        <tr id="courceID{{$competiton->id}}">
                                                <td>{{$competiton->id}}</td>
                                                <td class="product-name">{{$competiton->name}}</td>
                                                <td class="product-price">{{$competiton->subAmount}}</td>
                                                <td>
                                                <a class="btn btn-primary btn-sm" href="{{route('admin.transactionList',$competiton->id)}}">Transaction List </a>
                                                
                                                </td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Competition ID</th>
                                            <th>Title</th>
                                            <th>price</th>
                                            <th>action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $competitons->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>
@endsection
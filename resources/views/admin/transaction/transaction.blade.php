@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Transaction list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of transactions
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Transaction ID</th>
                                            <th>Email</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($transactions as $key=>$transaction)
                                        <tr id="">
                                                <td>{{$key+1}}</td>
                                                <td class="product-name">{{$transaction->user->email}}</td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($transaction->method)){
                                                    echo $transaction->method;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($transaction->amount)){
                                                    echo $transaction->amount;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td>{{$transaction->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                        <th>Transaction ID</th>
                                            <th>Email</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $transactions->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
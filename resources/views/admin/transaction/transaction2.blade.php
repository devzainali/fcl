@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Transaction list</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of transactions
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped dataex-html5-selectors" id="transactionTable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Email</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
$(document).ready(function(){

$('#user_table').DataTable({
 processing: true,
 serverSide: true,
 ajax:{
  url: "{{ route('ajax-crud.index') }}",
 },
 columns:[
 {
   data: 'first_name',
   name: 'first_name'
  },
  {
   data: 'image',
   name: 'image',
   render: function(data, type, full, meta){
       
    return "<img src={{ URL::to('/') }}/images/" + data + " width='70' class='img-thumbnail' />";
   },
   orderable: false
  },
  {
   data: 'first_name',
   name: 'first_name'
  },
  {
   data: 'last_name',
   name: 'last_name'
  },
  {
   data: 'action',
   name: 'action',
   orderable: false
  }
 ]
});
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
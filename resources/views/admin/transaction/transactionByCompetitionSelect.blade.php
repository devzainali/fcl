@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Transaction list (competition Name)</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">List of transactions
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <div class="row">
                        <div class="col-md-4 mt-5 ml-2">
                        <h4>Select Competition</h4>
                            <form action="{{route('admin.transactionListCompetition','come')}}" method="get">
                            @csrf
                            <select class="select2-size-lg form-control" id="large-select" onchange="select(this)" required name="competition_id">
                                                <option value selected style="display: none">Select Competition</option>
                                                @foreach($competitions as $key=>$competition)
                                            <option value="{{$competition->id}}" {{ $competition->id == @$quizCompetition ? 'selected' : '' }}>{{$competition->name}}</option>
                                                @endforeach
                                            </select>
                            </form>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Transaction ID</th>
                                            <th>Email</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($transactions))
                                    @foreach($transactions as $key=>$transaction)
                                        <tr id="">
                                                <td>{{$key+1}}</td>
                                                <td class="product-name">{{$transaction->user->email}}</td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($transaction->method)){
                                                    echo $transaction->method;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td class="product-price">
                                                @php
                                                if(!is_null($transaction->amount)){
                                                    echo $transaction->amount;
                                                }else{
                                                    echo 'Not Found';
                                                }
                                                @endphp
                                                </td>
                                                <td>{{$transaction->created_at}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Transaction ID</th>
                                            <th>Email</th>
                                            <th>Method</th>
                                            <th>Amount</th>
                                            <th>Time</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                @if(isset($transactions))
                                {{ $transactions->links() }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Column selectors with Export Options and print table -->
</div>


@endsection

@section('page_level_scripts')
<script src="{{asset('admin/app-assets/js/scripts/modal/components-modal.js')}}"></script>

<script type="text/javascript"> 
function select(item)
{
    let url = "{{route('admin.transactionListCompetition','come')}}";
    window.location.href=url.replace('come',item.value);
}
function changeCompetitionStatus(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/competiton/statusChange/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
          debugger;
        if (data.status == true) {
          toastr.success(data.message);
        }
        if (data.error) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
@extends('admin.layouts.app')

@section('page_content')
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">User List</h2>
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active">User Listing
                        </li>
                        <li class="breadcrumb-item active">Total Users ({{$count}})
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="content-body">
       <!-- Data list view starts -->
     <!-- Column selectors with Export Options and print table -->
     <section id="column-selectors">
        <div class="row">
            <div class="col-12">
                <div class="card">
                   
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            
                        <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Player ID</th>
                                            <th>Name</th>
                                            <th>email</th>
                                            <!-- <th>spins</th>
                                            <th>points</th> -->
                                            <!-- <th>action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $key=>$user)
                                        <tr id="userID{{$user->id}}">
                                            <td>{{$user->id}}</td>
                                            <td class="product-name" >{{$user->f_name.' '.$user->l_name}}</td>
                                            <td class="product-price" >{{$user->email}}</td>
                                            <!-- @if(isset($user->userSpin))
                                                <td class="product-price">{{$user->userSpin->spinCount}}</td>
                                            @else
                                                <td class="product-price">0</td>
                                            @endif -->
                                            <!-- @if(isset($user->userPoint))
                                                <td class="product-price">{{$user->userPoint->points}}</td>
                                            @else
                                                <td class="product-price">0</td>
                                            @endif -->
                                             <!-- <td>
                                                    <a ><i class="fa fa-eye fonticon-container" aria-hidden="true"  style="color:blue"></i></a>
                                                    <a data-toggle="modal" data-target="#deleteUser{{$user->id}}"><i class="fa fa-trash fonticon-container" aria-hidden="true" style="color: red"></i></a>

                                                </td> -->
                                            </tr>
                                            <div class="modal fade text-left" id="deleteUser{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="myModalLabel33">Delete User Request</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <form action="#">
                                                            <div class="modal-body">
                                                                <h5>Are you sure to delete your User ? </h5>
                                                                <button type="button" class="btn btn-danger mr-1 my-1" onclick="deleteUser({{$user->id}});">Yes</button>
                                                                <button type="button" class="btn btn-primary my-1" data-dismiss="modal">No</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>email</th>
                                            <!-- <th>spins</th>
                                            <th>points</th> -->
                                            <!-- <th>action</th> -->
                                        </tr>
                                    </tfoot>
                                </table>
                                {{ $users->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Column selectors with Export Options and print table -->
</div>

@endsection

@section('page_level_scripts')
<script type="text/javascript">
function deleteUser(id) {
    $('#cover-spin').show(0);
    $.ajax({
      type: "POST",
      url: "/admin/contact/delete/"+id,
      processData: false,
      contentType: false,
      success: function (data) {
        $(`#deleteContact${id}`).modal('hide');
        if (data.status == true) {
          toastr.success(data.message);
          $(`#userID${id}`).remove();
        }
        if (data.status == false) {
          toastr.error(data.message);
        }
        $('#cover-spin').hide();
      },
    });
  }
</script>
@endsection
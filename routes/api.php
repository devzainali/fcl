<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('user/login', 'UserController@login');
Route::post('user/register', 'UserController@register');
Route::post('user/activate', 'UserController@activateAccount');
Route::post('user/resendActivate', 'UserController@resendVerification');
Route::post('user/reset/request', 'ResetPasswordController@request');
Route::post('user/reset/validate', 'ResetPasswordController@validateCode');
Route::post('user/reset/password', 'ResetPasswordController@resetPassword');

Route::middleware(['auth:api','verified','cors', 'json.response'])->group(function () {
    Route::get('user/user', 'UserController@details');
    Route::post('user/updateProfile', 'UserController@updateProfile');
    Route::post('user/changeProfileImage', 'UserController@changeProfileImage');
    Route::post('user/changePassword', 'UserController@changePassword');
    Route::post('contact/add', 'ContactController@store');
    Route::get('news/all', 'NewsController@index');
    Route::get('news/specfic/{id}', 'NewsController@show');
    Route::get('competiton/all', 'CompetitionController@index');
    Route::get('competiton/weeks/{id}', 'CompetitionController@CompetitionWeeks');
    Route::get('competiton/gift/all/{id}', 'CompetitionController@gifts');
    Route::get('competiton/quiz/{id}', 'CompetitionController@CompetitionQuiz');
    Route::get('competition/score/{id}', 'CompetitionController@getCompetitionScore');
    Route::get('competition/{id}/scoreList', 'CompetitionController@getLeagueScoreList');


    Route::get('sponsers/all', 'SponserController@index');
    Route::post('quiz/submit', 'SubmitQuizController@submitQuiz');
    //test
    Route::get('quiz/result/{id}', 'SubmitQuizController@showResult');
    Route::get('quiz/score/{id}', 'SubmitQuizController@checkQuizScoreList');
    //general Ads
    Route::get('generalAds/all', 'AdController@index');
    //User Subscribe to a competition  //subscribeCompetition
    Route::post('competiton/subscribe', 'CompetitionController@subscribeCompetition');
    Route::post('competiton/subscribe/free', 'CompetitionController@subscribeFreeCompetition');
    //Push Notification User
    Route::get('user/pushNotification', 'PushNotificationController@getUserNotification');
    //user subscribe competition lists
    Route::get('user/subscription', 'UserController@getUserSubscription');
    // app stats
    Route::post('user/appStat', 'AppStatController@addStat');
    //ad stat
    Route::post('user/adStat', 'AppStatController@adStat');
    //get score url
    Route::get('scoreUrl', 'LiveScoreController@index');
    //get Social Links
    Route::get('social', 'SocialController@index');

});

Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
 
Route::middleware('auth:admin')->group(function () {
    Route::get('user', 'PassportController@details');
});



<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::group(['prefix' => 'admin'], function () {
//   Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
//   Route::post('/login', 'AdminAuth\LoginController@login');
//   Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

//   Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
//   Route::post('/register', 'AdminAuth\RegisterController@register');

//   Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
//   Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
//   Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
//   Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
// });
Route::get('admin/login','AdminAuth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login','AdminAuth\LoginController@login')->name('adminlogin');
Route::group(['prefix'=>'admin','middleware'=>'auth:admin'],function(){
    // Route::get('/dash','AdminController@index')->name('admin.dash');
	// Authentication Routes...
 
    Route::post('/logout','AdminAuth\LoginController@logout')->name('adminlogout');
    Route::get('/profile','Admin\AdminController@showProfile')->name('admin.profile');
    Route::post('/profile/changePassword','Admin\AdminController@changePassword')->name('admin.changePassword');
    Route::get('/contactList','Admin\ContactController@index')->name('admin.showContacts');
    Route::post('/contact/delete/{id}','Admin\ContactController@destroy')->name('admin.deleteContacts');
    Route::get('/applyList','Admin\ApplyController@index')->name('admin.showApplys');
    Route::post('/replay','Admin\ContactController@contactReplay')->name('admin.contactreply');
    Route::post('/apply/delete/{id}','Admin\ApplyController@destroy')->name('admin.deleteApply');
    Route::get('/apply/detail/{id}','Admin\ApplyController@detail')->name('admin.applyDetail');
    Route::get('/user/list','Admin\AdminController@userList')->name('admin.userList');
   //news
   Route::get('/news/add','Admin\NewsController@create')->name('admin.addNews');
   Route::post('/news/store','Admin\NewsController@store')->name('admin.newsStore');
   Route::get('/news/list','Admin\NewsController@index')->name('admin.newsStorelist');
   Route::post('/news/delete/{id}','Admin\NewsController@destroy')->name('admin.deleteNews');
   Route::get('/news/edit/{id}','Admin\NewsController@edit')->name('admin.editNews');
   Route::post('/news/update/{id}','Admin\NewsController@update')->name('admin.updateNews');
   //competition
   Route::get('/competition/add','Admin\CompetitionController@create')->name('admin.addCompetition');
   Route::post('/competition/store','Admin\CompetitionController@store')->name('admin.storeCompetition');
   Route::get('/competition/list','Admin\CompetitionController@index')->name('admin.showCompetition');
   Route::get('/competition/edit/{id}','Admin\CompetitionController@edit')->name('admin.editCompetition');
   Route::post('/competition/update/{id}','Admin\CompetitionController@update')->name('admin.updateCompetition');
   Route::post('/competiton/statusChange/{id}','Admin\CompetitionController@changeStatus')->name('admin.changeStatusCompetition');
   Route::post('/competition/delete/{id}','Admin\CompetitionController@destroy')->name('admin.deleteCompetition');
   Route::get('/competiton/score',function(){
    return view('admin.competition.competitionUserScoreList');
   })->name('admin.geCompetitionScoreLoad');
   Route::get('/competiton/score/{id}','Admin\CompetitionController@getCompetitionScore')->name('admin.geCompetitionScore');
   Route::get('/competiton/userList',function(){
    return view('admin.competition.subscriptionList');
   })->name('admin.geCompetitionUsers');
   Route::get('/competiton/userList/{id}','Admin\CompetitionController@competitionUserList')->name('admin.geCompetitionUserList');
   Route::post('/competiton/markWinner','Admin\CompetitionController@markWinner')->name('admin.CompetitionMarkWinner');
   //gift
   Route::get('/gift/add/{id}','Admin\GiftController@create')->name('admin.addgift');
   Route::post('/gift/store','Admin\GiftController@store')->name('admin.storeGift');
   Route::get('/gift/list/{id}','Admin\GiftController@show')->name('admin.showGift');
   Route::get('/gift/list',function(){
    return view('admin.gift.list');
   })->name('admin.showGiftList');
   Route::get('/gift/edit/{id}','Admin\GiftController@edit')->name('admin.editgift');
   Route::post('/gift/update/{id}','Admin\GiftController@update')->name('admin.updategift');
   //sponser
   Route::get('/sponser/list','Admin\SponserController@index')->name('admin.showSponser');
   Route::post('/sponser/store','Admin\SponserController@store')->name('admin.SponserStore');
   Route::post('/sponser/delete/{id}','Admin\SponserController@destroy')->name('admin.SponserDelete');
   //Quiz
   Route::get('/quiz/list','Admin\QuizController@index')->name('admin.showQuiz');
   Route::get('/competition/quiz/list/{competition_id}','Admin\QuizController@loadQuizByCompetition')->name('admin.showQuizByCompetition');
   Route::post('/quiz/store','Admin\QuizController@store')->name('admin.storeQuiz');
   Route::get('/quiz/edit/{id}','Admin\QuizController@edit')->name('admin.editQuiz');
   Route::post('/quiz/delete/{id}','Admin\QuizController@destroy')->name('admin.deleteQuiz');
   Route::post('/quiz/update/{id}','Admin\QuizController@update')->name('admin.updateQuiz');
   Route::get('/quiz/calculate/{id}','Admin\QuizController@calculateQuizResult')->name('admin.calculateQuizResult');
   Route::get('/quiz/showResult/{id}','Admin\QuizController@showQuizResult')->name('admin.showQuizResult');
   //Question
   Route::get('/question/competition','Admin\QuestionController@loadCompetitions')->name('admin.QuestionPage');
   Route::get('/question/competition/quiz/get/{id}','Admin\QuestionController@loadCompetitionsQuiz')->name('admin.QuestionCompetitionQuiz');
   Route::get('/question/competition/quiz/gets','Admin\QuestionController@loadQuestions')->name('admin.loadQuestions');
   Route::get('/quiz/question/list/{id}','Admin\QuizController@loadQuestionByQuiz')->name('admin.submitAnswer');
   Route::post('/quiz/question/list','Admin\QuizController@submitQuizAnswer')->name('admin.submitAnswerStore');


   //Mcq's question
   Route::get('/quiz/mcq/list/{id}','Admin\McqController@index')->name('admin.showMcq');
   Route::post('/quiz/mcq/add','Admin\McqController@store')->name('admin.storeMcq');
   Route::get('/quiz/mcq/edit/{id}','Admin\McqController@edit')->name('admin.editMcq');
   Route::post('/quiz/mcq/update/{id}','Admin\McqController@update')->name('admin.updateMcq');
   Route::post('/quiz/mcq/delete/{id}','Admin\McqController@destroy')->name('admin.deleteMcq');

      //Tf question
   Route::get('/quiz/tf/list/{id}','Admin\TfController@index')->name('admin.showTf');
   Route::post('/quiz/tf/add','Admin\TfController@store')->name('admin.storeTf');
   Route::get('/quiz/tf/edit/{id}','Admin\TfController@edit')->name('admin.editTf');
   Route::post('/quiz/tf/update/{id}','Admin\TfController@update')->name('admin.updateTf');
   Route::post('/quiz/tf/delete/{id}','Admin\TfController@destroy')->name('admin.deleteTf');

    //verus question
    Route::get('/quiz/vs/list/{id}','Admin\VerusController@index')->name('admin.showVs');
    Route::post('/quiz/vs/add','Admin\VerusController@store')->name('admin.storeVs');
    Route::get('/quiz/vs/edit/{id}','Admin\VerusController@edit')->name('admin.editVs');
    Route::post('/quiz/vs/update/{id}','Admin\VerusController@update')->name('admin.updateVs');
    Route::post('/quiz/vs/delete/{id}','Admin\VerusController@destroy')->name('admin.deleteVs');

    //Competition Native Add
    Route::get('/NativeAdd','Admin\CompetitionNativeAddController@index')->name('admin.addNativeAdd');
    Route::get('/NativeAdd/competitions','Admin\CompetitionNativeAddController@list')->name('admin.addNativeCompetition');
    Route::post('/NativeAdd/store','Admin\CompetitionNativeAddController@store')->name('admin.storeNativeAdd');
    Route::get('/NativeAdd/list/{id}','Admin\CompetitionNativeAddController@getCompetitionAds')->name('admin.geteNativeAdd');
    Route::post('/NativeAdd/competition/delete/{id}','Admin\CompetitionNativeAddController@destroy')->name('admin.deleeCompetitionNativeAdd');
    Route::get('/competition/NativeAdd/list/{id}','Admin\CompetitionNativeAddController@loadAdsCompetition')->name('admin.showAdsByCompetition');

    //General Add
    Route::get('/GeneralAdd','Admin\GeneralAdController@index')->name('admin.addGeneralAdd');
    Route::post('/GeneralAdd/store','Admin\GeneralAdController@store')->name('admin.storeGeneralAdd');
    Route::get('/GeneralAdd/list','Admin\GeneralAdController@show')->name('admin.geteGeneralNativeSave');
    Route::post('/GeneralAdd/native/delete/{id}','Admin\GeneralAdController@destroy')->name('admin.deleeNativeAdd');

    //Transaction
    Route::get('/transactionList/{id}','Admin\TransactionController@getCompetitionTransaction')->name('admin.transactionList');
    Route::get('/transaction/List/{competition_id}','Admin\TransactionController@getTransactionByCompetition')->name('admin.transactionListCompetition');
    Route::get('/transaction/competition','Admin\TransactionController@competitionList')->name('admin.TransactioncompetitionList');
    Route::get('/transaction/competition/list','Admin\TransactionController@getCompetitionForSelectBox')->name('admin.getCompetitionForSelectBox');
    //Push Notification
    Route::get('/pushNotification','Admin\PushNotificationController@index')->name('admin.NotificationList');
    Route::post('/pushNotification/store','Admin\PushNotificationController@store')->name('admin.storeNotification');
    Route::post('/pushNotification/delete/{id}','Admin\PushNotificationController@destroy')->name('admin.deleteNotification');
    //App Statistics AppStatController
    Route::get('/appStatistics/','Admin\AppStatController@gerStatistics')->name('admin.statList');
    Route::get('/appStatistics/{type}','Admin\AppStatController@gerStatisticsByType')->name('admin.statListByType');
    Route::post('/appStatistics/delete','Admin\AppStatController@destroy')->name('admin.deletestat');

    //live score api
    Route::get('/liveScore','Admin\LiveScoreController@create')->name('admin.livescore');
    Route::get('/liveScore/list','Admin\LiveScoreController@show')->name('admin.livescoreList');
    Route::post('/liveScore/add','Admin\LiveScoreController@store')->name('admin.livescore.add');
    Route::get('/liveScore/edit/{id}','Admin\LiveScoreController@edit')->name('admin.livescore.edit');
    Route::post('/liveScore/edit/{id}','Admin\LiveScoreController@update')->name('admin.livescore.update');
    Route::post('/liveScore/delete/{id}','Admin\LiveScoreController@destroy')->name('admin.livescore.delete');
    //Social Links
    Route::get('/socialLinks','Admin\SocialController@index')->name('admin.social.list');
    Route::post('/socialLinks/store','Admin\SocialController@store')->name('admin.social.store');
    Route::post('/socialLinks/delete/{id}','Admin\SocialController@destroy')->name('admin.social.delete');
    Route::get('/socialLinks/edit/{id}','Admin\SocialController@edit')->name('admin.social.edit');
    Route::post('/socialLinks/update/{id}','Admin\SocialController@update')->name('admin.social.update');
    //dashboard
    Route::get('/dashboard','Admin\AdminController@dashboard')->name('admin.dashboard');
    //Ad Stat
    Route::get('/adStat/{id}/{type}','Admin\AdController@index')->name('admin.adStatistics');
    Route::post('/adStatistics/delete','Admin\AdController@destroy')->name('admin.deleteadstat');



});
